import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/Layout.js"
import SEO from "../components/seo"
import NewHeaderES from "../components/sections/HeaderNewES.js"
import FooterES from "../components/sections/FooterES.js"
import CookiesBar from "../components/sections/cookies.js"

import { HeaderGroup } from "../components/styles/TextStyles.js"
import NewsletterForm from "../components/sections/newsletter.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"
import {
  CategoryMenuContainer,
  CategoryGroup,
  AllCategories,
  Category,
  Body,
  FeaturedPost,
  FeaturedImage,
  FeaturedText,
  FeaturedTitle,
  PostGroup,
  Post,
  PostImage,
  PostText,
  Title,
  CategoryTagContainer,
  CategoryTag,
  Newsletter,
  NewsletterTitle,
  NewsletterDescription,
  SocialMediaIcons,
  SocialIcon,
} from "./blog.js"

class BlogES extends React.Component {
  render() {
    const { data } = this.props

    const posts = data.allContentfulBlogPost.edges
    const seoContent = data.allContentfulMetasForGeneratedPages.edges[0]

    return (
      <Layout location={this.props.location}>
        <SEO
          title={seoContent.node.blogseometatitle}
          description={seoContent.node.blogseometadescription}
        />
        <NewHeaderES en="/blog" fr="/blog-fr" es="/blog-es" />
        <HeaderGroup>
          <CategoryMenuContainer>
            <CategoryGroup>
              <AllCategories>
                <Link to="/blog-es" activeClassName="active">
                  All
                </Link>
              </AllCategories>
              {data.allContentfulBlogCategory.edges.map(({ node }) => {
                return (
                  <Category key={node.slug}>
                    <Link activeClassName="active" to={`/${node.slug}`}>
                      {node.title}
                    </Link>
                  </Category>
                )
              })}
            </CategoryGroup>
          </CategoryMenuContainer>
        </HeaderGroup>
        <Body>
          <FeaturedPost>
            {posts.map(({ node }) => {
              const title = node.title || node.slug
              if (node.featured) {
                return (
                  <div key={node.slug}>
                    <FeaturedImage>
                      <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
                        <img src={node.image.fluid.src} alt="" />
                      </Link>
                    </FeaturedImage>
                    <FeaturedText>
                      <FeaturedTitle>
                        <Link
                          style={{ boxShadow: `none` }}
                          to={`/${node.slug}`}
                        >
                          {title}
                        </Link>
                      </FeaturedTitle>
                      <CategoryTagContainer>
                        {node.categories.map(categoryES => {
                          return (
                            <Link to={`/${categoryES.slug}`}>
                              <CategoryTag>
                                {categoryES.title}&#160;&#160;|
                              </CategoryTag>
                            </Link>
                          )
                        })}
                      </CategoryTagContainer>
                    </FeaturedText>
                  </div>
                )
              } else {
                return null
              }
            })}
          </FeaturedPost>
          <PostGroup>
            {posts.map(({ node }) => {
              const title = node.title || node.slug
              if (node.featured) {
                return null
              } else {
                return (
                  <Post key={node.slug}>
                    <PostImage>
                      <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
                        <img src={node.image.fluid.src} alt="" />
                      </Link>
                    </PostImage>
                    <PostText>
                      <Title>
                        <Link
                          style={{ boxShadow: `none` }}
                          to={`/${node.slug}`}
                        >
                          {title}
                        </Link>
                      </Title>
                      <CategoryTagContainer>
                        {node.categories.map(categoryES => {
                          return (
                            <Link to={`/${categoryES.slug}`}>
                              <CategoryTag>
                                {categoryES.title}&#160;&#160;|
                              </CategoryTag>
                            </Link>
                          )
                        })}
                      </CategoryTagContainer>
                    </PostText>
                  </Post>
                )
              }
            })}
          </PostGroup>
          <Newsletter>
            <NewsletterTitle>Suscríbete a nuestro Newsletter</NewsletterTitle>
            <NewsletterDescription>
              Recibe los más recientes artículos y update de nuestros productos.
            </NewsletterDescription>
            <NewsletterForm />

            <SocialMediaIcons>
              <a
                href="https://www.linkedin.com/company/optimetriks"
                target="_blank"
                rel="noopener noreferrer"
              >
                <SocialIcon src="/images/linkedIn.png" alt="LinkedIn Logo" />
              </a>
              <a
                href="https://www.facebook.com/fieldproapp"
                target="_blank"
                rel="noopener noreferrer"
              >
                <SocialIcon src="/images/facebook.png" alt="Facebook Logo" />
              </a>
            </SocialMediaIcons>
          </Newsletter>
        </Body>
        <FooterES />
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}

export default BlogES

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allContentfulBlogCategory(
      sort: { fields: updatedAt, order: ASC }
      filter: { language: { eq: "ES" } }
    ) {
      edges {
        node {
          title
          slug
        }
      }
    }
    allContentfulBlogPost(
      sort: { fields: createdAt, order: DESC }
      filter: { language: { eq: "ES" } }
    ) {
      edges {
        node {
          slug
          title
          featured
          image {
            fluid {
              src
            }
          }
          categories {
            title
            slug
          }
        }
      }
    }
    allContentfulMetasForGeneratedPages(filter: { language: { eq: "ES" } }) {
      edges {
        node {
          blogseometadescription
          blogseometatitle
        }
      }
    }
  }
`
