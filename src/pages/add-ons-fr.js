import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"

import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"
import CookiesBar from "../components/sections/cookies.js"

import NewHeaderFR from "../components/sections/HeaderNewFR.js"

import FooterFR from "../components/sections/FooterFR.js"
import {
  BodyMain,
  Caption,
  H1,
  H2,
  H4,
  P,
} from "../components/styles/TextStyles.js"
import styled from "styled-components"

import { ButtonChip } from "../components/Buttons/CtaButton.js"
import { Link } from "gatsby"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"
import DarkCtaFR from "../components/sections/DarkCtaFR.js"

function IntegrationsFR({ data }) {
  const cards = data.allContentfulAddOnPage.edges
  const seoContent = data.allContentfulMetasForGeneratedPages.edges[0]
  const [title, setTitle] = useState("")
  const [foundCards, setFoundCards] = useState(cards)

  const filter = e => {
    const keyword = e.target.value

    if (keyword !== "") {
      const results = cards.filter(workflowcard => {
        return workflowcard.node.title
          .toLowerCase()
          .includes(keyword.toLowerCase())
        // Use the toLowerCase() method to make it case-insensitive
      })
      setFoundCards(results)
    } else {
      setFoundCards(cards)
      // If the text field is empty, show all users
    }

    setTitle(keyword)
  }
  const [offsetY, setOffsetY] = useState(0)
  const handleScroll = () => setOffsetY(window.pageYOffset)

  useEffect(() => {
    window.addEventListener("scroll", handleScroll)

    return () => window.removeEventListener("scroll", handleScroll)
  }, [])

  return (
    <Layout>
      <SEO
        title={seoContent.node.integrationsSeoMetaTitle}
        description={seoContent.node.integrationsSeoMetaDescription}
      />
      <NewHeaderFR en="/add-ons" fr="/add-ons-fr" es="/add-ons-es" />
      <HeroSectionWrapper>
        <HeroSection style={{ transform: `translateY(-${offsetY * 0.3}px)` }}>
          <HeroTitle>Add-ons</HeroTitle>
          <HeroDescription>
            Tirez parti de notre bibliothèque d’add on et d'intégrations
            logicielles pour adapter FieldPro à vos besoins opérationnels.
            Chaque module complémentaire et chaque intégration est construit sur
            la base des bonnes pratiques de votre secteur et vous aidera à
            bénéficier au maximum de la digitalisation de vos opérations sur le
            terrain..
          </HeroDescription>
        </HeroSection>
      </HeroSectionWrapper>
      <SeearchBarContainer
        style={{ transform: `translateY(-${offsetY * 0.3}px)` }}
      >
        <SearchInput
          type="search"
          value={title}
          onChange={filter}
          placeholder="Rechercher un add on..."
        />
      </SeearchBarContainer>
      <WorkflowTemplatesWrapper>
        {foundCards && foundCards.length > 0 ? (
          foundCards.map(workflowcard => (
            <Link to={`/${workflowcard.node.slug}`} key={workflowcard.node.id}>
              <CardTemplateWrapper
                style={{ transform: `translateY(-${offsetY * 0.3}px)` }}
              >
                <CardTemplateIcon>
                  {
                    <img
                      src={workflowcard.node.integrationIcon.file.url}
                      alt=""
                    />
                  }
                </CardTemplateIcon>
                <CardTemplateTitle>{workflowcard.node.title}</CardTemplateTitle>
                <CardTemplateDescription>
                  {workflowcard.node.metaDescription}
                </CardTemplateDescription>
                <CardDividerLine></CardDividerLine>
                <CardChipGroup>
                  {workflowcard.node.tags.map(templatetags => {
                    return <ButtonChip>{templatetags.title}</ButtonChip>
                  })}
                </CardChipGroup>
              </CardTemplateWrapper>
            </Link>
          ))
        ) : (
          <h1>
            Nous sommes désolés! Nous n'avons pas pu trouver les résultats pour
            votre recherche.
          </h1>
        )}
      </WorkflowTemplatesWrapper>
      <CtaBackground>
        <CtaIllustartion>
          <img
            src="/images/Geo-location.svg"
            style={{
              width: "100vw",
            }}
            alt=""
          />
        </CtaIllustartion>
        <CtaWrapper>
          <CtaContent>
            <CtaTagline>Manage your field salesforce digitally</CtaTagline>
            <CtaButtons>
              <DarkCtaFR />
            </CtaButtons>
          </CtaContent>
        </CtaWrapper>
      </CtaBackground>
      <FooterFR />
      <ScrollToTop />
      <CookiesBar />
    </Layout>
  )
}

export const query = graphql`
  {
    allContentfulAddOnPage(filter: { language: { eq: "FR" } }) {
      edges {
        node {
          callToAction
          metaTitle
          metaDescription
          integrationIcon {
            file {
              url
            }
          }
          tags {
            title
          }
          language
          title
          slug
        }
      }
    }
    allContentfulMetasForGeneratedPages(filter: { language: { eq: "FR" } }) {
      edges {
        node {
          integrationsSeoMetaDescription
          integrationsSeoMetaTitle
        }
      }
    }
  }
`

export default IntegrationsFR
export const HeroSectionWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
`
export const HeroSection = styled.div`
  padding: 240px 0px 0px 0px;
  max-width: 800px;

  @media (max-width: 830px) {
    padding: 200px 40px 40px 40px;
    margin: 0 0 0 0px;
    max-width: 672px;
  }
  @media (max-width: 512px) {
    padding: 200px 24px 40px 24px;
    margin: 0 0 0 0px;
    max-width: 360px;
  }
`
export const HeroTitle = styled(H1)`
  margin: 0 0 16px 0;
`
export const HeroDescription = styled(BodyMain)``
export const WorkflowTemplatesWrapper = styled.div`
  max-width: 1280px;
  margin: 60px auto 120px auto;
  display: grid;
  grid-template-columns: repeat(3, auto);
  gap: 40px;
  @media (max-width: 830px) {
    padding: 40px 40px 80px 40px;
    grid-template-columns: repeat(2, auto);
    gap: 30px;
  }
  @media (max-width: 512px) {
    margin: 60px auto 40px auto;
    padding: 40px 24px 40px 24px;
    grid-template-columns: repeat(1, auto);
    gap: 30px;
  }
`

//SEARCH BAR
export const SeearchBarContainer = styled.div`
  width: 480px;
  position: relative;
  display: flex;
  bottom: 0;
  left: 0;
  right: 0;
  margin: 0 0 0 1110px;
  @media (max-width: 830px) {
    margin: 20px 0 0 28px;
    width: 640px;
  }
  @media (max-width: 512px) {
    width: 320px;
    margin: 0px 0 0 28px;
  }
`
export const SearchInput = styled.input`
  width: 480px;
  height: 64px;
  margin: 0 0px 0 0px;

  border-radius: 8px;
  border: solid 1px #d3d3d3;
  font-size: 16px;
  padding: 15px 45px 15px 15px;

  color: #6c6c6c;
  border-radius: 6px;

  transition: all 0.4s;
  :focus {
    border: solid 1px #d3d3d3;
    outline: none;
    box-shadow: 0 1px 8px #b8c6db;
    -moz-box-shadow: 0 1px 8px #b8c6db;
    -webkit-box-shadow: 0 1px 8px #b8c6db;
  }
  @media (max-width: 830px) {
    margin: 0;
    width: 640px;
  }
  @media (max-width: 512px) {
    width: 320px;
    margin: 0;
  }
`

//CARDS CSS
export const CardTemplateWrapper = styled.div`
  width: 400px;
  height: 420px;
  border: 1px solid #979797;
  border-radius: 5px;
  padding: 40px;
  :hover {
    border: 2px solid #2c2c2c;
  }
  @media (max-width: 830px) {
    padding: 20px;
    width: 320px;
    height: auto;
  }
  @media (max-width: 512px) {
    padding: 20px;
    width: 320px;
    height: auto;
  }
`
export const CardTemplateIcon = styled.div`
  height: 64px;
  width: 64px;
  border-radius: 50%;
`
export const CardTemplateTitle = styled(H4)`
  margin: 20px 0 10px 0;
`
export const CardTemplateDescription = styled(P)``
export const CardDividerLine = styled.div`
  width: 320px;
  height: 1px;
  background: #d8d8d8;
  padding: 0;
  margin: 40px 0 20px 0;
  @media (max-width: 830px) {
    width: 280px;
  }
  @media (max-width: 512px) {
    width: 280px;
  }
`
export const CardChipGroup = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
`

//CTA TEXT
export const CtaBackground = styled.div`
  background-color: #124e5d;
  width: 100vw;
  position: relative;
  left: -8px;
`
export const CtaWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 200px 0 200px 0;
  @media (max-width: 830px) {
    padding: 100px 48px 100px 48px;
  }
  @media (max-width: 512px) {
    padding: 80px 24px 80px 24px;
  }
`
export const CtaIllustartion = styled.div`
  position: absolute;
  align-self: center;
  top: 100px;
`
export const CtaContent = styled.div`
  position: relative;
  z-index: 999;
  max-width: 1280px;
  padding: 0 0 0 0;
  @media (max-width: 830px) {
    padding: 0;
  }
  @media (max-width: 512px) {
    padding: 0px;
  }
`
export const CtaTagline = styled(H2)`
  font-size: 3.25em;
  color: #fefefe;
  max-width: 600px;
  margin: 0 0 20px 0;
  @media (max-width: 830px) {
    padding: 0px;
  }
  @media (max-width: 512px) {
    padding: 0px;
    max-width: 320px;
    font-size: 2.5em;
    margin: 0 0 40px 0;
  }
`
export const CtaButtons = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 512px) {
    flex-direction: column;
  }
`
export const TrialGroup = styled.div``
export const DisclaimerText = styled(Caption)`
  color: #fefefe;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
