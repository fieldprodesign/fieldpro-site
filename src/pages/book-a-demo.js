import React from "react"
import "react-phone-input-2/lib/style.css"
import { CountryDropdown } from "react-country-region-selector"

import SEO from "../components/seo.js"

import Footer from "../components/sections/Footer.js"
import CookiesBar from "../components/sections/cookies.js"
import styled from "styled-components"
import "../components/styles/buttons.css"
import RatingImg from "/static/images/Icons/star_rate-24px.svg"

import { Label, P, H3, H2, H4 } from "../components/styles/TextStyles.js"

import RatingsSlider from "../components/Homepage-components/RatingsSlider.js"
import NewHeaderDark from "../components/sections/HeaderNewDark.js"

class BookDemoForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      value: "",
      submitting: false,
      country: "",
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  //Allow user to selct country in input fileds
  selectCountry(val) {
    this.setState({ country: val })
  }

  //handle changes in input fileds
  handleChange(event) {
    this.setState({ value: event.target.value })
  }
  //submit to sheets
  handleSubmit(event) {
    event.preventDefault()
    const data = new FormData(event.target)

    fetch(
      "https://script.google.com/macros/s/AKfycbxfI06LLxIm3awq-MnpNAkfabuYD2pDqaZdSZydkrG5EHRP7XnGLmeawHDeabDgBUOaHA/exec",
      {
        method: "POST",
        body: data,
      }
    )
    //reset the form
    document.getElementById("book-demo").reset()
    //Confirmation message
    this.setState({
      submitting: true,
    })
    // //make the message dissapear
    // setTimeout(() => {
    //   this.setState({
    //     submitting: false,
    //   })
    // }, 3000)
  }
  handleClick() {
    this.setState({
      submitting: false,
    })
  }

  render() {
    const { country } = this.state
    const sucessMessage = (
      <SuceessMessageWrapper>
        <SucessMsg>
          <Checkamrk>
            <img src="images/Icons/check_circlebig.svg.svg" alt="" />
          </Checkamrk>
          <Title>Thank you, your request has been received !</Title>
          <DescText>
            One of our product specialists will be in touch with you shortly to
            present the FieldPro solution.
          </DescText>
        </SucessMsg>
      </SuceessMessageWrapper>
    )

    return (
      <React.Fragment>
        <SEO title="Request a Personalized Demo |FieldPro" description="" />{" "}
        <BodyWrapper>
          <NewHeaderDark
            en="/book-a-demo"
            fr="/book-a-demo-fr"
            es="/book-a-demo-es"
          />

          <PageWrapper>
            <HeroText>
              <HeroTitle>
                Get to know how FieldPro helps you manage your field operations
              </HeroTitle>
              <HeroSubtitle>With FieldPro you:</HeroSubtitle>
              <HeroList>
                <ListItems>Increase your field force productivity</ListItems>
                <ListItems>
                  Achieve higher customer satisfaction through regular visits
                </ListItems>
                <ListItems>Guarantee 100% field work compliance</ListItems>
                <ListItems>
                  Reduce time spent in data collection and visualisation
                </ListItems>
                <ListItems>
                  Benefit from a continuous and dedicated support
                </ListItems>
              </HeroList>
              <TestimonialSection>
                <TestimonialTitle> Our users say:</TestimonialTitle>
                <TestimonialContent>
                  <RatingsSlider>
                    <TestimonialWrapper>
                      <TestimonialQuote>
                        <Userdetails>
                          <Avator>
                            <Initials>A</Initials>
                          </Avator>
                          <UserID>
                            <Username>AINEBYE BRIAN</Username>
                            <Rating>
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>
                            </Rating>
                          </UserID>
                        </Userdetails>
                        <Quotation>
                          This app has really helped us in our day to day field
                          engangements,because it shows the last date of agent
                          visit,location,agent detailed description and above
                          all the readily vaialble support team incase of any
                          app related challenges in the field.{" "}
                        </Quotation>
                      </TestimonialQuote>
                    </TestimonialWrapper>
                    <TestimonialWrapper>
                      <TestimonialQuote>
                        <Userdetails>
                          <Avator>
                            <Initials>K</Initials>
                          </Avator>
                          <UserID>
                            <Username>KELVIN KARANI</Username>
                            <Rating>
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>
                            </Rating>
                          </UserID>
                        </Userdetails>
                        <Quotation>
                          It's a very convenient app to use for an organization
                          which seeks transparency and effectiveness. Still very
                          easy to manouvre through even for a new entry user
                          with little experience. It's one of the best platforms
                          I've come across. Kudos! for the great job on the app.
                        </Quotation>
                      </TestimonialQuote>
                    </TestimonialWrapper>
                    <TestimonialWrapper>
                      <TestimonialQuote>
                        <Userdetails>
                          <Avator>
                            <Initials>E</Initials>
                          </Avator>
                          <UserID>
                            <Username>Epenyu Daniel</Username>
                            <Rating>
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>
                            </Rating>
                          </UserID>
                        </Userdetails>
                        <Quotation>
                          It's a really great app, it has made locating agents
                          very easy.
                        </Quotation>
                      </TestimonialQuote>
                    </TestimonialWrapper>

                    <TestimonialWrapper>
                      <TestimonialQuote>
                        <Userdetails>
                          <Avator>
                            <Initials>P</Initials>
                          </Avator>
                          <UserID>
                            <Username>PAULINE NJOROGE</Username>
                            <Rating>
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>{" "}
                              <StarIcon>
                                <img src={RatingImg} alt="FieldPro rating" />
                              </StarIcon>
                            </Rating>
                          </UserID>
                        </Userdetails>
                        <Quotation>
                          My experience with fieldpro is amazing the app is easy
                          to use ,one is even able to use it offline and incase
                          there is any trouble shooting the clients are always
                          there to help. My best part is that one is able to
                          access product knowldge uploaded on the app on the
                          various categories of products that a brand has.
                        </Quotation>
                      </TestimonialQuote>
                    </TestimonialWrapper>
                  </RatingsSlider>
                </TestimonialContent>
              </TestimonialSection>
            </HeroText>
            {this.state.submitting ? sucessMessage : ""}

            {!this.state.submitting && (
              <FormWrapper>
                <Formbody>
                  <form
                    id="book-demo"
                    method="post"
                    onSubmit={this.handleSubmit}
                  >
                    <Formheader>
                      <FormTitle>Request a Demo</FormTitle>
                      <Formdescription>
                        One of our product specialists will get in touch quickly
                        with you to discuss your needs and show you around our
                        software
                      </Formdescription>
                    </Formheader>
                    <NameBlock>
                      <FormGroup>
                        <Label>
                          First Name<span style={{ color: "red" }}>*</span>
                        </Label>
                        <InputSmall
                          type="text"
                          name="firstname"
                          id="firstname"
                          required
                        />
                      </FormGroup>
                      <FormGroup>
                        <Label>
                          Last Name<span style={{ color: "red" }}>*</span>
                        </Label>
                        <InputSmall
                          type="text"
                          name="lastname"
                          id="lastname"
                          required
                        />
                      </FormGroup>
                    </NameBlock>

                    <FormGroup>
                      <Label>
                        Email<span style={{ color: "red" }}>*</span>
                      </Label>
                      <InputBox
                        type="email"
                        name="email"
                        id="email"
                        placeholder="youremail@yourcompany.com"
                        required
                      />
                    </FormGroup>
                    <PhoneBlock>
                      <FormGroup>
                        <Label style={{ paddingBottom: "8px" }}>
                          Country<span style={{ color: "red" }}>*</span>
                        </Label>
                        <CountryDropdown
                          name="Country"
                          value={country}
                          onChange={val => this.selectCountry(val)}
                          style={styles.countryStyle}
                          required
                        />
                      </FormGroup>
                      <FormGroup>
                        <Label>
                          Phone Number<span style={{ color: "red" }}>*</span>
                        </Label>
                        <InputPhone
                          type="text"
                          name="PhoneNumber"
                          id=""
                          required
                        />
                      </FormGroup>
                    </PhoneBlock>

                    <FormGroup>
                      <Label>
                        Company Name<span style={{ color: "red" }}>*</span>
                      </Label>
                      <InputBox
                        name="companyname"
                        type="text"
                        id="companyname"
                        placeholder="Enter your Company Name"
                        required
                      />
                    </FormGroup>
                    <FormGroup>
                      <Label style={{ paddingBottom: "16px" }}>
                        Number of Field Users
                        <span style={{ color: "red" }}>*</span>
                      </Label>
                      <div class="radio-toolbar">
                        <input
                          type="radio"
                          id="radio5to50"
                          name="numberofusers"
                          value="5 to 50"
                          onChange={this.state.handleChange}
                        />
                        <label htmlFor="radio5to50">5 to 50</label>

                        <input
                          type="radio"
                          id="radio50to500"
                          name="numberofusers"
                          value="50 to 500"
                          onChange={this.state.handleChange}
                        />
                        <label htmlFor="radio50to500">50 to 500</label>
                        <input
                          type="radio"
                          id="radio500"
                          name="numberofusers"
                          value="500+"
                          onChange={this.state.handleChange}
                        />
                        <label htmlFor="radio500">500+</label>
                      </div>
                    </FormGroup>

                    <FormGroup>
                      <Terms>
                        <Newsltettercheck>
                          <Checkbox
                            type="hidden"
                            name="send_newsletter"
                            value="no"
                          />
                          <Checkbox
                            type="checkbox"
                            name="send_newsletter"
                            id="send_newsletter"
                            value="yes"
                          />
                          <label htmlFor="send_newsletter">
                            I would like to subscribe to the newsletter to
                            receive training materials, industry insights and
                            FieldPro updates.
                          </label>
                        </Newsltettercheck>
                        <Termstext>
                          {" "}
                          <span>
                            By creating an account, you agree to our
                          </span>{" "}
                          <a href="https://fieldproapp.com/terms">
                            <span
                              style={{ color: "#124e5d", fontWeight: "bold" }}
                            >
                              Terms & Conditions
                            </span>
                          </a>{" "}
                          and{" "}
                          <a href="https://fieldproapp.com/privacypolicy">
                            <span
                              style={{ color: "#124e5d", fontWeight: "bold" }}
                            >
                              Privacy Policy
                            </span>
                          </a>
                        </Termstext>
                      </Terms>
                    </FormGroup>
                    <Nextbutton
                      id="book_a_demo_final_step"
                      type="submit"
                      value={this.state.value}
                      onChange={this.handleChange}
                    >
                      Book a Demo
                    </Nextbutton>
                  </form>
                </Formbody>{" "}
              </FormWrapper>
            )}
          </PageWrapper>
        </BodyWrapper>
        <Footer />
        <CookiesBar />
      </React.Fragment>
    )
  }
}

export default BookDemoForm
export const BodyWrapper = styled.div`
  --avatar-size: 3em;

  background-color: #124e5d;
  width: 101vw;

  padding: 0px !important;
  margin: 0px !important;
  position: relative;
  left: -8px;
  right: 0px;
`
export const PageWrapper = styled.div`
  position: relative;

  max-width: 1280px;
  margin: 0px auto -120px auto;
  padding: 240px 0;
  display: grid;
  grid-template-columns: 537px auto;
  grid-gap: 70px;
  @media (max-width: 830px) {
    padding: 160px 64px;
    grid-template-columns: repeat(1, auto);
  }
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
    padding: 120px 24px 40px 24px;
    margin: 0;
    grid-gap: 40px;
    max-width: 350px;
  }
`
export const HeroText = styled.div`
  margin: 0 auto;
  color: #fefefe;
  @media (max-width: 450px) {
    max-width: 350px;
  }
`
export const HeroTitle = styled(H2)`
  margin: 0 0 24px 0;
  font-size: 40px;
  @media (max-width: 450px) {
    max-width: 350px;
  }
`
export const HeroSubtitle = styled(H4)`
  margin: 24px 0 24px 0;
  @media (max-width: 450px) {
    margin: 0;
  }
`
export const HeroList = styled.ul`
  margin: 0;
  padding: 0;
  @media (max-width: 450px) {
    margin: 0;
  }
`
export const ListItems = styled.li`
  margin: 20px 0px 0px 0px;
  padding: 0;
  font-size: 18px;
  font-weight: 600;
  list-style-image: url("images/Icons/check_circle-white-20px.svg");
  list-style-position: inside;
  @media (max-width: 450px) {
    margin: 20px 000;
  }
`
export const TestimonialSection = styled.div`
  margin: 48px 0 0 0;
  @media (max-width: 450px) {
    margin: 0;
  }
`
export const TestimonialTitle = styled(H3)`
  margin: 48px 0 24px 0;
  @media (max-width: 450px) {
    margin: 0;
  }
`
export const TestimonialContent = styled.div`
  margin: 0;
  @media (max-width: 450px) {
    margin: 0;
  }
`
export const TestimonialWrapper = styled.div`
  width: 440px;

  border-radius: 16px;
  margin: 0;
  padding: 24px;
  background-color: #a0b8be;
  position: relative;
  -moz-border-radius: 10px;
  -webkit-border-radius: 10px;
  &::after {
    content: "";
    position: absolute;
    bottom: 0;
    left: 20%;
    width: 0;
    height: 0;

    border: 26px solid transparent;
    border-top-color: #a0b8be;
    border-bottom: 0;
    margin-left: -26px;
    margin-bottom: -26px;
  }
  @media (max-width: 450px) {
    margin: 0;
    width: 320px;
  }
`
export const Avator = styled.div`
  background-color: #124e5d;
  border-radius: 50%;
  height: var(--avatar-size);
  text-align: center;
  width: var(--avatar-size);
`

export const Initials = styled.div`
  font-size: calc(var(--avatar-size) / 2); /* 50% of parent */
  line-height: 1;
  position: relative;
  top: calc(var(--avatar-size) / 6); /* 25% of parent */
`
export const TestimonialQuote = styled.div`
  margin: 0;
  padding: 0;
`
export const Userdetails = styled.div`
  display: grid;
  grid-template-columns: 64px auto;
  grid-gap: 0px;
  margin: 0 0 12px 0;
  padding: 0;
  align-items: center;
`
export const Username = styled(P)`
  color: #2c2c2c;
  margin: 0;
  padding: 0;
`
export const UserID = styled.div`
  margin: 0 0 0 0;
  padding: 0;
`
export const Rating = styled.div`
  height: 16px;
  display: grid;
  grid-template-columns: 16px 16px 16px 16px auto;
  grid-gap: 0;
  margin: 0;
  padding: 0;
  margin: 0;
`
export const StarIcon = styled.div`
  padding: 0 0 0 0px;
`
export const Quotation = styled(P)`
  color: #2c2c2c;
  font-style: italic;
`

export const FormWrapper = styled.div`
  width: 672px;
  height: auto;
  margin: 32px auto 160px auto;

  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.25);
  background-color: #fefefe;
  @media (max-width: 450px) {
    max-width: 350px;
    margin: 160px auto 160px auto;
  }
`
export const Formbody = styled.div`
  padding: 32px 32px;
  @media (max-width: 450px) {
    padding: 10px 10px;
  }
`
export const Formheader = styled.div``
export const FormTitle = styled(H2)`
  margin: 0;
  @media (max-width: 450px) {
    font-size: 2em;
    padding: 20px 10px 20px 10px;
  }
`
export const Formdescription = styled(P)`
  color: #6c6c6c;
  margin: 12px 0 0 0;
  font-size: 16px;
  @media (max-width: 450px) {
    padding: 0px 10px 0px 10px;
  }
`
export const FormGroup = styled.div`
  padding: 24px 0 0 0;
  display: grid;

  @media (max-width: 450px) {
    padding: 20px 10px 0px 10px;
  }
`

export const InputBox = styled.input`
  width: 607px;
  height: 48px;
  margin: 10px 0 0;
  padding: 14px 334px 14px 16px;
  border-radius: 5px;
  border: solid 1px #979797;
  outline: none;
  :focus {
    border: solid 1.5px #124e5d;
  }

  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  @media (max-width: 450px) {
    width: 300px;

    padding: 0 12px 00;
  }
`

export const InputSmall = styled.input`
  width: 295px;
  height: 48px;
  margin: 8px 0 0;
  border-radius: 5px;
  border: solid 1px #979797;

  padding: 0 1em;

  font-size: 1.2em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  outline: none;
  :focus {
    border: solid 1.5px #124e5d;
  }
  @media (max-width: 450px) {
    width: 300px;
  }
`
export const InputPhone = styled.input`
  width: 290px;
  height: 48px;
  margin: 8px 0 0;
  border-radius: 5px;
  border: solid 1px #979797;

  padding: 0 1em;

  font-size: 1.2em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  outline: none;
  :focus {
    border: solid 1.5px #124e5d;
  }
  @media (max-width: 450px) {
    width: 300px;
  }
`
export const NameBlock = styled.div`
  display: grid;
  grid-template-columns: 295px 295px;
  gap: 16px;
  @media (max-width: 450px) {
    max-width: 300px;
    grid-template-columns: auto;
    gap: 10px;
  }
`
export const PhoneBlock = styled.div`
  display: grid;
  grid-template-columns: 302px auto;
  gap: 16px;
  @media (max-width: 450px) {
    max-width: 300px;
    grid-template-columns: auto;
    gap: 10px;
  }
`

export const Nextbutton = styled.button`
  width: 607px;
  height: 60px;
  margin: 16px 0 0 0;
  padding: 12px 0;
  border: none;
  border-radius: 4px;
  background-color: #febd55;
  cursor: pointer;
  font-weight: 600;
  font-size: 1.25em;
  color: #fefefe;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
  @media (max-width: 450px) {
    max-width: 320px;
  }
`
export const StartTrialButton = styled.button`
  width: 607px;
  height: 60px;
  margin: 16px 0 0 0;
  padding: 12px 0;
  border-radius: 4px;
  background-color: #124e5d;
  cursor: pointer;
  border: none;
  font-weight: 600;
  font-size: 1.25em;
  color: #fefefe;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
  @media (max-width: 450px) {
    max-width: 320px;
  }
`
export const DownloadLinkBtn = styled.button`
  background-color: #124e5d;
  width: 800px;
  height: 60px;
  margin: 40px 0 0;
  padding: 12px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  font-weight: 600;
  font-size: 1.25em;
  color: #fefefe;
  @media (max-width: 450px) {
    max-width: 320px;
  }
`
export const DownloadAppButton = styled.button`
  width: 800px;
  height: 60px;
  margin: 16px 0 0;
  padding: 12px 0;
  border-radius: 4px;
  background-color: #124e5d;
  cursor: pointer;
  font-weight: 600;
  font-size: 1.25em;
  color: #fefefe;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
  @media (max-width: 450px) {
    max-width: 320px;
  }
`

export const Terms = styled.div`
  margin: 0;
  color: #6c6c6c;
`
export const Newsltettercheck = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 12px;
  margin: 8px 0 12px 0;
`
export const Termstext = styled.label`
  color: #6c6c6c;
  margin: 0;
  padding: 0;
`
export const Checkbox = styled.input`
  cursor: pointer;
  height: 24px;
  width: 24px;

  &:checked {
    background: #124e5d;
  }
  :hover {
    background: #124e5d;
  }
`
export const SignInopt = styled.div`
  margin: 32px 0 40px 0;
`

export const Checkamrk = styled.div`
  margin: 20px 0px 12px 160px;

  img {
    width: 120px;
  }
  @media (max-width: 450px) {
    transform: scale(0.7);
    margin: -20px 0 -20px 40px;
  }
`

export const Subtext = styled(P)`
  @media (max-width: 450px) {
    margin: 0px 10px 0px 10px;
  }
`
export const Title3 = styled(H3)`
  margin: 0;
  @media (max-width: 450px) {
    margin: 12px 10px 0px 10px;
    font-size: 1.2em;
  }
`
export const DescText = styled(P)`
  margin: 12px 0 20px 0;
  @media (max-width: 450px) {
    margin: 10px 10px 20px 10px;
    font-size: 12px;
  }
`
export const Title = styled(H4)`
  margin: 20px 0 10px 0;

  @media (max-width: 450px) {
    font-size: 2em;
    margin: 20px 10px 40px 10px;
  }
`
export const QuizBlock = styled.div`
  padding: 0;
  margin: 10px 0 32px 0;
`
export const QuestionTitle = styled(H4)`
  @media (max-width: 450px) {
    margin: 20px 10px 40px 10px;
  }
`
export const SelectChips = styled.div``
export const RadioBox = styled.input`
  width: 32px;
  height: 32px;
`
export const RadioGroup = styled.div`
  display: grid;
  gap: 4px;
  @media (max-width: 450px) {
    max-width: 320px;
  }
`
export const Item = styled.div`
  display: flex;
  align-items: center;
  height: 48px;
  position: relative;
  box-sizing: border-box;
  margin-bottom: 0px;
  color: #6c6c6c;
  @media (max-width: 450px) {
    max-width: 320px;
  }
`

///Customized radio buttons

export const RadioToolbar = styled.div`
  margin: 0;
  padding: 40px 0 20px 0;
`
export const RadioLabel = styled.label`
  display: inline-block;
  background-color: #fefefe;
  border-radius: 22px;
  border: solid 1px #979797;
  margin: 0;
  padding: 0;

  font-size: 18px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  color: #6c6c6c;
  :hover {
    background-color: #f5f5f5;
  }
`
export const RadioInput = styled.input`
  &:checked + ${RadioLabel} {
    background-color: #c4d3d6;
    border-color: none;
  }
`

export const OtherField = styled.input`
  margin: 0 20px 0 30px;
  padding: 0 1em;
  height: 3em;
  width: 385px;
  border: solid 1px #979797;
  border-radius: 0.2em;
  font-size: 1.2em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  @media (max-width: 450px) {
    margin: 0 10px 0 10px;
    width: 200px;
  }
`

//Navigation Bar
export const NavigationRow = styled.div`
  display: grid;
  grid-template-columns: 62px 140px 100px 100px 120px;
  grid-gap: 24px;
  margin: -24px 0 0 0;
  padding: 0;

  @media (max-width: 450px) {
    display: none;
  }
`
export const Stepone = styled.div`
  display: grid;
  align-items: center;
  align-self: center;
`
export const AccIcon = styled.div`
  padding: 0 0 0 32px;
`
export const AccIconmiddle = styled.div`
  padding: 0px 0 0 48px;
`
export const TextContent = styled.div`
  font-size: 20px;
  font-weight: 400;
  color: #2c2c2c;
  padding: 0 0 0px 0px;
`
export const TextContentGrey = styled.div`
  display: grid;
  grid-template-columns: 24px auto;
  gap: 8px;
  font-size: 20px;
  font-weight: bold;
  color: #c6c6c6;
  padding: 0 0 0 10px;
`
export const TextContentlast = styled.div`
  font-size: 20px;
  font-weight: 400;
  color: #6c6c6c;
  padding: 0 0 0 0px;
`
export const Bar = styled.div`
  width: 120px;
  height: 2px;
  margin: 38px 26px 16px 4px;
  background-color: #124e5d;
`
export const Bar2 = styled.div`
  width: 120px;
  height: 2px;
  margin: 38px 26px 16px 4px;
  background-color: #e6e6e6;
`
export const BarGrey = styled.div`
  width: 184px;
  height: 6px;
  margin: -16px 000;
  border-radius: 3px;
  background-color: #6c6c6c;
`
//Sucess message container
export const SuceessMessageWrapper = styled.div`
  max-width: 480px;
  margin: 0;
  padding: 0;
  height: 400px;
  border-radius: 4px;
  background-color: #fefefe;
  box-shadow: 0 0.5em 2em #d0dcdf07;
`
export const SucessMsg = styled.div`
  padding: 20px 32px;
`

///Phone Field styles
const styles = {
  countryStyle: {
    backgroundColor: "#fefefe",
    fontSize: "1em",
    color: "#6c6c6c",
    margin: "0",
    padding: "0 8px",
    height: "48px",
    width: "302px",
    border: "0.1em solid #6c6c6c",
    borderRadius: "0.2em",
  },
}
