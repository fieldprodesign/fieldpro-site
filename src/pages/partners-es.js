import React from "react"
import { graphql } from "gatsby"
import styled from "styled-components"

import Layout from "../components/Layout.js"
import SEO from "../components/seo"

import NewHeaderES from "../components/sections/HeaderNewES.js"

import FooterES from "../components/sections/FooterES.js"

import { H1, H2, H3, P } from "../components/styles/TextStyles.js"

import CookiesBar from "../components/sections/cookies.js"

import BecomeaPartnerDarkES from "../components/sections/partnersformmodalDarkES.js"
import BecomeaPartnerES from "../components/sections/partnersformmodalES.js"

class PartnerPageES extends React.Component {
  render() {
    const industry = this.props.data.allContentfulPartnersPage.edges[0].node

    const brandsLogo = this.props.data.allContentfulPartnersPage.edges[0].node
      .brandsLogos
    //FETCHING BODY CONTENT IN A LIST
    const listDataOne = this.props.data.allContentfulPartnersPage.edges[0].node
      .bodySectionText1.json.content[0].content

    const listDataTwo = this.props.data.allContentfulPartnersPage.edges[0].node
      .bodySectionText2.json.content[0].content

    //  x.data.allContentfulPartnersPage.edges[0].node.bodySectionText2.json.content[0] .content[0].content[0].content[0].value

    // x.data.allContentfulPartnersPage.edges[0].node.bodySectionText2.json.content[0].content[1].content[0].content[0].value

    return (
      <Layout location={this.props.location}>
        <SEO title={industry.pageName} description={industry.pageDescription} />

        <NewHeaderES en="/partners" fr="/partners-fr" es="/partners-es" />
        <BodyGroup>
          <HeroWrapper>
            <HeroContent>
              <Content>
                <Title>{industry.bannerTitle}</Title>

                <Description>{industry.bannerText}</Description>
                <CallToAction>
                  <BecomeaPartnerES id="becomepartner" />
                  {/* <Link to="/find-a-partner-es">
                    <ScheduleButton id="Findpartner">
                      Encuentra un Socio
                    </ScheduleButton>
                  </Link> */}
                </CallToAction>
              </Content>
              <HeroImage>
                <RectangleBackground
                  style={{
                    backgroundColor: `${industry.bannerImageBackground}`,
                  }}
                ></RectangleBackground>
                <ImageForeground>
                  <img
                    style={styles.foregroundImage}
                    src={industry.bannerImage.fluid.src}
                    alt=""
                  />
                </ImageForeground>
              </HeroImage>
            </HeroContent>
          </HeroWrapper>
          <BenefitsSectionWrapper>
            <BenefitsSectionTitle>
              {industry.benefitsSectionTitle}
            </BenefitsSectionTitle>
            <SectionCards>
              <BenefitsWrapper>
                <BenefitsIcon>
                  <img src={industry.benefitIcon1.file.url} alt="" />
                </BenefitsIcon>
                <BenefitsContent>
                  <BenefitsTitle>{industry.benefitTitle1}</BenefitsTitle>
                  <BenefitsDescription>
                    {industry.benefitText1}
                  </BenefitsDescription>
                </BenefitsContent>
              </BenefitsWrapper>
              <BenefitsWrapper>
                <BenefitsIcon>
                  <img src={industry.benefitIcon2.file.url} alt="" />
                </BenefitsIcon>
                <BenefitsContent>
                  <BenefitsTitle>{industry.benefitTitle2}</BenefitsTitle>
                  <BenefitsDescription>
                    {industry.benefitText2}
                  </BenefitsDescription>
                </BenefitsContent>
              </BenefitsWrapper>
              <BenefitsWrapper>
                <BenefitsIcon>
                  <img src={industry.benefitIcon3.file.url} alt="" />
                </BenefitsIcon>
                <BenefitsContent>
                  <BenefitsTitle>{industry.benefitTitle3}</BenefitsTitle>
                  <BenefitsDescription>
                    {industry.benefitText3}
                  </BenefitsDescription>
                </BenefitsContent>
              </BenefitsWrapper>
            </SectionCards>
          </BenefitsSectionWrapper>
          <BrandSectionWrapper>
            <BrandsSectionTitle>{industry.brandsTitle}</BrandsSectionTitle>
            <MiddleCtaButton>
              <BecomeaPartnerES id="becomepartner" />
            </MiddleCtaButton>
            <LogosWrapper>
              {brandsLogo.map(companyLogo => {
                return (
                  <Logo>
                    <img src={companyLogo.fluid.src} alt="" />
                  </Logo>
                )
              })}
            </LogosWrapper>
          </BrandSectionWrapper>
          <BodySectionWrapper>
            <BodyContentWrapper>
              <BodyContentDescription>
                <BodyContentTitle>
                  {industry.bodySectionTitle1}
                </BodyContentTitle>
                <BodyContentTextBlock>
                  <ListGroup>
                    {listDataOne.map(ListItemOne => {
                      return (
                        <ListItem>
                          {ListItemOne.content[0].content[0].value}
                        </ListItem>
                      )
                    })}
                  </ListGroup>
                </BodyContentTextBlock>
                <TextCta>
                  <a
                    style={{ color: "#febd55" }}
                    href={
                      industry.bodySectionLink1.json.content[0].content[1].data
                        .uri
                    }
                  >
                    {
                      industry.bodySectionLink1.json.content[0].content[1]
                        .content[0].value
                    }
                  </a>
                  <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                </TextCta>
              </BodyContentDescription>
              <ProductPicture>
                <img src={industry.bodySectionImage1.fluid.src} alt="" />
              </ProductPicture>
            </BodyContentWrapper>
            <BodyContentWrapperTwo>
              <ProductPictureTwo>
                <img src={industry.bodySectionImage2.fluid.src} alt="" />
              </ProductPictureTwo>
              <BodyContentDescription>
                <BodyContentTitle>
                  {industry.bodySectionTitle2}
                </BodyContentTitle>
                <BodyContentTextBlock>
                  <ListGroup>
                    {listDataTwo.map(ListItemTwo => {
                      return (
                        <ListItem>
                          {ListItemTwo.content[0].content[0].value}
                        </ListItem>
                      )
                    })}
                  </ListGroup>
                </BodyContentTextBlock>
                <TextCta>
                  <a
                    style={{ color: "#febd55" }}
                    href={
                      industry.bodySectionLink2.json.content[0].content[1].data
                        .uri
                    }
                  >
                    {
                      industry.bodySectionLink2.json.content[0].content[1]
                        .content[0].value
                    }
                  </a>
                  <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                </TextCta>
              </BodyContentDescription>
            </BodyContentWrapperTwo>
          </BodySectionWrapper>
          <BottomCallToAction>
            <CtaBackground>
              <img
                style={{ width: "72em" }}
                src={industry.ctaBgIllustration.fluid.src}
                alt="Illustration"
              />
            </CtaBackground>
            <CtaContent>
              <CtaText>
                <Tagline style={{ color: "#fefefe" }}>
                  {industry.callToAction}
                </Tagline>
                <CtaGroup>
                  <TrialGroup>
                    <BecomeaPartnerDarkES id="becomepartner" />
                  </TrialGroup>
                  {/* <Link to="/find-a-partner-es">
                    <ScheduleButtonDark id="findpartner">
                      Trouver un partenaire
                    </ScheduleButtonDark>
                  </Link> */}
                </CtaGroup>
              </CtaText>
            </CtaContent>
          </BottomCallToAction>
        </BodyGroup>
        <FooterES />
        <CookiesBar />
      </Layout>
    )
  }
}

export default PartnerPageES

export const pageQuery = graphql`
  {
    allContentfulPartnersPage(filter: { language: { eq: "ES" } }) {
      edges {
        node {
          bannerImage {
            fluid {
              src
            }
          }
          bannerImageBackground
          bannerText
          bannerTitle
          benefitIcon1 {
            file {
              url
            }
          }
          benefitIcon2 {
            file {
              url
            }
          }
          benefitIcon3 {
            file {
              url
            }
          }
          benefitText1
          benefitText2
          benefitText3
          benefitTitle1
          benefitTitle2
          benefitTitle3
          benefitsSectionTitle
          pageDescription
          pageName
          ctaBgIllustration {
            fluid {
              src
            }
          }
          callToAction
          brandsTitle
          brandsLogos {
            fluid {
              src
            }
          }
          bodySectionTitle2
          bodySectionTitle1
          bodySectionText2 {
            json
          }
          bodySectionText1 {
            json
          }
          bodySectionLink2 {
            json
          }
          bodySectionLink1 {
            json
          }
          bodySectionImage2 {
            fluid {
              src
            }
          }
          bodySectionImage1 {
            fluid {
              src
            }
          }
        }
      }
    }
  }
`

//BODYGROUP
const BodyGroup = styled.div`
  max-width: 1280px;
  margin: 0 auto;
`

//HERO SECTION STYLE
const HeroWrapper = styled.div`
  position: relative;
`
const HeroContent = styled.div`
  max-width: 1280px;

  padding: 280px 0px 0 0px;

  display: grid;
  grid-template-columns: repeat(2, auto);

  @media (max-width: 450px) {
    grid-template-columns: auto;
    margin: 0;
    padding: 0;
  }
`
const Content = styled.div`
  max-width: 660px;
  @media (max-width: 450px) {
    max-width: 400px;

    padding: 200px 40px 80px;
  }
`
const Title = styled(H1)`
  color: #2c2c2c;
  padding-bottom: 20px;

  @media (max-width: 450px) {
    font-size: 2em;
  }
`
const Description = styled(P)`
  padding-bottom: 30px;
`
const CallToAction = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 450px) {
    flex-direction: column;
  }
`
const HeroImage = styled.div`
  @media (max-width: 450px) {
    display: none;
  }
`
const RectangleBackground = styled.div`
  width: 864px;
  height: 648px;
  position: absolute;
  top: 160px;

  padding: 0 0 0 65px;
  transform: rotate(-20deg);
  border-radius: 48px;
  -webkit-filter: blur(12px);
  filter: blur(12px);
  /* background-color: #febd55; */
  @media (max-width: 450px) {
    display: none;
  }
`
const ImageForeground = styled.div`
  border-radius: 48px;
  padding: 0 0 0 65px;
`

const styles = {
  foregroundImage: {
    width: "864px",
    height: "648px",
    borderRadius: "48px",
    position: "absolute",
    top: "160px",
    padding: "0 0 0 0px",
    transform: "rotate(-27deg)",
  },
}

//Benefits section cards
const BenefitsSectionWrapper = styled.div`
  margin: 380px auto 160px auto;
  @media (max-width: 450px) {
    margin: 0px auto 80px auto;
  }
`
const BenefitsSectionTitle = styled(H2)`
  padding-bottom: 60px;
  max-width: 800px;

  @media (max-width: 450px) {
    max-width: 350px;
    padding: 0 16px 0px 0;
  }
`
const SectionCards = styled.div`
  display: grid;
  grid-template-columns: 450px 450px 450px;
  grid-gap: 12px;
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
    padding: 40px 10px;
  }
`
const BenefitsWrapper = styled.div`
  width: 385px;
  height: 180px;

  @media (max-width: 450px) {
    max-width: 320px;
    margin: 20px 16px;
  }
`
const BenefitsContent = styled.div`
  margin: -120px 0px 0px 50px;
`
const BenefitsIcon = styled.div``
const BenefitsTitle = styled(H3)``
const BenefitsDescription = styled(P)``

//LOGOS WRAPPER
const BrandSectionWrapper = styled.div`
  margin: 200px auto 200px auto;
  @media (max-width: 450px) {
    margin: 0px auto 40px auto;
  }
`
const BrandsSectionTitle = styled(H2)`
  padding: 0px 0px 0px 120px;

  @media (max-width: 450px) {
    max-width: 350px;
    padding: 40px 16px;
  }
`
const MiddleCtaButton = styled.div`
  padding: 0px 0px 20px 120px;
  @media (max-width: 450px) {
    max-width: 350px;
    padding: 0px 16px;
  }
`
const LogosWrapper = styled.div`
  max-width: 1280px;
  padding: 20px 0px 0px 120px;
  display: grid;
  grid-template-columns: repeat(5, auto);
  grid-gap: 10px;

  @media (max-width: 450px) {
    display: grid;
    padding: 40px 0 0 0;
    grid-template-columns: auto auto;
  }
`
const Logo = styled.div`
  padding: 0;
`

//Body content styling
const BodySectionWrapper = styled.div`
  max-width: 1280px;

  margin: 200px auto 240px auto;
  @media (max-width: 450px) {
    margin: 40px auto 40px auto;
  }
`
const BodyContentWrapper = styled.div`
  margin: 0px auto 120px auto;
  display: grid;
  grid-template-columns: 562px 552px;
  grid-gap: 120px;

  @media (max-width: 450px) {
    padding: 0px 40px 0 40px;
    margin: 0;
    display: flex;
    flex-direction: column-reverse;
    gap: 30px;
  }
`
const BodyContentWrapperTwo = styled.div`
  margin: 0px auto 120px auto;
  display: grid;
  grid-template-columns: 562px 552px;
  grid-gap: 120px;

  @media (max-width: 450px) {
    padding: 0px 40px 0 40px;
    margin: 0;
    display: grid;
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
`
const ProductPicture = styled.div`
  @media (max-width: 450px) {
    width: auto;
  }
`
const ProductPictureTwo = styled.div`
  @media (max-width: 450px) {
    width: auto;
  }
`
const BodyContentDescription = styled.div`
  padding: 20px 0 0 0;
  @media (max-width: 450px) {
    padding: 0;
  }
`
const BodyContentTitle = styled(H3)`
  max-width: 480px;
`
const BodyContentTextBlock = styled.div`
  max-width: 480px;
`

const TextCta = styled.p`
  font-weight: 600;
  font-size: 1.25em;
  color: #febd55 !important;
  cursor: pointer;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`

const ArrowButton = styled.img`
  padding-left: 8px;
`

//Bottom Call To Action
const BottomCallToAction = styled.div`
  position: relative;
  display: grid;
  max-width: 72em;
  margin: 0px 0px 320px 0px;
  padding: 0;
  @media (max-width: 830px) {
    width: 600px;
  }
  @media (max-width: 450px) {
    max-width: 360px;
    margin: 100px 0px;
  }
`
const CtaBackground = styled.div`
  position: absolute;
  z-index: 1;
  align-self: center;
`

const CtaContent = styled.div`
  position: relative;
  display: grid;
  max-width: 72em;
  background: #124e5d;
  box-shadow: 0 0.5em 2em #2c2c2c18;
  margin: 0 0 4em 0;
  padding: 1.5em 8em 2.25em 8em;
  @media (max-width: 32em) {
    width: 24em;
    margin: 0 0 4em 0;
    padding: 1em 2em 3em 2em;
  }
  @media (max-width: 450px) {
    max-width: 360px;
  }
`
const Tagline = styled(H2)`
  padding: 10px 0px 0px 0px;
  max-width: 800px;
  @media (max-width: 450px) {
    padding: 10px 0px 40px 0px;
  }
`
const CtaText = styled.div`
  position: relative;
  align-self: center;
  top: -2em;
  color: #2a2a2a;

  padding: 50px 0 50px 0;
  z-index: 2;
  h2 {
    font-size: 3.25em;
  }
  @media (max-width: 32em) {
    width: 24em;
    margin: 0;
    padding: 0;
    h2 {
      font-size: 2.5em;
      margin: 1.2em 1.2em 1em 0;
    }
  }
  @media (max-width: 450px) {
    max-width: 360px;
    margin: 0;
    padding: 0;
  }
`

const CtaGroup = styled.div`
  margin: -30px 0 0 0;

  display: flex;

  flex-direction: row;

  grid-gap: 20px;
  @media (max-width: 450px) {
    flex-direction: column;
  }
`
const ListGroup = styled.ul``
const ListItem = styled.li`
  list-style-image: url("/images/green-check-mark.svg");
`

const TrialGroup = styled.div``
