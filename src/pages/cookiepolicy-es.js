import React, { Component } from "react"
import { BLOCKS } from "@contentful/rich-text-types"
import { graphql } from "gatsby"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"

import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"

import NewHeaderES from "../components/sections/HeaderNewES.js"

import FooterES from "../components/sections/FooterES.js"
import { H1, H2, H3, P } from "../components/styles/TextStyles.js"
import styled from "styled-components"

class CookiePolicyES extends Component {
  render() {
    const { data } = this.props
    const terms = data.allContentfulCookiePolicy.edges[0]
    const options = {
      renderNode: {
        [BLOCKS.HEADING_1]: (node, children) => <H1>{children}</H1>,
        [BLOCKS.HEADING_2]: (node, children) => <H2>{children}</H2>,
        [BLOCKS.HEADING_3]: (node, children) => <H3>{children}</H3>,
        [BLOCKS.PARAGRAPH]: (node, children) => <P>{children}</P>,
      },
      renderMark: {},
    }
    return (
      <Layout>
        <SEO
          title={terms.node.pageName}
          description={terms.node.pageDescription}
        />
        <NewHeaderES
          en="/cookiepolicy"
          fr="/cookiepolicy-fr"
          es="/cookiepolicy-es"
        />
        <Body>
          <BodyGroup>
            {documentToReactComponents(terms.node.content.json, options)}
          </BodyGroup>
        </Body>
        <FooterES />
      </Layout>
    )
  }
}

export default CookiePolicyES
export const CookiePolicyQuery = graphql`
  {
    allContentfulCookiePolicy(filter: { language: { eq: "ES" } }) {
      edges {
        node {
          content {
            json
          }
          pageName
          pageDescription
        }
      }
    }
  }
`
const Body = styled.div`
  display: grid;
  justify-items: center;
  justify-content: center;
  margin: 0;
  padding: 0;
`

const BodyGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(1, auto);
  justify-self: left;
  max-width: 80em;
  margin: 10em auto 4em 8em;
  padding: 0;
  img {
    margin: 1em 1em 1em 0;
    padding: 0;
    width: 39em;
  }

  @media (max-width: 450px) {
    margin: 120px 40px;
  }
`
