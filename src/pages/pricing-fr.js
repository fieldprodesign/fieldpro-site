import React from "react"
import { BLOCKS } from "@contentful/rich-text-types"
import { graphql, Link } from "gatsby"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"

import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"
import styled from "styled-components"

import NewHeaderFR from "../components/sections/HeaderNewFR.js"
import FooterFR from "../components/sections/FooterFR.js"
import CookiesBar from "../components/sections/cookies.js"

import { H1, H2, H3, P } from "../components/styles/TextStyles.js"

import {
  ScheduleButtonDark,
  DownloadButtonDarkArrow,
  DownloadButtonArrow,
} from "../components/Buttons/CtaButton.js"

import {
  TooltipText,
  StarterDetails2,
  HeroSectionWrapper,
  HeroContent,
  HeroTitle,
  HeroDescription,
  PriceTableWrapper,
  PriceDetail,
  PricingFeauture,
  FeautureDetailGrouplast,
  FeautureGroup,
  FeautureDetail,
  FeautureToolTip,
  FeautureTitle,
  PricingPro,
  StarterDescription,
  PricingStarterMobile,
  PricingProMobile,
  StarterTitle,
  PricingStarter,
  StarterPrice,
  ProPrice,
  StarterDetails,
  FeautureDetailGroup,
  FeautureDetailMobile,
  PriceFollowText,
  BottomSectionWrapper,
  BrandsTitle,
  BrandsLogoWrapper,
  BrandLogo,
  BottomCallToAction,
  CtaBackground,
  CtaContent,
  TrialGroup,
  CtaTagline,
  CtaButtons,
  CtaWrapper,
  CtaIllustartion,
  DisclaimerText,
  FeautureDetailGroupTwo,
  StarterDescriptionClose,
} from "../pages/pricing.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"

class Pricing extends React.Component {
  constructor(props) {
    super(props)
    // this.state = { loading: true }
    this.state = { fetchedData: "" }
    this.state = { value: "" }
    this.state = { showToolTip: false }
    this.state = { showToolTip2: false }
    this.state = { showToolTip3: false }
    this.state = { showToolTip4: false }
    this.state = { showToolTip5: false }
    this.state = { showToolTip6: false }
    this.state = { showToolTip7: false }
    this.state = { showToolTip8: false }
    this.state = { showToolTip9: false }
    this.state = { showToolTip10: false }
    this.state = { showToolTip11: false }
    this.state = { showToolTip12: false }
    this.state = { showToolTip13: false }
    this.state = { showToolTip14: false }
    this.state = { showToolTip15: false }
    this.state = { showToolTip16: false }
    this.state = { showToolTip17: false }
    this.state = { showToolTip18: false }
    this.state = { showToolTip19: false }

    this.handleToolTip = this.handleToolTip.bind(this)
    this.handleToolTipHide = this.handleToolTipHide.bind(this)
    this.handleToolTip2 = this.handleToolTip2.bind(this)
    this.handleToolTipHide2 = this.handleToolTipHide2.bind(this)
    this.handleToolTip3 = this.handleToolTip3.bind(this)
    this.handleToolTipHide3 = this.handleToolTipHide3.bind(this)
    this.handleToolTip4 = this.handleToolTip4.bind(this)
    this.handleToolTipHide4 = this.handleToolTipHide4.bind(this)
    this.handleToolTip5 = this.handleToolTip5.bind(this)
    this.handleToolTipHide5 = this.handleToolTipHide5.bind(this)
    this.handleToolTip6 = this.handleToolTip6.bind(this)
    this.handleToolTipHide6 = this.handleToolTipHide6.bind(this)
    this.handleToolTip7 = this.handleToolTip7.bind(this)
    this.handleToolTipHide7 = this.handleToolTipHide7.bind(this)
    this.handleToolTip8 = this.handleToolTip8.bind(this)
    this.handleToolTipHide8 = this.handleToolTipHide8.bind(this)
    this.handleToolTip9 = this.handleToolTip9.bind(this)
    this.handleToolTipHide9 = this.handleToolTipHide9.bind(this)
    this.handleToolTip10 = this.handleToolTip10.bind(this)
    this.handleToolTipHide10 = this.handleToolTipHide10.bind(this)
    this.handleToolTip11 = this.handleToolTip11.bind(this)
    this.handleToolTipHide11 = this.handleToolTipHide11.bind(this)
    this.handleToolTip12 = this.handleToolTip12.bind(this)
    this.handleToolTipHide12 = this.handleToolTipHide12.bind(this)
    this.handleToolTip13 = this.handleToolTip13.bind(this)
    this.handleToolTipHide13 = this.handleToolTipHide13.bind(this)
    this.handleToolTip14 = this.handleToolTip14.bind(this)
    this.handleToolTipHide14 = this.handleToolTipHide14.bind(this)
    this.handleToolTip15 = this.handleToolTip15.bind(this)
    this.handleToolTipHide15 = this.handleToolTipHide15.bind(this)
    this.handleToolTip16 = this.handleToolTip16.bind(this)
    this.handleToolTipHide16 = this.handleToolTipHide16.bind(this)
    this.handleToolTip17 = this.handleToolTip17.bind(this)
    this.handleToolTipHide17 = this.handleToolTipHide17.bind(this)
    this.handleToolTip18 = this.handleToolTip18.bind(this)
    this.handleToolTipHide18 = this.handleToolTipHide18.bind(this)
    this.handleToolTip19 = this.handleToolTip19.bind(this)
    this.handleToolTipHide19 = this.handleToolTipHide19.bind(this)
  }

  handleToolTip(event) {
    //Show ToolTip

    this.setState({
      showToolTip: true,
    })
  }
  handleToolTipHide(event) {
    //Hide ToolTip
    this.setState({
      showToolTip: false,
    })
  }
  handleToolTip2(event) {
    //Show ToolTip

    this.setState({
      showToolTip2: true,
    })
  }
  handleToolTipHide2(event) {
    //Hide ToolTip
    this.setState({
      showToolTip2: false,
    })
  }

  handleToolTipHide3(event) {
    //Hide ToolTip
    this.setState({
      showToolTip3: false,
    })
  }
  handleToolTip3(event) {
    //Show ToolTip

    this.setState({
      showToolTip3: true,
    })
  }
  handleToolTip4(event) {
    //Show ToolTip

    this.setState({
      showToolTip4: true,
    })
  }
  handleToolTipHide4(event) {
    //Hide ToolTip
    this.setState({
      showToolTip4: false,
    })
  }
  handleToolTip5(event) {
    //Show ToolTip

    this.setState({
      showToolTip5: true,
    })
  }
  handleToolTipHide5(event) {
    //Hide ToolTip
    this.setState({
      showToolTip5: false,
    })
  }
  handleToolTip6(event) {
    //Show ToolTip

    this.setState({
      showToolTip6: true,
    })
  }
  handleToolTipHide6(event) {
    //Hide ToolTip
    this.setState({
      showToolTip6: false,
    })
  }
  handleToolTip7(event) {
    //Show ToolTip

    this.setState({
      showToolTip7: true,
    })
  }
  handleToolTipHide7(event) {
    //Hide ToolTip
    this.setState({
      showToolTip7: false,
    })
  }
  handleToolTip8(event) {
    //Show ToolTip

    this.setState({
      showToolTip8: true,
    })
  }
  handleToolTipHide8(event) {
    //Hide ToolTip
    this.setState({
      showToolTip8: false,
    })
  }
  handleToolTip9(event) {
    //Show ToolTip

    this.setState({
      showToolTip9: true,
    })
  }
  handleToolTipHide9(event) {
    //Hide ToolTip
    this.setState({
      showToolTip9: false,
    })
  }
  handleToolTip10(event) {
    //Show ToolTip

    this.setState({
      showToolTip10: true,
    })
  }
  handleToolTipHide10(event) {
    //Hide ToolTip
    this.setState({
      showToolTip10: false,
    })
  }
  handleToolTip11(event) {
    //Show ToolTip

    this.setState({
      showToolTip11: true,
    })
  }
  handleToolTipHide11(event) {
    //Hide ToolTip
    this.setState({
      showToolTip11: false,
    })
  }
  handleToolTip12(event) {
    //Show ToolTip

    this.setState({
      showToolTip12: true,
    })
  }
  handleToolTipHide12(event) {
    //Hide ToolTip
    this.setState({
      showToolTip12: false,
    })
  }
  handleToolTip13(event) {
    //Show ToolTip

    this.setState({
      showToolTip13: true,
    })
  }
  handleToolTipHide13(event) {
    //Hide ToolTip
    this.setState({
      showToolTip13: false,
    })
  }
  handleToolTip14(event) {
    //Show ToolTip

    this.setState({
      showToolTip14: true,
    })
  }
  handleToolTipHide14(event) {
    //Hide ToolTip
    this.setState({
      showToolTip14: false,
    })
  }
  handleToolTip15(event) {
    //Show ToolTip

    this.setState({
      showToolTip15: true,
    })
  }
  handleToolTipHide15(event) {
    //Hide ToolTip
    this.setState({
      showToolTip15: false,
    })
  }
  handleToolTip16(event) {
    //Show ToolTip

    this.setState({
      showToolTip16: true,
    })
  }
  handleToolTipHide16(event) {
    //Hide ToolTip
    this.setState({
      showToolTip16: false,
    })
  }
  handleToolTip17(event) {
    //Show ToolTip

    this.setState({
      showToolTip17: true,
    })
  }
  handleToolTipHide17(event) {
    //Hide ToolTip
    this.setState({
      showToolTip17: false,
    })
  }
  handleToolTip18(event) {
    //Show ToolTip

    this.setState({
      showToolTip18: true,
    })
  }
  handleToolTipHide18(event) {
    //Hide ToolTip
    this.setState({
      showToolTip18: false,
    })
  }
  handleToolTip19(event) {
    //Show ToolTip

    this.setState({
      showToolTip19: true,
    })
  }
  handleToolTipHide19(event) {
    //Hide ToolTip
    this.setState({
      showToolTip19: false,
    })
  }
  componentDidMount() {
    fetch(`http://ip-api.com/json/?fields=continent`)
      .then(response => {
        return response.json()
      })
      .then(json => {
        this.setState({
          fetchedData: json.continent,
        })
      })
  }

  render() {
    const { fetchedData } = this.state
    const { data } = this.props
    const pricingContent = data.allContentfulPricingPage.edges[0]
    const brandsLogo = data.allContentfulPricingPage.edges[0].node.brandsLogos
    const options = {
      renderNode: {
        [BLOCKS.HEADING_1]: (node, children) => <H1>{children}</H1>,
        [BLOCKS.HEADING_2]: (node, children) => <H2>{children}</H2>,
        [BLOCKS.HEADING_3]: (node, children) => <H3>{children}</H3>,
        [BLOCKS.PARAGRAPH]: (node, children) => <P>{children}</P>,
      },
      renderMark: {},
    }
    const Tooltip = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture1Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltiptwo = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture2Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipthree = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture3Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipfour = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture4Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipfive = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture5Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipsix = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture6Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipseven = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture7Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipeight = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture8Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipnine = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture9Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipten = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture10Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipeleven = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture11Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltiptwelve = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture12Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipthirteen = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture13Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipfourteen = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture14Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipfifteen = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture15Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipsixteen = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture16Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipseventeen = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture17Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipeighteen = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture18Tooltip}</TooltipText>
      </TooltipContainer>
    )
    const Tooltipnineteen = (
      <TooltipContainer>
        <TooltipText>{pricingContent.node.feauture19Tooltip}</TooltipText>
      </TooltipContainer>
    )
    var starterPrice
    var proPrice
    if (fetchedData === "Europe") {
      starterPrice = <StarterPrice>10€</StarterPrice>
      proPrice = <ProPrice>25€</ProPrice>
    } else {
      starterPrice = <StarterPrice>10$</StarterPrice>

      proPrice = <ProPrice>25$</ProPrice>
    }

    return (
      <Layout>
        <SEO
          title={pricingContent.node.pageName}
          description={pricingContent.node.pageDescription}
        />
        <NewHeaderFR en="/pricing" fr="/pricing-fr" es="/pricing-es" />
        <HeroSectionWrapper>
          <HeroContent>
            <HeroTitle>{pricingContent.node.bannerTitle}</HeroTitle>
            <HeroDescription>{pricingContent.node.bannerText}</HeroDescription>
          </HeroContent>
        </HeroSectionWrapper>
        <PriceTableWrapper>
          <PriceDetail>
            <PricingFeauture>
              <FeautureTitle>{pricingContent.node.feautureTitle}</FeautureTitle>
              <FeautureDetailGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture1}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip}
                    onMouseOut={this.handleToolTipHide}
                    ref={this.myRef}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip ? Tooltip : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture2}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip2}
                    onMouseOut={this.handleToolTipHide2}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip2 ? Tooltiptwo : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {" "}
                    {pricingContent.node.feauture3}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip3}
                    onMouseOut={this.handleToolTipHide3}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip3 ? Tooltipthree : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture4}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip4}
                    onMouseOut={this.handleToolTipHide4}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip4 ? Tooltipfour : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture5}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip5}
                    onMouseOut={this.handleToolTipHide5}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip5 ? Tooltipfive : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture6}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip6}
                    onMouseOut={this.handleToolTipHide6}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip6 ? Tooltipsix : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture7}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip7}
                    onMouseOut={this.handleToolTipHide7}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip7 ? Tooltipseven : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture8}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip8}
                    onMouseOut={this.handleToolTipHide8}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip8 ? Tooltipeight : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture9}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip9}
                    onMouseOut={this.handleToolTipHide9}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip9 ? Tooltipnine : ""}
                  </FeautureToolTip>
                </FeautureGroup>
              </FeautureDetailGroup>
              <FeautureDetailGroupTwo>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture10}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip10}
                    onMouseOut={this.handleToolTipHide10}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip10 ? Tooltipten : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture11}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip11}
                    onMouseOut={this.handleToolTipHide11}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip11 ? Tooltipeleven : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture12}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip12}
                    onMouseOut={this.handleToolTipHide12}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip12 ? Tooltiptwelve : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {" "}
                    {pricingContent.node.feauture13}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip13}
                    onMouseOut={this.handleToolTipHide13}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip13 ? Tooltipthirteen : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture14}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip14}
                    onMouseOut={this.handleToolTipHide14}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip14 ? Tooltipfourteen : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture15}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip15}
                    onMouseOut={this.handleToolTipHide15}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip15 ? Tooltipfifteen : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture16}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip16}
                    onMouseOut={this.handleToolTipHide16}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip16 ? Tooltipsixteen : ""}
                  </FeautureToolTip>
                </FeautureGroup>
              </FeautureDetailGroupTwo>
              <FeautureDetailGrouplast>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture17}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip17}
                    onMouseOut={this.handleToolTipHide17}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip17 ? Tooltipseventeen : ""}
                  </FeautureToolTip>
                </FeautureGroup>
                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture18}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip18}
                    onMouseOut={this.handleToolTipHide18}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip18 ? Tooltipeighteen : ""}
                  </FeautureToolTip>
                </FeautureGroup>

                <FeautureGroup>
                  <FeautureDetail>
                    {pricingContent.node.feauture19}
                  </FeautureDetail>
                  <FeautureToolTip
                    onMouseOver={this.handleToolTip19}
                    onMouseOut={this.handleToolTipHide19}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="20"
                      viewBox="0 0 24 24"
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path
                        class="tooltip-icon-1"
                        fill="#c4c4c4"
                        d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
                      />
                    </svg>
                    {this.state.showToolTip19 ? Tooltipnineteen : ""}
                  </FeautureToolTip>
                </FeautureGroup>
              </FeautureDetailGrouplast>
            </PricingFeauture>
            <PricingStarter>
              <StarterTitle>STARTER</StarterTitle>
              <StarterPrice>10€</StarterPrice>
              <StarterDetails> utilisateur mobile / mois</StarterDetails>
              <StarterDetails2> 5 utilisateurs mobiles min</StarterDetails2>
              <FeautureDetailGroup>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
              </FeautureDetailGroup>
              <FeautureDetailGroupTwo>
                <StarterDescriptionClose>
                  <img src="/images/close_icon.png" alt="Pricing Details" />
                </StarterDescriptionClose>
                <StarterDescriptionClose>
                  <img src="/images/close_icon.png" alt="Pricing Details" />
                </StarterDescriptionClose>

                <StarterDescriptionClose>
                  <img src="/images/close_icon.png" alt="Pricing Details" />
                </StarterDescriptionClose>
                <StarterDescriptionClose>
                  <img src="/images/close_icon.png" alt="Pricing Details" />
                </StarterDescriptionClose>
                <StarterDescriptionClose>
                  <img src="/images/close_icon.png" alt="Pricing Details" />
                </StarterDescriptionClose>
                <StarterDescriptionClose>
                  <img src="/images/close_icon.png" alt="Pricing Details" />
                </StarterDescriptionClose>
                <StarterDescriptionClose>
                  <img src="/images/close_icon.png" alt="Pricing Details" />
                </StarterDescriptionClose>
              </FeautureDetailGroupTwo>
              <FeautureDetailGrouplast>
                <StarterDescription>
                  <img src="/images/close_icon.png" alt="Pricing Details" />
                </StarterDescription>
                <StarterDescription>
                  <img src="/images/close_icon.png" alt="Pricing Details" />
                </StarterDescription>
                <StarterDescription>
                  <img src="/images/close_icon.png" alt="Pricing Details" />
                </StarterDescription>
              </FeautureDetailGrouplast>
              <Link to="https://web.v3.fieldproapp.com/welcome">
                <DownloadButtonArrow id="start_trial">
                  Essai gratuit
                </DownloadButtonArrow>
              </Link>
            </PricingStarter>
            <PricingPro>
              <StarterTitle>PRO</StarterTitle>
              <ProPrice>25€</ProPrice>
              <StarterDetails>/ utilisateur mobile / mois</StarterDetails>
              <StarterDetails2> 20 utilisateurs mobiles min</StarterDetails2>
              <FeautureDetailGroup>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
              </FeautureDetailGroup>
              <FeautureDetailGroupTwo>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>

                <StarterDescription>
                  <img
                    src="/images/green-check-mark.png"
                    alt="Pricing Details"
                  />
                </StarterDescription>
              </FeautureDetailGroupTwo>
              <FeautureDetailGrouplast>
                <StarterDescription
                  style={{ padding: "0px 0px 20px 0px", fontWeight: "800" }}
                >
                  Discutons en
                </StarterDescription>
                <StarterDescription
                  style={{ padding: "0px 0px 20px 0px", fontWeight: "800" }}
                >
                  Discutons en
                </StarterDescription>

                <StarterDescription
                  style={{ padding: "0px 0px 30px 0px", fontWeight: "800" }}
                >
                  Discutons en
                </StarterDescription>
              </FeautureDetailGrouplast>
              <Link to="https://web.v3.fieldproapp.com/welcome">
                <DownloadButtonArrow id="start_trial">
                  Essai gratuit
                </DownloadButtonArrow>
              </Link>
            </PricingPro>
          </PriceDetail>

          <PricingStarterMobile>
            <StarterTitle>STARTER</StarterTitle>
            {starterPrice}
            <StarterDetails> / utilisateur mobile / mois</StarterDetails>
            <StarterDetails2> 5 utilisateurs mobiles min</StarterDetails2>
            <FeautureDetailGroup>
              <FeautureDetailMobile>
                {pricingContent.node.feauture1}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture2}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {" "}
                {pricingContent.node.feauture3}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture4}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture5}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture6}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture7}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture8}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {" "}
                {pricingContent.node.feauture9}
              </FeautureDetailMobile>
            </FeautureDetailGroup>
            <Link to="https://web.v3.fieldproapp.com/welcome">
              <DownloadButtonArrow id="start_trial">
                Essai gratuit
              </DownloadButtonArrow>
            </Link>
          </PricingStarterMobile>
          <PricingProMobile>
            <StarterTitle>PRO</StarterTitle>
            {proPrice}
            <StarterDetails> / utilisateur mobile / mois</StarterDetails>
            <StarterDetails2> 20 utilisateurs mobiles min</StarterDetails2>

            <FeautureDetailGroup>
              <FeautureDetailMobile>
                {pricingContent.node.feauture1}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture2}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {" "}
                {pricingContent.node.feauture3}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture4}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture5}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture6}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture7}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture8}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {" "}
                {pricingContent.node.feauture9}{" "}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {" "}
                {pricingContent.node.feauture10}
              </FeautureDetailMobile>
            </FeautureDetailGroup>
            <FeautureDetailGroup>
              <FeautureDetailMobile>
                {pricingContent.node.feauture11}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture12}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {" "}
                {pricingContent.node.feauture13}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture14}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture15}
              </FeautureDetailMobile>
            </FeautureDetailGroup>

            <FeautureDetailGroup>
              <FeautureDetailMobile>
                {pricingContent.node.feauture16}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {pricingContent.node.feauture17}
              </FeautureDetailMobile>
              <FeautureDetailMobile>
                {" "}
                {pricingContent.node.feauture18}
              </FeautureDetailMobile>{" "}
              <FeautureDetailMobile>
                {pricingContent.node.feauture19}
              </FeautureDetailMobile>
            </FeautureDetailGroup>
            <Link to="https://web.v3.fieldproapp.com/welcome">
              <DownloadButtonArrow id="start_trial">
                Essai gratuit
              </DownloadButtonArrow>
            </Link>
          </PricingProMobile>

          <PriceFollowText>
            {documentToReactComponents(
              pricingContent.node.additionalDetails.json,
              options
            )}
          </PriceFollowText>
        </PriceTableWrapper>
        <BottomSectionWrapper>
          <BrandsTitle>{pricingContent.node.brandsTitle}</BrandsTitle>
          <BrandsLogoWrapper>
            {brandsLogo.map(companyLogo => {
              return (
                <BrandLogo>
                  <img src={companyLogo.fluid.src} alt="" />
                </BrandLogo>
              )
            })}
          </BrandsLogoWrapper>{" "}
        </BottomSectionWrapper>

        <BottomCallToAction>
          <CtaBackground>
            <CtaIllustartion>
              <img
                src={pricingContent.node.ctaBgIllustration.fluid.src}
                style={{ width: "100vw" }}
                alt=""
              />
            </CtaIllustartion>
            <CtaWrapper>
              <CtaContent>
                <CtaTagline>{pricingContent.node.callToAction}</CtaTagline>
                <CtaButtons>
                  <TrialGroup>
                    <Link to="https://web.v3.fieldproapp.com/welcome">
                      <DownloadButtonDarkArrow id="start_trial">
                        Essai gratuit
                      </DownloadButtonDarkArrow>
                    </Link>

                    <DisclaimerText> *Sans engagement</DisclaimerText>
                  </TrialGroup>
                  <Link to="/book-a-demo-fr">
                    <ScheduleButtonDark id="bookdemo">
                      Demander une démo
                    </ScheduleButtonDark>
                  </Link>
                </CtaButtons>
              </CtaContent>
            </CtaWrapper>
          </CtaBackground>
        </BottomCallToAction>
        <FooterFR />
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}
export default Pricing
export const query = graphql`
  {
    allContentfulPricingPage(filter: { language: { eq: "FR" } }) {
      edges {
        node {
          additionalDetails {
            json
          }
          bannerText
          bannerTitle
          brandsLogos {
            fluid {
              src
            }
          }
          brandsTitle
          callToAction
          ctaBgIllustration {
            fluid {
              src
            }
          }
          feauture1
          feauture10
          feauture10Tooltip
          feauture11
          feauture11Tooltip
          feauture12
          feauture12Tooltip
          feauture13
          feauture13Tooltip
          feauture14
          feauture14Tooltip
          feauture15
          feauture15Tooltip
          feauture16
          feauture16Tooltip
          feauture17
          feauture17Tooltip
          feauture18
          feauture18Tooltip
          feauture19
          feauture19Tooltip
          feauture1Tooltip
          feauture2
          feauture2Tooltip
          feauture3
          feauture3Tooltip
          feauture4
          feauture4Tooltip
          feauture5
          feauture5Tooltip
          feauture6
          feauture6Tooltip
          feauture7
          feauture7Tooltip
          feauture8
          feauture8Tooltip
          feauture9
          feauture9Tooltip
          feautureTitle
          pageDescription
          pageName
        }
      }
    }
  }
`
const TooltipContainer = styled.div`
  width: 420px;
  height: 96px;
  box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.08);
  background-color: #fefefe;
  position: absolute;
  left: 40px;
  top: -40px;
  padding: 10px 5px 5px 10px;
`
