import React from "react"
import { graphql, Link } from "gatsby"

import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"
import NewHeaderFR from "../components/sections/HeaderNewFR.js"
import FooterFR from "../components/sections/FooterFR.js"
import Brands from "../components/Testimonial-components/Brands.js"
import HeroSectionFR from "../components/Testimonial-components/HeroSectionFR.js"
import CookiesBar from "../components/sections/cookies.js"

import {
  DownloadButtonDarkArrow,
  ScheduleButtonDark,
} from "../components/Buttons/CtaButton.js"

import {
  Wrapper,
  Description,
  Title,
  TextBlock,
  Quotation,
  Quote,
  Citation,
  Profile,
  Details,
  PersonName,
  Position,
  CompanyName,
  TextCta,
  ArrowButton,
  CompanyPicture,
  BottomCallToAction,
  CtaBackground,
  CtaContent,
  CtaIllustartion,
  CtaWrapper,
  CtaTagline,
  CtaButtons,
  TrialGroup,
  DisclaimerText,
} from "../pages/testimonials.js"

import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"
class TestimonialCardFR extends React.Component {
  render() {
    const { data } = this.props
    const testimonials = data.allContentfulTestimonial.edges
    return (
      <Layout>
        <SEO title="Témoignages" />
        <NewHeaderFR
          en="/testimonials"
          fr="/testimonials-fr"
          es="/testimonials-es"
        />
        <HeroSectionFR />
        <Brands />

        {testimonials.map((node, index) => {
          if (index % 2 === 0) {
            return (
              <Wrapper>
                <CompanyPicture>
                  <img
                    src={node.node.testimonialImage.fluid.src}
                    alt="Company Logo"
                  />
                </CompanyPicture>
                <Description>
                  <Title>{node.node.testimonialTitle}</Title>
                  <TextBlock>
                    <Quotation>"</Quotation>
                    <Quote>{node.node.quote.quote}"</Quote>
                  </TextBlock>
                  <Citation>
                    <Profile>
                      <img
                        src={node.node.citationImage.fluid.src}
                        alt="Citation"
                      />
                    </Profile>
                    <Details>
                      <PersonName>{node.node.citationName}</PersonName>
                      <Position>{node.node.citationPosition} </Position>
                      <CompanyName>{node.node.citationCompany}</CompanyName>
                    </Details>
                  </Citation>
                  <TextCta>
                    <a
                      style={{ color: "#febd55" }}
                      href={
                        node.node.testimonialLink.json.content[0].content[1]
                          .data.uri
                      }
                    >
                      {
                        node.node.testimonialLink.json.content[0].content[1]
                          .content[0].value
                      }
                    </a>
                    <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                  </TextCta>
                </Description>
              </Wrapper>
            )
          } else {
            return (
              <Wrapper>
                <Description>
                  <Title>{node.node.testimonialTitle}</Title>
                  <TextBlock>
                    <Quotation>"</Quotation>
                    <Quote>{node.node.quote.quote}"</Quote>
                  </TextBlock>
                  <Citation>
                    <Profile>
                      <img
                        src={node.node.citationImage.fluid.src}
                        alt="Citation"
                      />
                    </Profile>
                    <Details>
                      <PersonName>{node.node.citationName}</PersonName>
                      <Position>{node.node.citationPosition} </Position>
                      <CompanyName>{node.node.citationCompany}</CompanyName>
                    </Details>
                  </Citation>
                  <TextCta>
                    <a
                      style={{ color: "#febd55" }}
                      href={
                        node.node.testimonialLink.json.content[0].content[1]
                          .data.uri
                      }
                    >
                      {
                        node.node.testimonialLink.json.content[0].content[1]
                          .content[0].value
                      }
                    </a>
                    <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                  </TextCta>
                </Description>
                <CompanyPicture>
                  <img
                    src={node.node.testimonialImage.fluid.src}
                    alt="Company"
                  />
                </CompanyPicture>
              </Wrapper>
            )
          }
        })}

        <BottomCallToAction>
          <CtaBackground>
            <CtaIllustartion>
              <img
                src="/images/Geo-location.svg"
                style={{ width: "100vw" }}
                alt=""
              />
            </CtaIllustartion>
            <CtaWrapper>
              <CtaContent>
                <CtaTagline>Faites partie de nos clients</CtaTagline>
                <CtaButtons>
                  <TrialGroup>
                    <Link to="https://web.v3.fieldproapp.com/welcome">
                      <DownloadButtonDarkArrow id="start_trial">
                        Essai gratuit
                      </DownloadButtonDarkArrow>
                    </Link>

                    <DisclaimerText> *Sans engagement</DisclaimerText>
                  </TrialGroup>
                  <Link to="/book-a-demo-fr">
                    <ScheduleButtonDark id="bookdemo">
                      Demander une démo
                    </ScheduleButtonDark>
                  </Link>
                </CtaButtons>
              </CtaContent>
            </CtaWrapper>
          </CtaBackground>
        </BottomCallToAction>

        <FooterFR />
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}
export default TestimonialCardFR
export const testimonialCardQuery = graphql`
  {
    allContentfulTestimonial(filter: { language: { eq: "FR" } }) {
      edges {
        node {
          testimonialImage {
            fluid {
              src
            }
          }
          testimonialTitle
          quote {
            quote
          }
          citationImage {
            fluid {
              src
            }
          }
          citationName
          citationPosition
          citationCompany
          testimonialLink {
            json
          }
        }
      }
    }
  }
`
