import React from "react"
import { graphql, Link } from "gatsby"
import styled from "styled-components"

import SEO from "../components/seo.js"
import Layout from "../components/Layout.js"
import NewHeader from "../components/sections/HeaderNew.js"
import Footer from "../components/sections/Footer.js"
import HeroSection from "../components/Testimonial-components/HeroSection.js"
import Brands from "../components/Testimonial-components/Brands.js"
import CookiesBar from "../components/sections/cookies.js"

import { H3, H2, QuoteText, Caption } from "../components/styles/TextStyles.js"
import {
  DownloadButtonDarkArrow,
  ScheduleButtonDark,
} from "../components/Buttons/CtaButton.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"

class TestimonialCard extends React.Component {
  render() {
    /* */
    const { data } = this.props
    const testimonials = data.allContentfulTestimonial.edges
    const seo = data.allContentfulTestimonialsPageSeo.edges[0]

    return (
      <Layout>
        <SEO title={seo.node.titleEN} description={seo.node.descriptionEN} />

        <NewHeader
          en="/testimonials"
          fr="/testimonials-fr"
          es="/testimonials-es"
        />
        <HeroSection />
        <Brands />
        {testimonials.map((node, index) => {
          if (index % 2 === 0) {
            return (
              <Wrapper>
                <CompanyPicture>
                  <img
                    src={node.node.testimonialImage.fluid.src}
                    alt="Company Logo"
                  />
                </CompanyPicture>
                <Description>
                  <Title>{node.node.testimonialTitle}</Title>
                  <TextBlock>
                    <Quotation>"</Quotation>
                    <Quote>{node.node.quote.quote}"</Quote>
                  </TextBlock>
                  <Citation>
                    <Profile>
                      <img
                        src={node.node.citationImage.fluid.src}
                        alt="Citation"
                      />
                    </Profile>
                    <Details>
                      <PersonName>{node.node.citationName}</PersonName>
                      <Position>{node.node.citationPosition} </Position>
                      <CompanyName>{node.node.citationCompany}</CompanyName>
                    </Details>
                  </Citation>
                  <TextCta>
                    <a
                      style={{ color: "#febd55" }}
                      href={
                        node.node.testimonialLink.json.content[0].content[1]
                          .data.uri
                      }
                    >
                      {
                        node.node.testimonialLink.json.content[0].content[1]
                          .content[0].value
                      }
                    </a>
                    <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                  </TextCta>
                </Description>
              </Wrapper>
            )
          } else {
            return (
              <Wrapper>
                <Description>
                  <Title>{node.node.testimonialTitle}</Title>

                  <TextBlock>
                    <Quotation>"</Quotation>
                    <Quote>{node.node.quote.quote}"</Quote>
                  </TextBlock>
                  <Citation>
                    <Profile>
                      <img
                        src={node.node.citationImage.fluid.src}
                        alt="Citation"
                      />
                    </Profile>
                    <Details>
                      <PersonName>{node.node.citationName}</PersonName>
                      <Position>{node.node.citationPosition} </Position>
                      <CompanyName>{node.node.citationCompany}</CompanyName>
                    </Details>
                  </Citation>
                  <TextCta>
                    <a
                      style={{ color: "#febd55" }}
                      href={
                        node.node.testimonialLink.json.content[0].content[1]
                          .data.uri
                      }
                    >
                      {
                        node.node.testimonialLink.json.content[0].content[1]
                          .content[0].value
                      }
                    </a>
                    <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                  </TextCta>
                </Description>
                <CompanyPicture>
                  <img
                    src={node.node.testimonialImage.fluid.src}
                    alt="Company"
                  />
                </CompanyPicture>
              </Wrapper>
            )
          }
        })}
        <BottomCallToAction>
          <CtaBackground>
            <CtaIllustartion>
              <img
                src="/images/Geo-location.svg"
                style={{
                  width: "100vw",
                }}
                alt=""
              />
            </CtaIllustartion>
            <CtaWrapper>
              <CtaContent>
                <CtaTagline>Digitilise your field operations</CtaTagline>
                <CtaButtons>
                  <TrialGroup>
                    <Link to="https://web.v3.fieldproapp.com/welcome">
                      <DownloadButtonDarkArrow id="start_trial">
                        Start Free Trial
                      </DownloadButtonDarkArrow>
                    </Link>

                    <DisclaimerText> *No credit card required</DisclaimerText>
                  </TrialGroup>
                  <Link to="/book-a-demo">
                    <ScheduleButtonDark id="bookdemo">
                      Book a Demo
                    </ScheduleButtonDark>
                  </Link>
                </CtaButtons>
              </CtaContent>
            </CtaWrapper>
          </CtaBackground>
        </BottomCallToAction>

        <Footer />
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}

export default TestimonialCard
export const testimonialCardQuery = graphql`
  {
    allContentfulTestimonialsPageSeo {
      edges {
        node {
          titleEN
          titleFR
          titleES
          descriptionEN
          descriptionFR
          descriptionES
        }
      }
    }
    allContentfulTestimonial(filter: { language: { eq: "EN" } }) {
      edges {
        node {
          testimonialImage {
            fluid {
              src
            }
          }
          testimonialTitle
          quote {
            quote
          }
          citationImage {
            fluid {
              src
            }
          }
          citationName
          citationPosition
          citationCompany
          testimonialLink {
            json
          }
        }
      }
    }
  }
`

export const Wrapper = styled.div`
  max-width: 1234px;
  padding: 160px 30px 0px 50px;
  margin: 0 auto;
  display: grid;
  grid-template-columns: 562px 552px;
  grid-gap: 120px;

  @media (max-width: 450px) {
    grid-template-columns: auto;
    padding: 100px 40px;
    gap: 30px;
  }
`
export const CompanyPicture = styled.div`
  padding-top: 30px;
`
export const Description = styled.div``
export const Title = styled(H3)``
export const Quotation = styled(QuoteText)`
  position: absolute;
  left: 500;
  margin-left: -12px;
`
export const Quote = styled(QuoteText)``
export const Citation = styled.div`
  display: grid;
  grid-template-columns: 80px auto;
  grid-gap: 30px;
  @media (max-width: 450px) {
    grid-template-columns: 60px auto;
    grid-gap: 20px;
  }
`
export const Details = styled.div`
  display: grid;
  grid-template-rows: 20px 20px auto;
  grid-gap: 10px;
  @media (max-width: 450px) {
    gap: 20px;
  }
`
export const TextBlock = styled.div`
  display: grid;
  grid-template-columns: auto auto;
`
export const PersonName = styled.p`
  font-size: 20px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: normal;
  color: #2c2c2c;
`
export const Profile = styled.div``

export const Position = styled.p`
  font-size: 20px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.5;
  letter-spacing: normal;
  color: #2c2c2c;
  @media (max-width: 450px) {
    line-height: 1;
  }
`
export const CompanyName = styled.p`
  font-size: 20px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.5;
  letter-spacing: normal;
  color: #2c2c2c;
`
export const TextCta = styled.p`
  font-weight: 800;
  font-size: 1.25em;
  color: #febd55;
  cursor: pointer;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`
export const ArrowButton = styled.img`
  padding-left: 8px;
`

///Bottom Call To Action
export const BottomCallToAction = styled.div``
export const CtaBackground = styled.div`
  background-color: #124e5d;
  width: 100vw;
  position: relative;
  left: -8px;
`
export const CtaWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 200px 0 200px 0;
  @media (max-width: 830px) {
    padding: 100px 48px 100px 48px;
  }
  @media (max-width: 512px) {
    padding: 80px 24px 80px 24px;
  }
`
export const CtaIllustartion = styled.div`
  position: absolute;
  align-self: center;
  top: 100px;
`
export const CtaContent = styled.div`
  position: relative;
  z-index: 1;
  max-width: 1280px;
  padding: 0 0 0 0;
  @media (max-width: 830px) {
    padding: 0;
  }
  @media (max-width: 512px) {
    padding: 0px;
  }
`
export const CtaTagline = styled(H2)`
  font-size: 3.25em;
  color: #fefefe;
  max-width: 600px;
  margin: 0 0 20px 0;
  @media (max-width: 830px) {
    padding: 0px;
  }
  @media (max-width: 512px) {
    padding: 0px;
    max-width: 320px;
    font-size: 2.5em;
    margin: 0 0 40px 0;
  }
`
export const CtaButtons = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 512px) {
    flex-direction: column;
  }
`
export const TrialGroup = styled.div``
export const DisclaimerText = styled(Caption)`
  color: #fefefe;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`

/////////////////////////////////////////////////////////
