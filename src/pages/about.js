import { graphql, Link } from "gatsby"
import React from "react"
import { BLOCKS, INLINES } from "@contentful/rich-text-types"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"

import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"

import Footer from "../components/sections/Footer.js"
import CookiesBar from "../components/sections/cookies.js"

import styled from "styled-components"
import { H1, H2, H3, H4, P, Caption } from "../components/styles/TextStyles.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"

import { DownloadButton } from "../components/Buttons/CtaButton.js"
import Clients from "../components/Homepage-components/Clients.js"
import NewHeader from "../components/sections/HeaderNew.js"
import LightCta from "../components/sections/LightCta.js"
import DarkCta from "../components/sections/DarkCta.js"

class AboutUs extends React.Component {
  render() {
    const options = {
      renderNode: {
        [BLOCKS.HEADING_1]: (node, children) => <H1>{children}</H1>,
        [BLOCKS.HEADING_2]: (node, children) => <H2>{children}</H2>,
        [BLOCKS.HEADING_3]: (node, children) => <H3>{children}</H3>,
        [BLOCKS.PARAGRAPH]: (node, children) => <P>{children}</P>,
        [BLOCKS.EMBEDDED_ENTRY]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [BLOCKS.EMBEDDED_ASSET]: (node, children) => (
          <Image>
            <img
              src={`https:${node.data.target.fields.file["en-US"].url}`}
              alt=""
            />
          </Image>
        ),
        [INLINES.EMBEDDED_ENTRY]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [INLINES.HYPERLINK]: (node, children) => {
          if (node.data.uri.includes("player.vimeo.com/video")) {
            return (
              <IframeContainer>
                <iframe
                  title="Unique Title 001"
                  src={node.data.uri}
                  frameBorder="0"
                  allowFullScreen
                ></iframe>
              </IframeContainer>
            )
          } else if (node.data.uri.includes("youtube.com/embed")) {
            return (
              <IframeContainer>
                <iframe
                  title="Unique Title 002"
                  src={node.data.uri}
                  allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                  frameBorder="0"
                  allowFullScreen
                ></iframe>
              </IframeContainer>
            )
          } else {
            return (
              <Link to={node.data.uri} target="_blank">
                {children}
              </Link>
            )
          }
        },

        [INLINES.ENTRY_HYPERLINK]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [INLINES.ASSET_HYPERLINK]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
      },
      renderMark: {},
    }

    const { data } = this.props
    const aboutContent = data.allContentfulAboutUsPage.edges[0]
    return (
      <Layout>
        <SEO
          title={aboutContent.pageNameSeoMetaTitle}
          description={aboutContent.pageDescriptionSeoMetaDescription}
        />
        <NewHeader en="/about" fr="/about-fr" es="/about-es" />
        <PageWrapper>
          <HeroSection>
            <HeroText>{aboutContent.node.title}</HeroText>
            <CallToAction>
              <LightCta />
            </CallToAction>
          </HeroSection>
          <BodySectionone>
            <MissionTextBlock>
              <BodyTitle>{aboutContent.node.ourMission}</BodyTitle>
              <BodyText>
                {documentToReactComponents(
                  aboutContent.node.missionDescription.json,
                  options
                )}
              </BodyText>
            </MissionTextBlock>
            <ImageBlock>
              <img
                src={aboutContent.node.ourMissionImage.file.url}
                alt=""
                style={{ width: "900px" }}
              />
            </ImageBlock>
          </BodySectionone>
          <SolutionsWrapper>
            <SectionTitle>
              {aboutContent.node.ourProductsAndServicesTitle}
            </SectionTitle>
            <SolutionCardsGroup>
              <SolutionsCardWrapper>
                <SolutionsIcon>
                  <img
                    src={aboutContent.node.fieldForceAutomationImage.file.url}
                    alt="Mobile CRM"
                  />
                </SolutionsIcon>
                <SolutionsTitle>
                  {aboutContent.node.fieldForceAutomationTitle}
                </SolutionsTitle>
                <SolutionsDescription>
                  {aboutContent.node.fieldForceAutomationDescription}
                </SolutionsDescription>
              </SolutionsCardWrapper>

              <SolutionsCardWrapper>
                <SolutionsIcon>
                  {" "}
                  <img
                    src={aboutContent.node.retailerAppImage.file.url}
                    alt=""
                  />
                </SolutionsIcon>
                <SolutionsTitle>
                  {" "}
                  {aboutContent.node.retailerAppTitle}
                </SolutionsTitle>
                <SolutionsDescription>
                  {aboutContent.node.retailerAppDescription}
                </SolutionsDescription>
              </SolutionsCardWrapper>
              <SolutionsCardWrapper>
                <SolutionsIcon>
                  <img
                    src={aboutContent.node.dataConsultingImage.file.url}
                    alt=""
                  />{" "}
                </SolutionsIcon>
                <SolutionsTitle>
                  {" "}
                  {aboutContent.node.dataConsultingTitle}
                </SolutionsTitle>
                <SolutionsDescription>
                  {aboutContent.node.dataConsultingDescription}
                </SolutionsDescription>
              </SolutionsCardWrapper>
            </SolutionCardsGroup>
          </SolutionsWrapper>
          <Clients />
          <TeamSection>
            <MeetTitle> {aboutContent.node.team}</MeetTitle>

            <a
              href="https://optimetriks.factorialhr.com/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <DownloadButton id="download">Meet Our Team</DownloadButton>
            </a>
          </TeamSection>
          <MetricSection>
            <MetricsWrapper>
              <MetricsIcon>
                <img src={aboutContent.node.usersIcon.file.url} alt="" />
              </MetricsIcon>
              <MetricsContent>
                <MetricsTitle>{aboutContent.node.users}</MetricsTitle>
                <MetricsDescription>
                  {aboutContent.node.usersText}
                </MetricsDescription>
              </MetricsContent>
            </MetricsWrapper>
            <MetricsWrapper>
              <MetricsIcon>
                <img src={aboutContent.node.clientsIcon.file.url} alt="" />
              </MetricsIcon>
              <MetricsContent>
                <MetricsTitle>{aboutContent.node.clients}</MetricsTitle>
                <MetricsDescription>
                  {aboutContent.node.clientsText}
                </MetricsDescription>
              </MetricsContent>
            </MetricsWrapper>
            <MetricsWrapper>
              <MetricsIcon>
                <img src={aboutContent.node.countriesIcon.file.url} alt="" />
              </MetricsIcon>
              <MetricsContent>
                <MetricsTitle>{aboutContent.node.countries}</MetricsTitle>
                <MetricsDescription>
                  {aboutContent.node.countriesText}
                </MetricsDescription>
              </MetricsContent>
            </MetricsWrapper>
            <MetricsWrapper>
              <MetricsIcon>
                <img src={aboutContent.node.officesIcon.file.url} alt="" />
              </MetricsIcon>
              <MetricsContent>
                <MetricsTitle>{aboutContent.node.offices}</MetricsTitle>
                <MetricsDescription>
                  {aboutContent.node.officesText}
                </MetricsDescription>
              </MetricsContent>
            </MetricsWrapper>
          </MetricSection>{" "}
        </PageWrapper>
        <BottomCallToAction>
          <CtaBackground>
            <CtaIllustartion>
              <img
                src="/images/Geo-location.svg"
                style={{
                  width: "100vw",
                }}
                alt=""
              />
            </CtaIllustartion>
            <CtaWrapper>
              <CtaContent>
                <CtaTagline>{aboutContent.node.contactUs}</CtaTagline>
                <CtaButtons>
                  <DarkCta />
                </CtaButtons>
              </CtaContent>
            </CtaWrapper>
          </CtaBackground>
        </BottomCallToAction>

        <Footer />
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}
export default AboutUs
export const query = graphql`
  {
    allContentfulAboutUsPage(filter: { language: { eq: "EN" } }) {
      edges {
        node {
          clients
          clientsIcon {
            file {
              url
            }
          }
          clientsText
          contactUs
          countries
          countriesIcon {
            file {
              url
            }
          }
          countriesText
          dataConsultingDescription
          dataConsultingImage {
            file {
              url
            }
          }
          fieldForceAutomationDescription
          dataConsultingTitle
          fieldForceAutomationImage {
            file {
              url
            }
          }
          fieldForceAutomationTitle

          offices

          officesText
          officesIcon {
            file {
              url
            }
          }

          ourMission
          missionDescription {
            json
          }
          ourMissionImage {
            file {
              url
            }
          }
          ourProductsAndServicesTitle

          pageDescriptionSeoMetaDescription
          pageNameSeoMetaTitle
          retailerAppDescription
          retailerAppTitle
          slug
          retailerAppImage {
            file {
              url
            }
          }
          usersText
          users

          team
          title
          usersIcon {
            file {
              url
            }
          }
        }
      }
    }
  }
`
export const PageWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
`

export const HeroSection = styled.div`
  margin: 240px 0 160px 0;

  @media (max-width: 450px) {
    margin: 160px 0 120px 0;
    padding: 0px 20px 0 20px;
  }
`
export const CallToAction = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 450px) {
    flex-direction: column;
  }
`
export const HeroText = styled(H1)`
  padding-bottom: 40px;
  max-width: 800px;
`

export const BodySectionone = styled.div`
  margin: 160px 0 160px 0;

  display: grid;
  grid-template-columns: 560px 560px;
  grid-gap: 160px;
  @media (max-width: 830px) {
    display: flex;
    flex-direction: column-reverse;
    grid-template-columns: repeat(1, auto);
    margin: 40px 0 800px 0;
    padding: 0px 20px 0 20px;
    grid-gap: 20px;
  }
  @media (max-width: 450px) {
    display: flex;
    flex-direction: column-reverse;
    grid-template-columns: repeat(1, auto);
    margin: 40px 0 240px 0;
    padding: 0px 20px 0 20px;
    grid-gap: 20px;
  }
`

export const ImageBlock = styled.div`
  padding: 0px 0 0 0;
  position: absolute;
  right: 0px;
  @media (max-width: 830px) {
    top: 1240px;
    position: absolute;
    right: 0px;
    max-width: 720px;
    margin-bottom: 80px;
  }
  @media (max-width: 350px) {
    top: 1200px;
    position: absolute;
    right: 0px;
    max-width: 300px;
    margin-bottom: 80px;
  }
`
export const TextBlock = styled.div`
  @media (max-width: 450px) {
    max-width: 360px;
    padding: 0;
  }
`
export const BodyTitle = styled(H2)``
export const BodyText = styled(P)``
export const MeetTitle = styled(H2)`
  max-width: 800px;
`
export const TeamSection = styled.div`
  margin: 80px 0 240px 0;
  @media (max-width: 450px) {
    margin: 80px 0 120px 0;
    padding: 0px 20px 0 20px;
  }
`
export const MissionTextBlock = styled.div`
  padding: 80px 0 0 0;
  @media (max-width: 450px) {
    padding: 20px 000;
  }
`
export const MetricSection = styled.div`
  margin: 240px 0 240px 0;
  display: grid;
  grid-template-columns: repeat(4, auto);
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    grid-gap: 80px;
    margin: 100px 0px;
  }
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
    margin: 100px 0px;
  }
`
export const MetricsWrapper = styled.div`
  width: 385px;
  height: 180px;

  @media (max-width: 450px) {
    max-width: 320px;
    margin: 40px 16px;
  }
`
export const MetricsContent = styled.div`
  margin: -120px 0px 0px 50px;
`
export const MetricsIcon = styled.div``
export const MetricsTitle = styled.p`
  font-weight: 800;
  font-size: 90px;
  line-height: 60px;
  padding: 50px 0 0 0;
  @media (max-width: 450px) {
    font-size: 80px;
  }
`
export const MetricsDescription = styled.p`
  font-weight: 400;
  font-size: 30px;
  line-height: 20px;
  padding-left: 15px;
`
///Bottom Call To Action
export const BottomCallToAction = styled.div``
export const CtaBackground = styled.div`
  background-color: #124e5d;
  width: 100vw;
  position: relative;
  left: -8px;
`
export const CtaWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 200px 0 200px 0;
  @media (max-width: 830px) {
    padding: 100px 48px 100px 48px;
  }
  @media (max-width: 512px) {
    padding: 80px 24px 80px 24px;
  }
`
export const CtaIllustartion = styled.div`
  position: absolute;
  align-self: center;
  top: 100px;
`
export const CtaContent = styled.div`
  position: relative;
  z-index: 1;
  max-width: 1280px;
  padding: 0 0 0 0;
  @media (max-width: 830px) {
    padding: 0;
  }
  @media (max-width: 512px) {
    padding: 0px;
  }
`
export const CtaTagline = styled(H2)`
  font-size: 3.25em;
  color: #fefefe;
  max-width: 850px;
  margin: 0 0 20px 0;
  @media (max-width: 830px) {
    padding: 0px;
  }
  @media (max-width: 512px) {
    padding: 0px;
    max-width: 320px;
    font-size: 2.5em;
    margin: 0 0 40px 0;
  }
`
export const CtaButtons = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 512px) {
    flex-direction: column;
  }
`
export const TrialGroup = styled.div``
export const DisclaimerText = styled(Caption)`
  color: #fefefe;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`

/////////////////////////////////////////////////////////
export const SolutionsWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 120px 0 120px 0;
  @media (max-width: 830px) {
    padding: 60px 48px 60px 48px;
  }
  @media (max-width: 512px) {
    margin: 0 0 120px 0;
    padding: 60px 24px 60px 24px;
  }
`
export const SectionTitle = styled(H2)`
  margin: 0 0 60px 0;
`
export const SolutionCardsGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(3, auto);
  grid-gap: 60px 40px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    grid-gap: 20px;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 30px;
  }
`
export const SolutionsCardWrapper = styled.div`
  width: 400px;
  height: 400px;
  border: 1px solid #979797;
  border-radius: 5px;
  padding: 40px;
  :hover {
    border: none;
    background-color: #e7edef;
    box-shadow: 0 2px 4px 0 #d0dcdf;
  }
  @media (max-width: 830px) {
    width: 320px;
    height: auto;
  }
  @media (max-width: 512px) {
    width: 320px;
    height: auto;
  }
`
export const SolutionsIcon = styled.div`
  height: 54px;
  width: 54px;
`
export const SolutionsTitle = styled(H4)`
  margin: 20px 0 10px 0;
`
export const SolutionsDescription = styled(P)``

////////////JOURNEY SECTION///////////
export const OurJourneySection = styled.div`
  @media (max-width: 512px) {
    grid-template-column: repeat(1, auto);
    margin: 0 24px 0 24px;
  }
`
export const JourneyTitle = styled(H2)`
  margin: 16px 0 16px 0px;
`
export const Description = styled(P)``
export const JourneyWrapper = styled.div``
export const JourneySectionOne = styled.div`
  max-width: 1280px;
  margin: 80px auto 0px auto;
  display: grid;
  grid-template-columns: 620px 0px 556px;
  @media (max-width: 830px) {
    display: none;
  }
`
// export const JourneySectionOneMobile = styled.div`
//   display: none;

//   @media (max-width: 830px) {
//     display: grid;
//     margin: 0 0 0 -320px;
//   }
// `
export const JourneySectionTwo = styled.div`
  max-width: 1280px;
  margin: 16px auto;
  display: grid;
  grid-template-columns: auto 108px 556px;
  @media (max-width: 830px) {
    display: none;
  }
`
export const JourneySectionTwoMobile = styled.div`
  display: none;

  @media (max-width: 830px) {
    display: grid;
    grid-template-columns: 40px 600px;
  }
  @media (max-width: 512px) {
    display: grid;
    grid-template-columns: 40px 248px;
  }
`
export const RectBar = styled.div`
  align-self: center;
  justify-self: center;
  width: 6px;
  height: 150px;
  background-color: #eb9000;
`
export const RectBarMobile = styled.div`
  align-self: center;
  justify-self: center;
  width: 6px;
  height: 240px;
  background-color: #eb9000;
`
export const RectBarTeal = styled.div`
  align-self: center;
  justify-self: center;
  width: 6px;
  height: 150px;
  background-color: #639faa;
`
export const RectBarTealMobile = styled.div`
  align-self: center;
  justify-self: center;
  width: 6px;
  height: 240px;
  background-color: #639faa;
`
export const JourneyCircleProp = styled.div`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  background-color: #eb9000;
  margin: 16px 0px 16px -24px;
`
export const JourneyCirclePropMobile = styled.div`
  width: 32px;
  height: 32px;
  border-radius: 50%;
  background-color: #eb9000;
  margin: 24px 0px 32px -12px;
`
export const JourneyCirclePropTeal = styled.div`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  background-color: #639faa;
  margin: 16px 0px 16px -24px;
`
export const JourneyCirclePropTealMobile = styled.div`
  width: 32px;
  height: 32px;
  border-radius: 50%;
  background-color: #639faa;
  margin: 24px 0px 32px -12px;
`
export const TextSquad = styled.div`
  max-width: 566px;
`
export const SpaceBar = styled.div`
  @media (max-width: 512px) {
    display: none;
  }
`
export const AlignGroup = styled.div``
export const JourneyinnerTitle = styled(H3)``
export const JourneyinnerDescription = styled(P)``

export const DisclaimerTextDark = styled(Caption)`
  color: #6c6c6c;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`

//Youtube video embed
export const IframeContainer = styled.span`
  padding-bottom: 56.25%;
  position: relative;
  display: block;
  width: 100%;

  > iframe {
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;
  }
`
export const Image = styled.div``
