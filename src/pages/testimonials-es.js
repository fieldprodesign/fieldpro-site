import React from "react"
import { graphql, Link } from "gatsby"

import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"

import NewHeaderES from "../components/sections/HeaderNewES.js"
import FooterES from "../components/sections/FooterES.js"

import Brands from "../components/Testimonial-components/Brands.js"
import HeroSectionES from "../components/Testimonial-components/HeroSectionES.js"
import CookiesBar from "../components/sections/cookies.js"

import {
  DownloadButtonDarkArrow,
  ScheduleButtonDark,
} from "../components/Buttons/CtaButton.js"

import {
  Wrapper,
  Description,
  Title,
  TextBlock,
  Quotation,
  Quote,
  Citation,
  Profile,
  Details,
  PersonName,
  Position,
  CompanyName,
  TextCta,
  ArrowButton,
  CompanyPicture,
  BottomCallToAction,
  CtaBackground,
  CtaContent,
  CtaButtons,
  CtaTagline,
  CtaWrapper,
  CtaIllustartion,
  TrialGroup,
  DisclaimerText,
} from "../pages/testimonials.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"

class TestimonialCardES extends React.Component {
  render() {
    const { data } = this.props
    const testimonials = data.allContentfulTestimonial.edges
    return (
      <Layout>
        <SEO
          title="Testimonios"
          description="Conozca cómo nuestros clientes monitorean en tiempo real sus ventas de campo y realizan auditorías minoristas utilizando nuestra aplicación móvil y web FieldPro."
        />
        <NewHeaderES
          en="/testimonials"
          fr="/testimonials-fr"
          es="/testimonials-es"
        />
        <HeroSectionES />
        <Brands />

        {testimonials.map((node, index) => {
          if (index % 2 === 0) {
            return (
              <Wrapper>
                <CompanyPicture>
                  <img
                    src={node.node.testimonialImage.fluid.src}
                    alt="Company Logo"
                  />
                </CompanyPicture>
                <Description>
                  <Title>{node.node.testimonialTitle}</Title>
                  <TextBlock>
                    <Quotation>"</Quotation>
                    <Quote>{node.node.quote.quote}"</Quote>
                  </TextBlock>
                  <Citation>
                    <Profile>
                      <img
                        src={node.node.citationImage.fluid.src}
                        alt="Citation"
                      />
                    </Profile>
                    <Details>
                      <PersonName>{node.node.citationName}</PersonName>
                      <Position>{node.node.citationPosition} </Position>
                      <CompanyName>{node.node.citationCompany}</CompanyName>
                    </Details>
                  </Citation>
                  <TextCta>
                    <a
                      style={{ color: "#febd55" }}
                      href={
                        node.node.testimonialLink.json.content[0].content[1]
                          .data.uri
                      }
                    >
                      {
                        node.node.testimonialLink.json.content[0].content[1]
                          .content[0].value
                      }
                    </a>
                    <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                  </TextCta>
                </Description>
              </Wrapper>
            )
          } else {
            return (
              <Wrapper>
                <Description>
                  <Title>{node.node.testimonialTitle}</Title>
                  <TextBlock>
                    <Quotation>"</Quotation>
                    <Quote>{node.node.quote.quote}"</Quote>
                  </TextBlock>
                  <Citation>
                    <Profile>
                      <img
                        src={node.node.citationImage.fluid.src}
                        alt="Citation"
                      />
                    </Profile>
                    <Details>
                      <PersonName>{node.node.citationName}</PersonName>
                      <Position>{node.node.citationPosition} </Position>
                      <CompanyName>{node.node.citationCompany}</CompanyName>
                    </Details>
                  </Citation>
                  <TextCta>
                    <a
                      style={{ color: "#febd55" }}
                      href={
                        node.node.testimonialLink.json.content[0].content[1]
                          .data.uri
                      }
                    >
                      {
                        node.node.testimonialLink.json.content[0].content[1]
                          .content[0].value
                      }
                    </a>
                    <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                  </TextCta>
                </Description>
                <CompanyPicture>
                  <img
                    src={node.node.testimonialImage.fluid.src}
                    alt="Company"
                  />
                </CompanyPicture>
              </Wrapper>
            )
          }
        })}

        <BottomCallToAction>
          <CtaBackground>
            <CtaIllustartion>
              <img
                src="/images/Geo-location.svg"
                style={{ width: "100vw" }}
                alt=""
              />
            </CtaIllustartion>
            <CtaWrapper>
              <CtaContent>
                <CtaTagline>Manage your field salesforce digitally</CtaTagline>
                <CtaButtons>
                  <TrialGroup>
                    <Link to="https://web.v3.fieldproapp.com/welcome">
                      <DownloadButtonDarkArrow id="start_trial">
                        Empieza la prueba gratis
                      </DownloadButtonDarkArrow>
                    </Link>

                    <DisclaimerText>
                      *No se requiere tarjeta de crédito
                    </DisclaimerText>
                  </TrialGroup>
                  <Link to="/book-a-demo-es">
                    <ScheduleButtonDark id="bookdemo">
                      Reserva una demo
                    </ScheduleButtonDark>
                  </Link>
                </CtaButtons>
              </CtaContent>
            </CtaWrapper>
          </CtaBackground>
        </BottomCallToAction>

        <FooterES />
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}
export default TestimonialCardES
export const testimonialCardQueryES = graphql`
  {
    allContentfulTestimonial(filter: { language: { eq: "ES" } }) {
      edges {
        node {
          testimonialImage {
            fluid {
              src
            }
          }
          testimonialTitle
          quote {
            quote
          }
          citationImage {
            fluid {
              src
            }
          }
          citationName
          citationPosition
          citationCompany
          testimonialLink {
            json
          }
        }
      }
    }
  }
`
