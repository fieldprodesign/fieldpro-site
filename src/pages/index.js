import React, { Component } from "react"
import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"

import NewHeaderDark from "../components/sections/HeaderNewDark.js"
import Hero from "../components/Homepage-components/Hero.js"
import SolutionsSection from "../components/Homepage-components/SolutionsSection.js"
import Clients from "../components/Homepage-components/Clients.js"
import WorkflowTemplates from "../components/Homepage-components/WorkflowTemplates.js"
import Industries from "../components/Homepage-components/Industries.js"
import AboutSection from "../components/Homepage-components/AboutSection.js"
import MetricsSection from "../components/Homepage-components/MetricsSection.js"
import IntergrationSection from "../components/Homepage-components/IntergrationSection.js"
import Testimonials from "../components/Homepage-components/TestimonialsSection.js"
import Cta from "../components/Homepage-components/Cta.js"
import { graphql } from "gatsby"

import Footer from "../components/sections/Footer.js"
import CookiesBar from "../components/sections/cookies.js"
import styled from "styled-components"
import NewsletterSection from "../components/Homepage-components/NewsletterSection.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"

class Home extends Component {
  render() {
    const { data } = this.props
    const homeContent = data.allContentfulHomePage.edges[0]
    return (
      <Layout>
        <SEO
          title={homeContent.node.pageNameSeoMetaTitle}
          description={homeContent.node.pageDescriptionseoMetaDescription}
        />
        <PageWrapper>
          <NewHeaderDark en="/" fr="/fr" es="/es" />

          <Hero />
          <SolutionsSection />
          <Clients />
          <Industries />
          <WorkflowTemplates />
          <AboutSection />
          <MetricsSection />
          <IntergrationSection />
          <Testimonials />
          <Cta />
          <NewsletterSection />

          <Footer />
          <ScrollToTop />
          <CookiesBar />
        </PageWrapper>
      </Layout>
    )
  }
}
export default Home
export const indexPageQuery = graphql`
  {
    allContentfulHomePage(filter: { language: { eq: "EN" } }) {
      edges {
        node {
          pageDescriptionseoMetaDescription
          pageNameSeoMetaTitle
        }
      }
    }
  }
`
const PageWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  margin: 0px;
  padding: 0px;
`
