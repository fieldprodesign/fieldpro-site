import React, { Component } from "react"
import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"
import { graphql } from "gatsby"

import NewHeaderDarkFR from "../components/sections/HeaderNewDarkFR.js"

import CookiesBar from "../components/sections/cookies.js"
import styled from "styled-components"

import FooterFR from "../components/sections/FooterFR.js"

import HeroFR from "../components/Homepage-components/HeroFR.js"
import ClientsFR from "../components/Homepage-components/ClientsFR.js"
import SolutionsSectionFR from "../components/Homepage-components/solutionSectionFR.js"
import IndustriesFR from "../components/Homepage-components/IndustriesFR.js"
import WorkflowTemplatesFR from "../components/Homepage-components/WorkflowTemplatesFR.js"
import AboutSectionFR from "../components/Homepage-components/AboutSectionFR.js"
import MetricsSectionFR from "../components/Homepage-components/MetricsSectionFR.js"
import IntergrationSectionFR from "../components/Homepage-components/IntergrationSectionFR.js"
import TestimonialsFR from "../components/Homepage-components/TestimonialSectionFR.js"
import CtaFR from "../components/Homepage-components/CtaFR.js"
import NewsletterSectionFR from "../components/Homepage-components/NewsletterSectionFR.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"
class HomeFR extends Component {
  render() {
    const { data } = this.props
    const homeContent = data.allContentfulHomePage.edges[0]
    return (
      <Layout>
        <SEO
          title={homeContent.node.pageNameSeoMetaTitle}
          description={homeContent.node.pageDescriptionseoMetaDescription}
        />
        <PageWrapper>
          <NewHeaderDarkFR en="/" fr="/fr" es="/es" />
          <HeroFR />
          <SolutionsSectionFR />
          <ClientsFR />
          <IndustriesFR />
          <WorkflowTemplatesFR />
          <AboutSectionFR />
          <MetricsSectionFR />
          <IntergrationSectionFR />
          <TestimonialsFR />
          <CtaFR />
          <NewsletterSectionFR />
          <FooterFR />
          <ScrollToTop />
          <CookiesBar />
        </PageWrapper>
      </Layout>
    )
  }
}
export default HomeFR
export const indexPageQuery = graphql`
  {
    allContentfulHomePage(filter: { language: { eq: "FR" } }) {
      edges {
        node {
          pageDescriptionseoMetaDescription
          pageNameSeoMetaTitle
        }
      }
    }
  }
`
const PageWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  margin: 0px;
  padding: 0px;
`
