import React from "react"

import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"

import Footer from "../components/sections/Footer.js"
import CookiesBar from "../components/sections/cookies.js"
import styled from "styled-components"
import { H1, H2, P, Caption } from "../components/styles/TextStyles.js"
import ContactForm from "../components/sections/contactForm.js"

import TabsBar from "../components/sections/Tabs.js"
import NewHeaderES from "../components/sections/HeaderNewES.js"

import DarkCtaES from "../components/sections/DarkCtaES.js"

class ContactUsES extends React.Component {
  render() {
    return (
      <Layout>
        <SEO title="Contact Us" description="Get in touch" />
        <NewHeaderES en="/contact" fr="/contact-fr" es="/contact-es" />
        <PageWrapper>
          <HeroWrapper>
            <Content>
              <Title>Contact Us</Title>

              <Description>
                If you have any questions or queries, please contact one of our
                offices. Please feel free to call us or email us at any time,
                and we will be sure to get back to you as soon as possible. Our
                office locations are listed below.
              </Description>
            </Content>
          </HeroWrapper>
          <OfficeSectionWrapper>
            <TabsBar />
          </OfficeSectionWrapper>

          <ContactWrapper>
            <OfficePicture>
              <img src="/images/Nairobi.jpeg" alt="" />
            </OfficePicture>
            <ContactFormSection>
              <ContactForm />
            </ContactFormSection>
          </ContactWrapper>
        </PageWrapper>
        <BottomCallToAction>
          <CtaBackground>
            <CtaIllustartion>
              <img
                src="/images/Geo-location.svg"
                style={{
                  width: "100vw",
                }}
                alt=""
              />
            </CtaIllustartion>
            <CtaWrapper>
              <CtaContent>
                <CtaTagline>Become a part of our esteemed customers</CtaTagline>
                <CtaButtons>
                  <DarkCtaES />
                </CtaButtons>
              </CtaContent>
            </CtaWrapper>
          </CtaBackground>
        </BottomCallToAction>

        <Footer />
        <CookiesBar />
      </Layout>
    )
  }
}
export default ContactUsES

export const PageWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
`
//HERO SECTION STYLE
const HeroWrapper = styled.div`
  padding: 280px 0px 0 0px;
`

const Content = styled.div`
  max-width: 800px;
  @media (max-width: 830px) {
    max-width: 720px;

    padding: 40px 80px 40px;
  }
  @media (max-width: 450px) {
    max-width: 400px;
    margin: 0;
    padding: 0px 40px 20px;
  }
`
const Title = styled(H1)`
  color: #2c2c2c;
  padding-bottom: 20px;

  @media (max-width: 450px) {
    font-size: 2em;
  }
`
const Description = styled(P)`
  padding-bottom: 30px;
`

const OfficeSectionWrapper = styled.div`
  margin: 120px 0 160px 0;

  @media (max-width: 830px) {
    display: grid;
    grid-template-columns: repeat(1, auto);
    margin: 80px 0 80px 0;
  }
  @media (max-width: 450px) {
    display: grid;
    grid-template-columns: repeat(1, auto);
    margin: 40px 0 40px 0;
  }
`

//////OFFICE SEND QUERY SECTION

const ContactWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 80px;
  margin: 160px 0 160px 0;
  @media (max-width: 830px) {
    width: auto;
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
  @media (max-width: 450px) {
    width: auto;
    grid-template-columns: repeat(1, auto);
    margin: 40px 0 40px 0;
    grid-gap: 20px;
  }
`
const OfficePicture = styled.div`
  padding: 140px 000;
  @media (max-width: 830px) {
    padding: 0;
  }
  @media (max-width: 450px) {
    padding: 60px 000;
  }
`
const ContactFormSection = styled.div`
  @media (max-width: 830px) {
    margin: 0 0px 0 40px;
  }
  @media (max-width: 450px) {
    margin: 0 0px 0 20px;
  }
`
///Bottom Call To Action
export const BottomCallToAction = styled.div``
export const CtaBackground = styled.div`
  background-color: #124e5d;
  width: 100vw;
  position: relative;
  left: -8px;
`
export const CtaWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 200px 0 200px 0;
  @media (max-width: 830px) {
    padding: 100px 48px 100px 48px;
  }
  @media (max-width: 512px) {
    padding: 80px 24px 80px 24px;
  }
`
export const CtaIllustartion = styled.div`
  position: absolute;
  align-self: center;
  top: 100px;
`
export const CtaContent = styled.div`
  position: relative;
  z-index: 1;
  max-width: 1280px;
  padding: 0 0 0 0;
  @media (max-width: 830px) {
    padding: 0;
  }
  @media (max-width: 512px) {
    padding: 0px;
  }
`
export const CtaTagline = styled(H2)`
  font-size: 3.25em;
  color: #fefefe;
  max-width: 850px;
  margin: 0 0 20px 0;
  @media (max-width: 830px) {
    padding: 0px;
  }
  @media (max-width: 512px) {
    padding: 0px;
    max-width: 320px;
    font-size: 2.5em;
    margin: 0 0 40px 0;
  }
`
export const CtaButtons = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 512px) {
    flex-direction: column;
  }
`
export const TrialGroup = styled.div``
export const DisclaimerText = styled(Caption)`
  color: #fefefe;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
