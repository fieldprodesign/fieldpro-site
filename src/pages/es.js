import React, { Component } from "react"
import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"
import { graphql } from "gatsby"

import NewHeaderDarkES from "../components/sections/HeaderNewDarkES.js"

import CookiesBar from "../components/sections/cookies.js"
import styled from "styled-components"

import FooterES from "../components/sections/FooterES.js"

import HeroES from "../components/Homepage-components/HeroES.js"
import ClientsES from "../components/Homepage-components/ClientsES.js"
import SolutionsSectionES from "../components/Homepage-components/SolutionsSectionES.js"
import IndustriesES from "../components/Homepage-components/IndustriesES.js"
import WorkflowTemplatesES from "../components/Homepage-components/WorkflowTemplatesES.js"
import AboutSectionES from "../components/Homepage-components/AboutSectionES.js"
import MetricsSectionES from "../components/Homepage-components/MetricsSectionES.js"
import IntergrationSectionES from "../components/Homepage-components/IntergrationSectionES.js"
import TestimonialsES from "../components/Homepage-components/TestimonialSectionEs.js"
import CtaES from "../components/Homepage-components/CtaES.js"
import NewsletterSectionES from "../components/Homepage-components/NewsletterSectionES.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"
class HomeES extends Component {
  render() {
    const { data } = this.props
    const homeContent = data.allContentfulHomePage.edges[0]
    return (
      <Layout>
        <SEO
          title={homeContent.node.pageNameSeoMetaTitle}
          description={homeContent.node.pageDescriptionseoMetaDescription}
        />
        <PageWrapper>
          <NewHeaderDarkES en="/" fr="/fr" es="/es" />
          <HeroES />
          <SolutionsSectionES />
          <ClientsES />
          <IndustriesES />
          <WorkflowTemplatesES />
          <AboutSectionES />
          <MetricsSectionES />
          <IntergrationSectionES />
          <TestimonialsES />
          <CtaES />
          <NewsletterSectionES />
          <FooterES />
          <ScrollToTop />
          <CookiesBar />
        </PageWrapper>
      </Layout>
    )
  }
}
export default HomeES
export const indexPageQuery = graphql`
  {
    allContentfulHomePage(filter: { language: { eq: "ES" } }) {
      edges {
        node {
          pageDescriptionseoMetaDescription
          pageNameSeoMetaTitle
        }
      }
    }
  }
`
const PageWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  margin: 0px;
  padding: 0px;
`
