import { graphql, Link } from "gatsby"
import React from "react"
import { BLOCKS, INLINES } from "@contentful/rich-text-types"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"

import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"

import FooterFR from "../components/sections/FooterFR.js"
import CookiesBar from "../components/sections/cookies.js"
import { H1, H2, H3, P } from "../components/styles/TextStyles.js"

import { DownloadButton } from "../components/Buttons/CtaButton.js"
import ClientsFR from "../components/Homepage-components/ClientsFR.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"
import LightCtaFR from "../components/sections/LightCtaFR.js"
import DarkCtaFR from "../components/sections/DarkCtaFR.js"
import styled from "styled-components"
import {
  PageWrapper,
  HeroSection,
  HeroText,
  CallToAction,
  BodySectionone,
  MissionTextBlock,
  BodyTitle,
  BodyText,
  ImageBlock,
  SolutionsIcon,
  SolutionsTitle,
  SolutionsDescription,
  TeamSection,
  MeetTitle,
  MetricsIcon,
  MetricsWrapper,
  MetricsContent,
  MetricsTitle,
  MetricsDescription,
  BottomCallToAction,
  MetricSection,
  CtaBackground,
  CtaIllustartion,
  CtaWrapper,
  CtaContent,
  CtaTagline,
  CtaButtons,
  SolutionsWrapper,
  SectionTitle,
  SolutionCardsGroup,
  Image,
  IframeContainer,
} from "../pages/about.js"
import NewHeaderFR from "../components/sections/HeaderNewFR.js"

class AboutUs extends React.Component {
  render() {
    const options = {
      renderNode: {
        [BLOCKS.HEADING_1]: (node, children) => <H1>{children}</H1>,
        [BLOCKS.HEADING_2]: (node, children) => <H2>{children}</H2>,
        [BLOCKS.HEADING_3]: (node, children) => <H3>{children}</H3>,
        [BLOCKS.PARAGRAPH]: (node, children) => <P>{children}</P>,
        [BLOCKS.EMBEDDED_ENTRY]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [BLOCKS.EMBEDDED_ASSET]: (node, children) => (
          <Image>
            <img
              src={`https:${node.data.target.fields.file["en-US"].url}`}
              alt=""
            />
          </Image>
        ),
        [INLINES.EMBEDDED_ENTRY]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [INLINES.HYPERLINK]: (node, children) => {
          if (node.data.uri.includes("player.vimeo.com/video")) {
            return (
              <IframeContainer>
                <iframe
                  title="Unique Title 001"
                  src={node.data.uri}
                  frameBorder="0"
                  allowFullScreen
                ></iframe>
              </IframeContainer>
            )
          } else if (node.data.uri.includes("youtube.com/embed")) {
            return (
              <IframeContainer>
                <iframe
                  title="Unique Title 002"
                  src={node.data.uri}
                  allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                  frameBorder="0"
                  allowFullScreen
                ></iframe>
              </IframeContainer>
            )
          } else {
            return (
              <Link to={node.data.uri} target="_blank">
                {children}
              </Link>
            )
          }
        },

        [INLINES.ENTRY_HYPERLINK]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [INLINES.ASSET_HYPERLINK]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
      },
      renderMark: {},
    }

    const { data } = this.props
    const aboutContent = data.allContentfulAboutUsPage.edges[0]
    return (
      <Layout>
        <SEO
          title={aboutContent.pageNameSeoMetaTitle}
          description={aboutContent.pageDescriptionSeoMetaDescription}
        />
        <NewHeaderFR en="/about" fr="/about-fr" es="/about-es" />
        <PageWrapper>
          <HeroSection>
            <HeroText>{aboutContent.node.title}</HeroText>
            <CallToAction>
              <LightCtaFR />
            </CallToAction>
          </HeroSection>
          <BodySectionone>
            <MissionTextBlock>
              <BodyTitle>{aboutContent.node.ourMission}</BodyTitle>
              <BodyText>
                {documentToReactComponents(
                  aboutContent.node.missionDescription.json,
                  options
                )}
              </BodyText>
            </MissionTextBlock>
            <ImageBlock>
              <img
                src={aboutContent.node.ourMissionImage.file.url}
                alt=""
                style={{ width: "900px" }}
              />
            </ImageBlock>
          </BodySectionone>
          <SolutionsWrapper>
            <SectionTitle>
              {aboutContent.node.ourProductsAndServicesTitle}
            </SectionTitle>
            <SolutionCardsGroup>
              <SolutionsCardWrapper>
                <SolutionsIcon>
                  <img
                    src={aboutContent.node.fieldForceAutomationImage.file.url}
                    alt="Mobile CRM"
                  />
                </SolutionsIcon>
                <SolutionsTitle>
                  {aboutContent.node.fieldForceAutomationTitle}
                </SolutionsTitle>
                <SolutionsDescription>
                  {aboutContent.node.fieldForceAutomationDescription}
                </SolutionsDescription>
              </SolutionsCardWrapper>

              <SolutionsCardWrapper>
                <SolutionsIcon>
                  {" "}
                  <img
                    src={aboutContent.node.retailerAppImage.file.url}
                    alt=""
                  />
                </SolutionsIcon>
                <SolutionsTitle>
                  {" "}
                  {aboutContent.node.retailerAppTitle}
                </SolutionsTitle>
                <SolutionsDescription>
                  {aboutContent.node.retailerAppDescription}
                </SolutionsDescription>
              </SolutionsCardWrapper>
              <SolutionsCardWrapper>
                <SolutionsIcon>
                  <img
                    src={aboutContent.node.dataConsultingImage.file.url}
                    alt=""
                  />{" "}
                </SolutionsIcon>
                <SolutionsTitle>
                  {" "}
                  {aboutContent.node.dataConsultingTitle}
                </SolutionsTitle>
                <SolutionsDescription>
                  {aboutContent.node.dataConsultingDescription}
                </SolutionsDescription>
              </SolutionsCardWrapper>
            </SolutionCardsGroup>
          </SolutionsWrapper>
          <ClientsFR />
          <TeamSection>
            <MeetTitle> {aboutContent.node.team}</MeetTitle>

            <a
              href="https://optimetriks.factorialhr.com/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <DownloadButton id="download">Rencontrer l'équipe</DownloadButton>
            </a>
          </TeamSection>
          <MetricSection>
            <MetricsWrapper>
              <MetricsIcon>
                <img src={aboutContent.node.usersIcon.file.url} alt="" />
              </MetricsIcon>
              <MetricsContent>
                <MetricsTitle>{aboutContent.node.users}</MetricsTitle>
                <MetricsDescription>
                  {aboutContent.node.usersText}
                </MetricsDescription>
              </MetricsContent>
            </MetricsWrapper>
            <MetricsWrapper>
              <MetricsIcon>
                <img src={aboutContent.node.clientsIcon.file.url} alt="" />
              </MetricsIcon>
              <MetricsContent>
                <MetricsTitle>{aboutContent.node.clients}</MetricsTitle>
                <MetricsDescription>
                  {aboutContent.node.clientsText}
                </MetricsDescription>
              </MetricsContent>
            </MetricsWrapper>
            <MetricsWrapper>
              <MetricsIcon>
                <img src={aboutContent.node.countriesIcon.file.url} alt="" />
              </MetricsIcon>
              <MetricsContent>
                <MetricsTitle>{aboutContent.node.countries}</MetricsTitle>
                <MetricsDescription>
                  {aboutContent.node.countriesText}
                </MetricsDescription>
              </MetricsContent>
            </MetricsWrapper>
            <MetricsWrapper>
              <MetricsIcon>
                <img src={aboutContent.node.officesIcon.file.url} alt="" />
              </MetricsIcon>
              <MetricsContent>
                <MetricsTitle>{aboutContent.node.offices}</MetricsTitle>
                <MetricsDescription>
                  {aboutContent.node.officesText}
                </MetricsDescription>
              </MetricsContent>
            </MetricsWrapper>
          </MetricSection>{" "}
        </PageWrapper>
        <BottomCallToAction>
          <CtaBackground>
            <CtaIllustartion>
              <img
                src="/images/Geo-location.svg"
                style={{ width: "100vw" }}
                alt=""
              />
            </CtaIllustartion>
            <CtaWrapper>
              <CtaContent>
                <CtaTagline>{aboutContent.node.contactUs}</CtaTagline>
                <CtaButtons>
                  <DarkCtaFR />
                </CtaButtons>
              </CtaContent>
            </CtaWrapper>
          </CtaBackground>
        </BottomCallToAction>

        <FooterFR />
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}
export default AboutUs
export const query = graphql`
  {
    allContentfulAboutUsPage(filter: { language: { eq: "FR" } }) {
      edges {
        node {
          clients
          clientsIcon {
            file {
              url
            }
          }
          clientsText
          contactUs
          countries
          countriesIcon {
            file {
              url
            }
          }
          countriesText
          dataConsultingDescription
          dataConsultingImage {
            file {
              url
            }
          }
          fieldForceAutomationDescription
          dataConsultingTitle
          fieldForceAutomationImage {
            file {
              url
            }
          }
          fieldForceAutomationTitle

          offices

          officesText
          officesIcon {
            file {
              url
            }
          }

          ourMission

          ourMissionImage {
            file {
              url
            }
          }
          ourProductsAndServicesTitle

          pageDescriptionSeoMetaDescription
          pageNameSeoMetaTitle
          retailerAppDescription
          retailerAppTitle
          slug
          retailerAppImage {
            file {
              url
            }
          }
          usersText
          users
          missionDescription {
            json
          }
          team
          title
          usersIcon {
            file {
              url
            }
          }
        }
      }
    }
  }
`
const SolutionsCardWrapper = styled.div`
  width: 400px;
  height: 448px;
  border: 1px solid #979797;
  border-radius: 5px;
  padding: 40px;
  :hover {
    border: none;
    background-color: #e7edef;
    box-shadow: 0 2px 4px 0 #d0dcdf;
  }
  @media (max-width: 830px) {
    width: 320px;
    height: auto;
  }
  @media (max-width: 512px) {
    width: 320px;
    height: auto;
  }
`
