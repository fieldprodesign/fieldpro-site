import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"

import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"
import CookiesBar from "../components/sections/cookies.js"
import {
  HeroSectionWrapper,
  HeroSection,
  HeroTitle,
  HeroDescription,
  SeearchBarContainer,
  SearchInput,
  WorkflowTemplatesWrapper,
  CardTemplateWrapper,
  CardTemplateIcon,
  CardTemplateTitle,
  CardTemplateDescription,
  CardDividerLine,
  CardChipGroup,
  CtaBackground,
  CtaIllustartion,
  CtaWrapper,
  CtaContent,
  CtaTagline,
  CtaButtons,
  FilterSearchBar,
  FilterBar,
} from "../pages/workflowtemplates.js"
import NewHeaderFR from "../components/sections/HeaderNewFR.js"
import Footer from "../components/sections/Footer.js"

import { ButtonChip } from "../components/Buttons/CtaButton.js"
import { Link } from "gatsby"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"
import DarkCtaFR from "../components/sections/DarkCtaFR.js"

function AppTemplatesFR({ data }) {
  const cards = data.allContentfulWorkflowTemplate.edges
  const seoContent = data.allContentfulMetasForGeneratedPages.edges[0]
  const [title, setTitle] = useState("")
  const [foundCards, setFoundCards] = useState(cards)

  const filter = e => {
    const keyword = e.target.value

    if (keyword !== "") {
      const results = cards.filter(workflowcard => {
        return workflowcard.node.title
          .toLowerCase()
          .includes(keyword.toLowerCase())
        // Use the toLowerCase() method to make it case-insensitive
      })
      setFoundCards(results)
    } else {
      setFoundCards(cards)
      // If the text field is empty, show all users
    }

    setTitle(keyword)
  }
  const [offsetY, setOffsetY] = useState(0)
  const handleScroll = () => setOffsetY(window.pageYOffset)

  useEffect(() => {
    window.addEventListener("scroll", handleScroll)

    return () => window.removeEventListener("scroll", handleScroll)
  }, [])

  return (
    <Layout>
      <SEO
        title={seoContent.node.workflowsSeoMetaTitle}
        description={seoContent.node.workflowsSeoMetaDescription}
      />
      <NewHeaderFR
        en="/workflowtemplates"
        fr="/workflowtemplates-fr"
        es="/workflowtemplates-es"
      />
      <HeroSectionWrapper>
        <HeroSection>
          <HeroTitle>Modèles de workflow </HeroTitle>
          <HeroDescription>
            Commencez simplement en utilisant nos modèles de workflow
            personnalisables.
          </HeroDescription>
        </HeroSection>
      </HeroSectionWrapper>
      <FilterSearchBar>
        <FilterBar>
          {/* <TextTitle>FilterBy</TextTitle>
          <CategoryBtn>
            <DdButton>Dropdown</DdButton>
            
          </CategoryBtn>
          <ResetBtn></ResetBtn> */}
        </FilterBar>
        <SeearchBarContainer
          style={{ transform: `translateY(-${offsetY * 0.3}px)` }}
        >
          <SearchInput
            type="search"
            value={title}
            onChange={filter}
            placeholder="Search template title..."
          />
        </SeearchBarContainer>
      </FilterSearchBar>
      <WorkflowTemplatesWrapper>
        {foundCards && foundCards.length > 0 ? (
          foundCards.map(workflowcard => (
            <Link to={`/${workflowcard.node.slug}`} key={workflowcard.node.id}>
              <CardTemplateWrapper>
                <CardTemplateIcon>
                  {<img src={workflowcard.node.workflowIcon.file.url} alt="" />}
                </CardTemplateIcon>
                <CardTemplateTitle>{workflowcard.node.title}</CardTemplateTitle>
                <CardTemplateDescription>
                  {workflowcard.node.metaDescription}
                </CardTemplateDescription>
                <CardDividerLine></CardDividerLine>
                <CardChipGroup>
                  {workflowcard.node.tags.map(templatetags => {
                    return <ButtonChip>{templatetags.title}</ButtonChip>
                  })}
                </CardChipGroup>
              </CardTemplateWrapper>
            </Link>
          ))
        ) : (
          <h1>We are sorry! We couldn't find the results for your search.</h1>
        )}
      </WorkflowTemplatesWrapper>
      <CtaBackground>
        <CtaIllustartion>
          <img
            src="/images/Geo-location.svg"
            style={{ width: "100vw" }}
            alt=""
          />
        </CtaIllustartion>
        <CtaWrapper>
          <CtaContent>
            <CtaTagline>
              {cards.CallToAction || "Manage your field salesforce digitally"}
            </CtaTagline>
            <CtaButtons>
              <DarkCtaFR />
            </CtaButtons>
          </CtaContent>
        </CtaWrapper>
      </CtaBackground>
      <Footer />
      <ScrollToTop />
      <CookiesBar />
    </Layout>
  )
}

export const queryFR = graphql`
  {
    allContentfulWorkflowTemplate(filter: { language: { eq: "FR" } }) {
      edges {
        node {
          title
          metaDescription
          workflowIcon {
            file {
              url
            }
          }
          tags {
            title
          }
          language
          callToAction
          slug
        }
      }
    }
    allContentfulMetasForGeneratedPages(filter: { language: { eq: "FR" } }) {
      edges {
        node {
          workflowsSeoMetaDescription
          workflowsSeoMetaTitle
        }
      }
    }
  }
`

export default AppTemplatesFR
