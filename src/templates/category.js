import React from "react"
import { Link, graphql } from "gatsby"
import styled from "styled-components"

import Layout from "../components/Layout.js"
import SEO from "../components/seo"

import NewHeader from "../components/sections/HeaderNew.js"
import NewHeaderES from "../components/sections/HeaderNewES.js"
import NewHeaderFR from "../components/sections/HeaderNewFR.js"
import Footer from "../components/sections/Footer.js"
import FooterFR from "../components/sections/FooterFR.js"
import FooterES from "../components/sections/FooterES.js"
import { HeaderGroup } from "../components/styles/TextStyles.js"
import {
  CategoryMenuContainer,
  CategoryGroup,
  AllCategories,
  Category,
  PostGroup,
  Post,
  PostImage,
  PostText,
  Title,
  CategoryTagContainer,
  CategoryTag,
} from "../pages/blog.js"

class CategoryTemplate extends React.Component {
  render() {
    /* */
    const { data } = this.props
    const posts = data.allContentfulBlogPost.edges

    const category = this.props.data.contentfulBlogCategory

    var headerMenu
    if (category.language === "EN") {
      headerMenu = <NewHeader />
    }
    if (category.language === "FR") {
      headerMenu = <NewHeaderFR />
    }
    if (category.language === "ES") {
      headerMenu = <NewHeaderES />
    }

    var footerMenu
    if (category.language === "EN") {
      footerMenu = <Footer />
    }
    if (category.language === "FR") {
      footerMenu = <FooterFR />
    }
    if (category.language === "ES") {
      footerMenu = <FooterES />
    }

    var AllLink
    if (category.language === "EN") {
      AllLink = (
        <AllCategories>
          <Link to="/blog" activeClassName="active">
            All
          </Link>
        </AllCategories>
      )
    }
    if (category.language === "FR") {
      AllLink = (
        <AllCategories>
          <Link to="/blog-fr" activeClassName="active">
            All
          </Link>
        </AllCategories>
      )
    }
    if (category.language === "ES") {
      AllLink = (
        <AllCategories>
          <Link to="/blog-es" activeClassName="active">
            All
          </Link>
        </AllCategories>
      )
    }

    return (
      <Layout location={this.props.location}>
        <SEO title="FieldPro Blog" keywords={[]} />
        {headerMenu}
        <HeaderGroup>
          <CategoryMenuContainer>
            <CategoryGroup>
              {AllLink}
              {data.allContentfulBlogCategory.edges.map(({ node }) => {
                if (node.language === category.language) {
                  return (
                    <Category key={node.slug}>
                      <Link activeClassName="active" to={`/${node.slug}`}>
                        {node.title}
                      </Link>
                    </Category>
                  )
                } else {
                  return null
                }
              })}
            </CategoryGroup>
          </CategoryMenuContainer>
        </HeaderGroup>
        <Body>
          <PostGroup>
            {posts.map(({ node }) => {
              const title = node.title || node.slug
              return (
                <Post key={node.slug}>
                  <PostImage>
                    <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
                      <img src={node.image.fluid.src} alt="" />
                    </Link>
                  </PostImage>
                  <PostText>
                    <Title>
                      <Link style={{ boxShadow: `none` }} to={`/${node.slug}`}>
                        {title}
                      </Link>
                    </Title>
                    <CategoryTagContainer>
                      {node.categories.map(category => {
                        return (
                          <Link to={`/${category.slug}`}>
                            <CategoryTag>
                              {category.title}&#160;&#160;|
                            </CategoryTag>
                          </Link>
                        )
                      })}
                    </CategoryTagContainer>
                  </PostText>
                </Post>
              )
            })}
          </PostGroup>
        </Body>
        {footerMenu}
      </Layout>
    )
  }
}

export default CategoryTemplate

export const categoryQuery = graphql`
  query CategoryBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    contentfulBlogCategory(slug: { eq: $slug }) {
      language
      slug
      title
    }
    allContentfulBlogCategory(sort: { fields: updatedAt, order: ASC }) {
      edges {
        node {
          language
          title
          slug
        }
      }
    }
    allContentfulBlogPost(
      sort: { fields: createdAt, order: DESC }
      filter: { categories: { elemMatch: { slug: { eq: $slug } } } }
    ) {
      edges {
        node {
          slug
          title
          featured
          image {
            fluid {
              src
            }
          }
          categories {
            language
            title
            slug
          }
        }
      }
    }
  }
`

export const Body = styled.div`
  display: grid;
  justify-items: center;
  margin: 8em 0 0 0;
  padding: 0;
  @media (max-width: 32em) {
    margin: 10em 0 0 0;
  }
`
