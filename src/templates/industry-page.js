import React from "react"
import { graphql, Link } from "gatsby"
import styled from "styled-components"

import Layout from "../components/Layout.js"
import SEO from "../components/seo"

import NewHeader from "../components/sections/HeaderNew.js"
import NewHeaderES from "../components/sections/HeaderNewES.js"
import NewHeaderFR from "../components/sections/HeaderNewFR.js"
import Footer from "../components/sections/Footer.js"
import FooterES from "../components/sections/FooterES.js"
import FooterFR from "../components/sections/FooterFR.js"
import { H1, H2, H3, P, QuoteText } from "../components/styles/TextStyles.js"

import { ScheduleButton } from "../components/Buttons/CtaButton.js"

import CookiesBar from "../components/sections/cookies.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"
import LightCta from "../components/sections/LightCta.js"
import LightCtaFR from "../components/sections/LightCtaFR.js"
import LightCtaES from "../components/sections/LightCtaES.js"
import DarkCtaFR from "../components/sections/DarkCtaFR.js"
import DarkCtaES from "../components/sections/DarkCtaES.js"
import DarkCta from "../components/sections/DarkCta.js"

class IndustryTemplate extends React.Component {
  render() {
    const industry = this.props.data.contentfulIndustryPage

    const brandsLogo = this.props.data.contentfulIndustryPage.brandsLogos
    //FETCHING BODY CONTENT IN A LIST
    const listDataOne = this.props.data.contentfulIndustryPage.bodySectionText1
      .json.content[0].content

    const listDataTwo = this.props.data.contentfulIndustryPage.bodySectionText2
      .json.content[0].content
    const listDataThree = this.props.data.contentfulIndustryPage
      .bodySectionText3.json.content[0].content

    var headerMenu
    if (industry.language === "EN") {
      headerMenu = (
        <NewHeader
          en={`/${industry.slug}`}
          fr={`/${industry.slug}-fr`}
          es={`/${industry.slug}-es`}
        />
      )
    }
    if (industry.language === "FR") {
      var currentSlug = industry.slug
      currentSlug = currentSlug.substring(0, currentSlug.length - 3)
      headerMenu = (
        <NewHeaderFR
          en={`/${currentSlug}`}
          fr={`/${currentSlug}-fr`}
          es={`/${currentSlug}-es`}
        />
      )
    }
    if (industry.language === "ES") {
      var currentSlugEs = industry.slug
      currentSlugEs = currentSlugEs.substring(0, currentSlugEs.length - 3)
      headerMenu = (
        <NewHeaderES
          en={`/${currentSlugEs}`}
          fr={`/${currentSlugEs}-fr`}
          es={`/${currentSlugEs}-es`}
        />
      )
    }

    var footerMenu
    if (industry.language === "EN") {
      footerMenu = <Footer />
    }
    if (industry.language === "FR") {
      footerMenu = <FooterFR />
    }
    if (industry.language === "ES") {
      footerMenu = <FooterES />
    }

    //Modal

    var ctaModal
    if (industry.language === "EN") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCta />
        </CTAClassWrapper>
      )
    }
    if (industry.language === "FR") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCtaFR />
        </CTAClassWrapper>
      )
    }
    if (industry.language === "ES") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCtaES />
        </CTAClassWrapper>
      )
    }
    //Middle CTA

    var middleCTA
    if (industry.language === "EN") {
      middleCTA = (
        <Link to="/book-a-demo">
          <ScheduleButton id="bookdemo">Book a Demo</ScheduleButton>
        </Link>
      )
    }
    if (industry.language === "FR") {
      middleCTA = (
        <Link to="/book-a-demo-fr">
          <ScheduleButton id="bookdemo">Demander une démo</ScheduleButton>
        </Link>
      )
    }
    if (industry.language === "ES") {
      middleCTA = (
        <Link to="/book-a-demo-es">
          <ScheduleButton id="bookdemo">Reserva una demo</ScheduleButton>
        </Link>
      )
    }

    //BottomModal
    var bottomCtaModal
    if (industry.language === "EN") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCta />
        </BottomCTAWrapper>
      )
    }
    if (industry.language === "FR") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCtaFR />
        </BottomCTAWrapper>
      )
    }
    if (industry.language === "ES") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCtaES />
        </BottomCTAWrapper>
      )
    }

    // Background image for testimonials section
    var background = industry.testimonialBgImage.file.url

    return (
      <Layout location={this.props.location}>
        <SEO title={industry.pageName} description={industry.pageDescription} />

        {headerMenu}
        <BodyGroup>
          <HeroWrapper>
            <HeroContent>
              <Content>
                <Title>{industry.bannerTitle}</Title>

                <Description>{industry.bannerText}</Description>
                <CallToAction>{ctaModal}</CallToAction>
              </Content>
              <HeroImage>
                <RectangleBackground
                  style={{
                    backgroundColor: `${industry.bannerImageBackground}`,
                  }}
                ></RectangleBackground>
                <ImageForeground>
                  <img
                    style={styles.foregroundImage}
                    src={industry.bannerImage.fluid.src}
                    alt=""
                  />
                </ImageForeground>
              </HeroImage>
            </HeroContent>
          </HeroWrapper>
          <BenefitsSectionWrapper>
            <BenefitsSectionTitle>
              {industry.benefitsSectionTitle}
            </BenefitsSectionTitle>
            <SectionCards>
              <BenefitsWrapper>
                <BenefitsIcon>
                  <img src={industry.benefitIcon1.fluid.src} alt="" />
                </BenefitsIcon>
                <BenefitsContent>
                  <BenefitsTitle>{industry.benefitTitle1}</BenefitsTitle>
                  <BenefitsDescription>
                    {industry.benefitText1}
                  </BenefitsDescription>
                </BenefitsContent>
              </BenefitsWrapper>
              <BenefitsWrapper>
                <BenefitsIcon>
                  <img src={industry.benefitIcon2.fluid.src} alt="" />
                </BenefitsIcon>
                <BenefitsContent>
                  <BenefitsTitle>{industry.benefitTitle2}</BenefitsTitle>
                  <BenefitsDescription>
                    {industry.benefitText2}
                  </BenefitsDescription>
                </BenefitsContent>
              </BenefitsWrapper>
              <BenefitsWrapper>
                <BenefitsIcon>
                  <img src={industry.benefitIcon3.fluid.src} alt="" />
                </BenefitsIcon>
                <BenefitsContent>
                  <BenefitsTitle>{industry.benefitTitle3}</BenefitsTitle>
                  <BenefitsDescription>
                    {industry.benefitText3}
                  </BenefitsDescription>
                </BenefitsContent>
              </BenefitsWrapper>
            </SectionCards>
          </BenefitsSectionWrapper>
          <BrandSectionWrapper>
            <BrandsSectionTitle>{industry.brandsTitle}</BrandsSectionTitle>
            <MiddleCtaButton>{middleCTA}</MiddleCtaButton>
            <LogosWrapper>
              {brandsLogo.map(companyLogo => {
                return (
                  <Logo>
                    <img src={companyLogo.fluid.src} alt="" />
                  </Logo>
                )
              })}
            </LogosWrapper>
          </BrandSectionWrapper>
          <BodySectionWrapper>
            <BodyContentWrapper>
              <BodyContentDescription>
                <BodyContentTitle>
                  {industry.bodySectionTitle1}
                </BodyContentTitle>
                <BodyContentTextBlock>
                  <ListGroup>
                    {listDataOne.map(ListItemOne => {
                      return (
                        <ListItem>
                          {ListItemOne.content[0].content[0].value}
                        </ListItem>
                      )
                    })}
                  </ListGroup>
                </BodyContentTextBlock>
                <TextCta>
                  <a
                    style={{ color: "#febd55" }}
                    href={
                      industry.bodySectionLink1.json.content[0].content[1].data
                        .uri
                    }
                  >
                    {
                      industry.bodySectionLink1.json.content[0].content[1]
                        .content[0].value
                    }
                  </a>
                  <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                </TextCta>
              </BodyContentDescription>
              <ProductPicture>
                <img src={industry.bodySectionImage1.fluid.src} alt="" />
              </ProductPicture>
            </BodyContentWrapper>
            <BodyContentWrapperTwo>
              <ProductPictureTwo>
                <img src={industry.bodySectionImage2.fluid.src} alt="" />
              </ProductPictureTwo>
              <BodyContentDescription>
                <BodyContentTitle>
                  {industry.bodySectionTitle2}
                </BodyContentTitle>
                <BodyContentTextBlock>
                  <ListGroup>
                    {listDataTwo.map(ListItemTwo => {
                      return (
                        <ListItem>
                          {ListItemTwo.content[0].content[0].value}
                        </ListItem>
                      )
                    })}
                  </ListGroup>
                </BodyContentTextBlock>
                <TextCta>
                  <a
                    style={{ color: "#febd55" }}
                    href={
                      industry.bodySectionLink2.json.content[0].content[1].data
                        .uri
                    }
                  >
                    {
                      industry.bodySectionLink2.json.content[0].content[1]
                        .content[0].value
                    }
                  </a>
                  <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                </TextCta>
              </BodyContentDescription>
            </BodyContentWrapperTwo>

            <BodyContentWrapper>
              <BodyContentDescription>
                <BodyContentTitle>
                  {industry.bodySectionTitle3}
                </BodyContentTitle>
                <BodyContentTextBlock>
                  <ListGroup>
                    {listDataThree.map(ListItemThree => {
                      return (
                        <ListItem>
                          {ListItemThree.content[0].content[0].value}
                        </ListItem>
                      )
                    })}
                  </ListGroup>
                </BodyContentTextBlock>
                <TextCta>
                  <a
                    style={{ color: "#febd55" }}
                    href={
                      industry.bodySectionLink3.json.content[0].content[1].data
                        .uri
                    }
                  >
                    {
                      industry.bodySectionLink3.json.content[0].content[1]
                        .content[0].value
                    }
                  </a>
                  <ArrowButton src="/images/Arrow.svg" alt=""></ArrowButton>
                </TextCta>
              </BodyContentDescription>
              <ProductPicture>
                <img src={industry.bodySectionImage3.fluid.src} alt="" />
              </ProductPicture>
            </BodyContentWrapper>
          </BodySectionWrapper>
          <TestimonialSection style={{ backgroundImage: `url(${background})` }}>
            <TestimonialCardWrapper>
              <TestimonialCardBox>
                <TextBlock>
                  <Quotation>"</Quotation>
                  <Quote>{industry.testimonialQuote.testimonialQuote}"</Quote>
                </TextBlock>
                <Citation>
                  <PersonName>{industry.citationName} </PersonName>
                  <Position>{industry.citationPosition}</Position>

                  <CompanyName>{industry.citationCompany}</CompanyName>
                </Citation>
              </TestimonialCardBox>
            </TestimonialCardWrapper>
          </TestimonialSection>
          <LinkPagesSectionWrapper>
            <SectionCards>
              <LinkCardWrapper>
                <Link
                  to={industry.linkCardUrl.json.content[0].content[1].data.uri}
                >
                  <CardBox>
                    <LinkCardIcon>
                      <img
                        src={industry.linkCardIcon.fluid.src}
                        alt=""
                        style={{
                          margin: "0",
                          padding: "0",
                          width: "48px",
                          height: "48px",
                        }}
                      />
                    </LinkCardIcon>
                    <LinkCardTitle>{industry.linkCardTitle}</LinkCardTitle>
                    <LinkCardText>{industry.linkCardText}</LinkCardText>
                  </CardBox>
                </Link>
              </LinkCardWrapper>
              <LinkCardWrapper>
                <Link
                  to={
                    industry.linkCardUrlTwo.json.content[0].content[1].data.uri
                  }
                >
                  <CardBox>
                    <LinkCardIcon>
                      <img
                        src={industry.linkCardIconTwo.file.url}
                        alt=""
                        style={{
                          margin: "0",
                          padding: "0",
                          width: "48px",
                          height: "48px",
                        }}
                      />
                    </LinkCardIcon>
                    <LinkCardTitle>{industry.linkCardTitle2}</LinkCardTitle>
                    <LinkCardText>{industry.linkCardText2}</LinkCardText>
                  </CardBox>
                </Link>
              </LinkCardWrapper>
            </SectionCards>
          </LinkPagesSectionWrapper>
          <BottomCallToAction>
            <CtaBackground>
              <img
                style={{ width: "72em" }}
                src={industry.ctaBgIllustration.fluid.src}
                alt="Illustration"
              />
            </CtaBackground>
            <CtaContent>
              <CtaText>
                <Tagline style={{ color: "#fefefe" }}>
                  {industry.callToAction}
                </Tagline>
                <CtaGroup>{bottomCtaModal}</CtaGroup>
              </CtaText>
            </CtaContent>
          </BottomCallToAction>
        </BodyGroup>
        {footerMenu}
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}

export default IndustryTemplate

export const pageQuery = graphql`
  query IndustryPageBySlug($slug: String!) {
    contentfulIndustryPage(slug: { eq: $slug }) {
      language
      pageName
      pageDescription
      bannerTitle
      bannerText
      bannerImage {
        fluid {
          src
        }
      }
      bannerImageBackground
      benefitsSectionTitle
      benefitTitle1
      benefitIcon1 {
        fluid {
          src
        }
      }
      benefitText1
      benefitTitle2
      benefitIcon2 {
        fluid {
          src
        }
      }
      benefitText2
      benefitTitle3
      benefitIcon3 {
        fluid {
          src
        }
      }
      benefitText3
      brandsTitle
      brandsLogos {
        fluid {
          src
        }
      }
      bodySectionImage1 {
        fluid {
          src
        }
      }
      bodySectionTitle1
      bodySectionText1 {
        json
      }
      bodySectionLink1 {
        json
      }
      bodySectionImage2 {
        fluid {
          src
        }
      }
      bodySectionTitle2
      bodySectionText2 {
        json
      }
      bodySectionLink2 {
        json
      }
      bodySectionImage3 {
        fluid {
          src
        }
      }
      bodySectionTitle3
      bodySectionText3 {
        json
      }
      bodySectionLink3 {
        json
      }

      citationName
      citationPosition
      citationCompany

      callToAction
      ctaBgIllustration {
        fluid {
          src
        }
      }
      linkCardIcon {
        fluid {
          src
        }
      }
      linkCardTitle
      linkCardText
      linkCardUrl {
        json
      }
      testimonialBgImage {
        file {
          url
        }
      }
      testimonialQuote {
        testimonialQuote
      }
      linkCardUrlTwo {
        json
      }
      linkCardIconTwo {
        file {
          url
        }
      }

      linkCardTitle2
      linkCardText2

      slug
    }
  }
`
//BODYGROUP
const BodyGroup = styled.div`
  max-width: 1280px;
  margin: 0 auto;
`

//HERO SECTION STYLE
const HeroWrapper = styled.div`
  position: relative;
`
const HeroContent = styled.div`
  max-width: 1280px;

  padding: 280px 0px 0 0px;

  display: grid;
  grid-template-columns: repeat(2, auto);
  @media (max-width: 830px) {
    grid-template-columns: auto;
    margin: 0;
    padding: 0;
  }
  @media (max-width: 450px) {
    grid-template-columns: auto;
    margin: 0;
    padding: 0;
  }
`
const Content = styled.div`
  max-width: 660px;
  @media (max-width: 830px) {
    max-width: auto;

    padding: 200px 40px 80px;
  }
  @media (max-width: 450px) {
    max-width: 400px;

    padding: 200px 40px 80px;
  }
`
const Title = styled(H1)`
  color: #2c2c2c;
  padding-bottom: 20px;
`
const Description = styled(P)`
  padding-bottom: 30px;
`
const CallToAction = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 450px) {
    flex-direction: column;
  }
`
const HeroImage = styled.div`
  @media (max-width: 830px) {
    display: none;
  }
`
const RectangleBackground = styled.div`
  width: 864px;
  height: 648px;
  position: absolute;
  top: 160px;

  padding: 0 0 0 65px;
  transform: rotate(-20deg);
  border-radius: 48px;
  -webkit-filter: blur(12px);
  filter: blur(12px);
  /* background-color: #febd55; */
  @media (max-width: 830px) {
    display: none;
  }
`
const ImageForeground = styled.div`
  border-radius: 48px;
  padding: 0 0 0 65px;
`

const styles = {
  foregroundImage: {
    width: "864px",
    height: "648px",
    borderRadius: "48px",
    position: "absolute",
    top: "160px",
    padding: "0 0 0 0px",
    transform: "rotate(-27deg)",
  },
}

//Benefits section cards
const BenefitsSectionWrapper = styled.div`
  margin: 380px auto 160px auto;
  @media (max-width: 830px) {
    margin: 0px 40px 80px 40px;
  }
  @media (max-width: 450px) {
    margin: 0px auto 80px auto;
  }
`
const BenefitsSectionTitle = styled(H2)`
  padding-bottom: 60px;
  max-width: 800px;
  @media (max-width: 830px) {
    max-width: 720px;
    padding: 0 16px 0px 0;
  }
  @media (max-width: 450px) {
    max-width: 350px;
    padding: 0 16px 0px 0;
  }
`
const SectionCards = styled.div`
  display: grid;
  grid-template-columns: 450px 450px 450px;
  grid-gap: 12px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(1, auto);
    padding: 40px 10px;
  }
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
    padding: 40px 10px;
  }
`
const BenefitsWrapper = styled.div`
  width: 385px;
  height: 180px;
  @media (max-width: 830px) {
    max-width: 385px;
    margin: 20px 16px;
  }
  @media (max-width: 450px) {
    max-width: 320px;
    margin: 20px 16px;
  }
`
const BenefitsContent = styled.div`
  margin: -120px 0px 0px 50px;
`
const BenefitsIcon = styled.div``
const BenefitsTitle = styled(H3)``
const BenefitsDescription = styled(P)``

//LOGOS WRAPPER
const BrandSectionWrapper = styled.div`
  margin: 200px auto 200px auto;
  @media (max-width: 830px) {
    margin: 0px 80px 40px 80px;
  }
  @media (max-width: 450px) {
    margin: 0px auto 40px auto;
  }
`
const BrandsSectionTitle = styled(H2)`
  padding: 0px 0px 0px 120px;
  @media (max-width: 830px) {
    max-width: auto;
    padding: 40px 16px;
  }
  @media (max-width: 450px) {
    max-width: 350px;
    padding: 40px 16px;
  }
`
const MiddleCtaButton = styled.div`
  padding: 0px 0px 20px 120px;
  @media (max-width: 830px) {
    max-width: auto;
    padding: 0px 16px;
  }
  @media (max-width: 450px) {
    max-width: 350px;
    padding: 0px 16px;
  }
`
const LogosWrapper = styled.div`
  max-width: 1280px;
  padding: 20px 0px 0px 120px;
  display: grid;
  grid-template-columns: repeat(5, auto);
  grid-gap: 10px;
  @media (max-width: 450px) {
    display: grid;
    padding: 40px 0 0 0;
    grid-template-columns: repeat(3, auto);
  }
  @media (max-width: 450px) {
    display: grid;
    padding: 40px 0 0 0;
    grid-template-columns: auto auto;
  }
`
const Logo = styled.div`
  padding: 0;
`

//Body content styling
const BodySectionWrapper = styled.div`
  max-width: 1280px;

  margin: 200px auto 240px auto;
  @media (max-width: 830px) {
    margin: 40px auto 40px auto;
  }
  @media (max-width: 450px) {
    margin: 40px auto 40px auto;
  }
`
const BodyContentWrapper = styled.div`
  margin: 0px auto 120px auto;
  display: grid;
  grid-template-columns: 562px 552px;
  grid-gap: 120px;
  @media (max-width: 830px) {
    padding: 0px 40px 0 40px;
    margin: 0;
    display: flex;
    flex-direction: column-reverse;
    gap: 30px;
  }
  @media (max-width: 450px) {
    padding: 0px 40px 0 40px;
    margin: 0;
    display: flex;
    flex-direction: column-reverse;
    gap: 30px;
  }
`
const BodyContentWrapperTwo = styled.div`
  margin: 0px auto 120px auto;
  display: grid;
  grid-template-columns: 562px 552px;
  grid-gap: 120px;

  @media (max-width: 830px) {
    padding: 0px 40px 0 40px;
    margin: 0;
    display: grid;
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
  @media (max-width: 450px) {
    padding: 0px 40px 0 40px;
    margin: 0;
    display: grid;
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
`
const ProductPicture = styled.div`
  @media (max-width: 830px) {
    width: auto;
  }
  @media (max-width: 450px) {
    width: auto;
  }
`
const ProductPictureTwo = styled.div`
  width: 800px;
  margin: 0 0 0 -160px;
  @media (max-width: 830px) {
    width: auto;
    margin: 40px 0 -40px 0px;
  }
  @media (max-width: 450px) {
    width: auto;
    margin: 40px 0 -40px 0px;
  }
`
const BodyContentDescription = styled.div`
  padding: 20px 0 0 0;
  @media (max-width: 830px) {
    padding: 0;
  }
  @media (max-width: 450px) {
    padding: 0;
  }
`
const BodyContentTitle = styled(H3)`
  max-width: 480px;
`
const BodyContentTextBlock = styled.div`
  max-width: 480px;
`

const TextCta = styled.p`
  font-weight: 600;
  font-size: 1.25em;
  color: #febd55 !important;
  cursor: pointer;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`

const ArrowButton = styled.img`
  padding-left: 8px;
`
//LINK CARDS STYLING
const LinkPagesSectionWrapper = styled.div`
  max-width: 1280px;

  margin: 240px auto;
  @media (max-width: 830px) {
    margin: 40px 80px 40px 80px;
  }
  @media (max-width: 450px) {
    margin: 40px auto 40px auto;
  }
`
const LinkCardWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 40px;
  @media (max-width: 830px) {
    max-width: auto;
    padding: 0;
    display: grid;
    grid-template-columns: repeat(2, auto);
    grid-gap: 40px;
  }
  @media (max-width: 450px) {
    max-width: 300px;
    padding: 0;
    display: grid;
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
`

const CardBox = styled.div`
  padding: 24px 40px 0px 40px;
  width: 400px;
  height: 360px;
  box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.08);
  background-color: #fefefe;
  @media (max-width: 830px) {
    max-width: auto;
    padding: 20px;
  }
  @media (max-width: 450px) {
    max-width: 360px;
    padding: 20px;
  }
`
const LinkCardIcon = styled.div`
  padding: 0 0 12px 0;
  margin: 24px 0 0 0;
`

const LinkCardTitle = styled(H3)`
  padding: 0 0 12px 0;
  margin: 0;
`
const LinkCardText = styled(P)`
  padding: 0;
`
//Bottom Call To Action
const BottomCallToAction = styled.div`
  position: relative;
  display: grid;
  max-width: 72em;
  margin: 0px 0px 320px 0px;
  padding: 0;
  @media (max-width: 830px) {
    width: auto;
  }
  @media (max-width: 450px) {
    max-width: 360px;
    margin: 100px 0px;
  }
`
const CtaBackground = styled.div`
  position: absolute;
  z-index: 1;
  align-self: center;
`

const CtaContent = styled.div`
  position: relative;
  display: grid;
  max-width: 72em;
  background: #124e5d;
  box-shadow: 0 0.5em 2em #2c2c2c18;
  margin: 0 0 4em 0;
  padding: 1.5em 8em 2.25em 8em;
  @media (max-width: 830px) {
    width: auto;
    margin: 0 0 4em 0;
    padding: 1em 2em 3em 2em;
  }
  @media (max-width: 450px) {
    max-width: 360px;
  }
`
const Tagline = styled(H2)`
  padding: 10px 0px 0px 0px;
  max-width: 800px;
  @media (max-width: 450px) {
    padding: 10px 0px 40px 0px;
  }
`
const CtaText = styled.div`
  position: relative;
  align-self: center;
  top: -2em;
  color: #2a2a2a;

  padding: 50px 0 50px 0;
  z-index: 2;
  h2 {
    font-size: 3.25em;
  }
  @media (max-width: 32em) {
    width: 24em;
    margin: 0;
    padding: 0;
    h2 {
      font-size: 2.5em;
      margin: 1.2em 1.2em 1em 0;
    }
  }
  @media (max-width: 450px) {
    max-width: 360px;
    margin: 0;
    padding: 0;
  }
`

const CtaGroup = styled.div`
  margin: -30px 0 0 0;

  display: flex;

  flex-direction: row;

  grid-gap: 20px;
  @media (max-width: 450px) {
    flex-direction: column;
  }
`
const ListGroup = styled.ul``
const ListItem = styled.li`
  list-style-image: url("/images/green-check-mark.svg");
`
//testimonial section
const TestimonialSection = styled.div`
  max-width: 1280px;
  margin: 240px auto;

  img {
    height: 600px;
    width: 1280px;
    background-repeat: no-repeat;
    background-size: cover;
  }
  @media (max-width: 450px) {
    max-width: 400px;
    margin: 120px 0 120px 0;
    padding: 40px 0px 40px 0px;
  }
`

const TestimonialCardWrapper = styled.div`
  padding: 72px 0px 72px 80px;
  @media (max-width: 450px) {
    max-width: 360px;
    padding: 0px 0px 0px 0px;
  }
`
const TestimonialCardBox = styled.div`
  max-width: 600px;
  box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.08);
  background-color: #fefefe;

  @media (max-width: 450px) {
    max-width: 360px;

    padding: 20px 20px;
  }
`
const TextBlock = styled.div`
  padding: 40px 40px 0px 40px;
  display: grid;
  grid-template-columns: auto auto;
  @media (max-width: 450px) {
    padding: 10px 10px 0 10px;
  }
`
const Quotation = styled(QuoteText)`
  position: absolute;
  left: 500;
  margin-left: -12px;
`
const Quote = styled(QuoteText)``
const Citation = styled.div`
  padding: 0px 40px 0 40px;
  display: grid;
  grid-template-rows: 20px 20px auto;
  grid-gap: 10px;
  @media (max-width: 450px) {
    gap: 20px;
    @media (max-width: 450px) {
      padding: 10px 10px 0 10px;
    }
  }
`
const PersonName = styled.p`
  font-size: 20px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: normal;
  color: #2c2c2c;
`

const Position = styled.p`
  font-size: 20px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.5;
  letter-spacing: normal;
  color: #2c2c2c;
  @media (max-width: 450px) {
    line-height: 1;
  }
`
const CompanyName = styled.p`
  font-size: 20px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.5;
  letter-spacing: normal;
  color: #2c2c2c;
`

////CTA AT TOP
const CTAClassWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 20px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    margin: 0;
    padding: 0px;
  }
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
    margin: 0;
    padding: 0;
  }
`
////CTA AT BOTTOM
const BottomCTAWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 20px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    margin: 0;
    padding: 0px;
  }
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
    margin: 0;
    padding: 0;
  }
`
