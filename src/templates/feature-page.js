import React from "react"
import { graphql, Link } from "gatsby"
import styled from "styled-components"

import Layout from "../components/Layout.js"
import SEO from "../components/seo"

import NewHeader from "../components/sections/HeaderNew.js"
import NewHeaderES from "../components/sections/HeaderNewES.js"
import NewHeaderFR from "../components/sections/HeaderNewFR.js"

import Footer from "../components/sections/Footer.js"
import FooterES from "../components/sections/FooterES.js"
import FooterFR from "../components/sections/FooterFR.js"
import { H1, H2, H3, P } from "../components/styles/TextStyles.js"

import {
  ArrowButton,
  RoundButtonDiagonalArrow,
} from "../components/Buttons/CtaButton.js"
import CookiesBar from "../components/sections/cookies.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"

import LightCta from "../components/sections/LightCta.js"
import LightCtaFR from "../components/sections/LightCtaFR.js"
import LightCtaES from "../components/sections/LightCtaES.js"
import DarkCtaFR from "../components/sections/DarkCtaFR.js"
import DarkCtaES from "../components/sections/DarkCtaES.js"
import DarkCta from "../components/sections/DarkCta.js"

class FeatureTemplate extends React.Component {
  render() {
    const feature = this.props.data.contentfulFeaturesPage
    const featurePages = this.props.data.allContentfulFeaturesPage.edges
    const filteredArray = featurePages.filter(filtercards => {
      return (
        feature.language === filtercards.node.language &&
        feature.pageTitleAsSeenOnMenu !== filtercards.node.pageTitleAsSeenOnMenu
      )
    })
    const _ = require("lodash")

    let shuffledArray = _.sampleSize(filteredArray, 3)

    var headerMenu
    if (feature.language === "EN") {
      headerMenu = (
        <NewHeader
          en={`/${feature.slug}`}
          fr={`/${feature.slug}-fr`}
          es={`/${feature.slug}-es`}
        />
      )
    }
    if (feature.language === "FR") {
      var currentSlug = feature.slug
      currentSlug = currentSlug.substring(0, currentSlug.length - 3)
      headerMenu = (
        <NewHeaderFR
          en={`/${currentSlug}`}
          fr={`/${currentSlug}-fr`}
          es={`/${currentSlug}-es`}
        />
      )
    }
    if (feature.language === "ES") {
      var currentSlugEs = feature.slug
      currentSlugEs = currentSlugEs.substring(0, currentSlugEs.length - 3)
      headerMenu = (
        <NewHeaderES
          en={`/${currentSlugEs}`}
          fr={`/${currentSlugEs}-fr`}
          es={`/${currentSlugEs}-es`}
        />
      )
    }

    var footerMenu
    if (feature.language === "EN") {
      footerMenu = <Footer />
    }
    if (feature.language === "FR") {
      footerMenu = <FooterFR />
    }
    if (feature.language === "ES") {
      footerMenu = <FooterES />
    }
    //Modal

    var ctaModal
    if (feature.language === "EN") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCta />
        </CTAClassWrapper>
      )
    }
    if (feature.language === "FR") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCtaFR />
        </CTAClassWrapper>
      )
    }
    if (feature.language === "ES") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCtaES />
        </CTAClassWrapper>
      )
    }

    //BottomModal
    var bottomCtaModal
    if (feature.language === "EN") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCta />
        </BottomCTAWrapper>
      )
    }
    if (feature.language === "FR") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCtaFR />
        </BottomCTAWrapper>
      )
    }
    if (feature.language === "ES") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCtaES />
        </BottomCTAWrapper>
      )
    }

    return (
      <Layout location={this.props.location}>
        <SEO title={feature.pageName} description={feature.pageDescription} />

        {headerMenu}

        <PageWrapper>
          <HeroWrapper>
            <HeroContentWrapper>
              <HeroText>
                <HeroSubTitle>{feature.bannerFeatureSubTitle}</HeroSubTitle>
                <HeroTitle>{feature.bannerFeatureTitle}</HeroTitle>
                <HeroDescriptionText>
                  {feature.bannerFeatureDescription}
                </HeroDescriptionText>
                <HeroCTA>{ctaModal}</HeroCTA>
              </HeroText>
              <HeroImage>
                <img
                  src={feature.bannerImage.file.url}
                  alt="FieldPro Mobile CRM"
                />
              </HeroImage>
            </HeroContentWrapper>
          </HeroWrapper>
          <BenefitsWrapper>
            <BenefitsContentWrapper>
              <BenfitsSectionTitle>
                {feature.keyBenefitsTitle}
              </BenfitsSectionTitle>
              <BenefitsBlockWrapper>
                <BenefitBlock>
                  <BenefitIcon>
                    <img src={feature.keyBenefitsIcon.file.url} alt="" />
                  </BenefitIcon>
                  <BenefitsTitle>
                    {" "}
                    {feature.keyBenefitsSmallTitle}
                  </BenefitsTitle>
                  <BenefitsDescription>
                    {feature.keyBenefitsDescription}
                  </BenefitsDescription>
                </BenefitBlock>
                <BenefitBlock>
                  <BenefitIcon>
                    <img src={feature.keyBenefitsIcon2.file.url} alt="" />
                  </BenefitIcon>
                  <BenefitsTitle>
                    {" "}
                    {feature.keyBenefitsSmallTitle2}
                  </BenefitsTitle>
                  <BenefitsDescription>
                    {feature.keyBenefitsDescription2}
                  </BenefitsDescription>
                </BenefitBlock>
                <BenefitBlock>
                  <BenefitIcon>
                    <img src={feature.keyBenefitsIcon3.file.url} alt="" />
                  </BenefitIcon>
                  <BenefitsTitle>
                    {" "}
                    {feature.keyBenefitsSmallTitle3}
                  </BenefitsTitle>
                  <BenefitsDescription>
                    {feature.keyBenefitsDescription3}
                  </BenefitsDescription>
                </BenefitBlock>
              </BenefitsBlockWrapper>
            </BenefitsContentWrapper>
          </BenefitsWrapper>
          <BodySectionWrapper>
            <BodyContentWrapper>
              <BodyImage>
                <img
                  src={feature.useCaseImage.file.url}
                  alt="FieldPro Mobile CRM"
                />
              </BodyImage>
              <BodyDescription>
                <BodyContentTitle> {feature.useCaseTitle}</BodyContentTitle>
                <BodyContentText>
                  {feature.useCaseDescription.useCaseDescription}
                </BodyContentText>
                <BodyTextCta>
                  <Link
                    to={feature.useCaseLink.json.content[0].content[1].data.uri}
                  >
                    <ArrowButton>
                      {" "}
                      {
                        feature.useCaseLink.json.content[0].content[1]
                          .content[0].value
                      }
                    </ArrowButton>
                  </Link>
                </BodyTextCta>
              </BodyDescription>
            </BodyContentWrapper>
            <BodyContentWrapper2>
              <BodyDescription>
                <BodyContentTitle>{feature.useCaseTitle2}</BodyContentTitle>
                <BodyContentText>
                  {feature.useCaseDescription2.useCaseDescription2}
                </BodyContentText>
                <BodyTextCta>
                  <Link
                    to={
                      feature.useCaseLink2.json.content[0].content[1].data.uri
                    }
                  >
                    <ArrowButton>
                      {" "}
                      {
                        feature.useCaseLink2.json.content[0].content[1]
                          .content[0].value
                      }
                    </ArrowButton>
                  </Link>
                </BodyTextCta>
              </BodyDescription>
              <BodyImage2>
                <img
                  src={feature.useCaseImage2.file.url}
                  alt="FieldPro Mobile CRM"
                />
              </BodyImage2>
            </BodyContentWrapper2>
            <BodyContentWrapper>
              <BodyImage>
                <img
                  src={feature.useCaseImage3.file.url}
                  alt="FieldPro Mobile CRM"
                />
              </BodyImage>
              <BodyDescription>
                <BodyContentTitle>{feature.useCaseTitle3}</BodyContentTitle>
                <BodyContentText>
                  {feature.useCaseDescription3.useCaseDescription3}
                </BodyContentText>
                <BodyTextCta>
                  <Link
                    to={
                      feature.useCaseLink3.json.content[0].content[1].data.uri
                    }
                  >
                    <ArrowButton>
                      {" "}
                      {
                        feature.useCaseLink3.json.content[0].content[1]
                          .content[0].value
                      }
                    </ArrowButton>
                  </Link>
                </BodyTextCta>
              </BodyDescription>
            </BodyContentWrapper>
          </BodySectionWrapper>

          <MoreSectionWrapper>
            <MoreSectionContentWrapper>
              <MoreSectionTitle>Explore more features</MoreSectionTitle>
              <MoreCardsWrapper>
                {shuffledArray.map(featureCards => {
                  return (
                    <Link
                      to={`/${featureCards.node.slug}`}
                      key={featureCards.node.id}
                    >
                      <CardWrapper>
                        <CardContentWrapper>
                          <CardIcon>
                            {" "}
                            <img
                              src={
                                featureCards.node.pageIconAsSeenOnMenu.file.url
                              }
                              alt=""
                            />
                          </CardIcon>
                          <CardTitle>
                            {featureCards.node.pageTitleAsSeenOnMenu}
                          </CardTitle>
                          <CardText>
                            {
                              featureCards.node
                                .pageDescriptionSeoMetaDescription
                            }
                          </CardText>
                          <RoundButtonDiagonalArrow />
                        </CardContentWrapper>
                      </CardWrapper>{" "}
                    </Link>
                  )
                })}
              </MoreCardsWrapper>
            </MoreSectionContentWrapper>
          </MoreSectionWrapper>
          <CtaBackground>
            <CtaIllustartion>
              <img
                src="/images/Geo-location.svg"
                style={{ width: "100vw" }}
                alt=""
              />
            </CtaIllustartion>
            <CtaWrapper>
              <CtaContent>
                <CtaTagline>{feature.cta}</CtaTagline>
                <CtaButtons>{bottomCtaModal}</CtaButtons>
              </CtaContent>
            </CtaWrapper>
          </CtaBackground>
        </PageWrapper>
        {footerMenu}
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}

export default FeatureTemplate

export const pageQuery = graphql`
  query FeaturePageBySlug($slug: String!) {
    contentfulFeaturesPage(slug: { eq: $slug }) {
      language
      bannerFeatureDescription
      bannerFeatureSubTitle
      bannerFeatureTitle
      bannerImage {
        file {
          url
        }
      }
      keyBenefitsDescription
      keyBenefitsDescription2
      keyBenefitsDescription3
      keyBenefitsIcon {
        file {
          url
        }
      }
      keyBenefitsIcon2 {
        file {
          url
        }
      }
      keyBenefitsIcon3 {
        file {
          url
        }
      }
      keyBenefitsSmallTitle
      keyBenefitsSmallTitle2
      keyBenefitsSmallTitle3
      keyBenefitsTitle
      language
      pageTitleAsSeenOnMenu
      pageNameSeoMetaTitle
      pageDescriptionSeoMetaDescription
      useCaseDescription {
        useCaseDescription
      }
      useCaseDescription2 {
        useCaseDescription2
      }
      useCaseDescription3 {
        useCaseDescription3
      }
      useCaseImage {
        file {
          url
        }
      }
      useCaseImage2 {
        file {
          url
        }
      }
      useCaseImage3 {
        file {
          url
        }
      }
      useCaseLink {
        json
      }
      useCaseLink2 {
        json
      }
      useCaseLink3 {
        json
      }
      useCaseTitle3
      useCaseTitle2
      useCaseTitle
      slug
      cta
    }
    allContentfulFeaturesPage {
      edges {
        node {
          language
          pageTitleAsSeenOnMenu
          pageShortDescriptionAsSeenOnMenu
          pageIconAsSeenOnMenu {
            file {
              url
            }
          }
          slug
          pageDescriptionSeoMetaDescription
        }
      }
    }
  }
`

//PageWrapper///////
const PageWrapper = styled.div``
////HEROSECTION/////////////
const HeroWrapper = styled.div`
  height: 840px;
  position: relative;
  @media (max-width: 1100px) {
    height: auto;
  }
`
const HeroContentWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 220px 0px 0 0px;
  display: grid;
  grid-template-columns: 600px auto;
  grid-gap: 80px;
  @media (max-width: 1100px) {
    grid-template-columns: auto;
    margin: 0;
    padding: 0px 40px 0px 40px;
    grid-gap: 20px;
  }
  @media (max-width: 550px) {
    grid-template-columns: auto;
    margin: 0;
    grid-gap: 20px;
    padding: 160px 24px 20px 24px;
  }
`
const HeroText = styled.div`
  padding: 80px 0px 80px 0px;

  @media (max-width: 1100px) {
    max-width: auto;
    padding: 160px 0px 80px 0px;
  }
  @media (max-width: 550px) {
    padding: 0px 0px 80px 0px;
    max-width: 400px;
  }
`
const HeroImage = styled.div`
  img {
    width: 1200px;

    position: absolute;
    right: -400px;
    top: 160px;
  }
  @media (max-width: 1100px) {
    img {
      position: relative;
      right: 0px;
      top: 0px;
      width: auto;
      margin: 0px 0px 80px 0px;
    }
  }
  @media (max-width: 550px) {
    img {
      position: relative;

      width: auto;
      margin: 0px 0px 80px 0px;
    }
  }
`
const HeroSubTitle = styled(P)`
  color: #6c6c6c;
  max-width: 50ch;
`
const HeroTitle = styled(H1)`
  margin-bottom: 20px;
`
const HeroDescriptionText = styled(P)`
  margin-bottom: 32px;
  max-width: 480px;
`
const HeroCTA = styled.div`
  display: flex;
  flex-direction: row;
  gap: 16px;
`
const CTAClassWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 20px;
  @media (max-width: 1100px) {
    grid-template-columns: repeat(2, auto);
    margin: 0;
    padding: 0px;
  }
  @media (max-width: 550px) {
    grid-template-columns: repeat(1, auto);
    margin: 0;
    padding: 0;
  }
`

////////BENEFITS SECTION ////////
const BenefitsWrapper = styled.div``
const BenefitsContentWrapper = styled.div`
  max-width: 1280px;
  margin: 120px auto 160px auto;
  @media (max-width: 1100px) {
    grid-template-columns: auto;
    margin: 0;
    padding: 80px 40px 80px 40px;
  }
  @media (max-width: 550px) {
    padding: 0px 40px 0px 40px;
    grid-template-columns: auto;
    margin: 0;
    padding: 0;
  }
`
const BenfitsSectionTitle = styled(H2)`
  max-width: 800px;
  margin-bottom: 80px;

  @media (max-width: 1100px) {
    max-width: 600px;
    margin-bottom: 40px;
    padding: 40px 20px 0px 20px;
  }
  @media (max-width: 550px) {
    max-width: 350px;
    margin-bottom: 80px;
    padding: 40px 20px 0px 20px;
  }
`
const BenefitsBlockWrapper = styled.div`
  display: grid;
  --min-column-width: min(320px, 100%);
  grid-template-columns: repeat(
    auto-fill,
    minmax(var(--min-column-width), 1fr)
  );
  gap: 40px;
`
const BenefitBlock = styled.div``
const BenefitIcon = styled.div`
  img {
    padding: 0;
    margin: 0;
  }
`
const BenefitsTitle = styled(H3)`
  line-height: 1.5em;
`
const BenefitsDescription = styled(P)`
  margin: 0;
`

///////BODY CONTENT SECTION ////////
const BodySectionWrapper = styled.div`
  position: relative;
`
const BodyContentWrapper = styled.div`
  max-width: 1280px;
  margin: 240px auto;

  display: grid;
  grid-template-columns: 562px 552px;
  grid-gap: 120px;

  @media (max-width: 1100px) {
    padding: 80px 40px 80px 40px;
    margin: 0;
    display: grid;
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
  @media (max-width: 550px) {
    padding: 0px 40px 0px 40px;
    margin: 40px 0;
    display: grid;
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
`
const BodyContentWrapper2 = styled.div`
  max-width: 1280px;
  margin: 240px auto;

  display: grid;
  grid-template-columns: 562px 552px;
  grid-gap: 120px;

  @media (max-width: 1100px) {
    padding: 80px 40px 80px 40px;
    margin: 160px 0;
    display: flex;
    flex-direction: column-reverse;
    grid-gap: 40px;
  }
  @media (max-width: 550px) {
    padding: 0px 40px 0px 40px;
    margin: 40px 0;
    display: flex;
    flex-direction: column-reverse;
    grid-gap: 40px;
  }
`

const BodyImage = styled.div`
  margin: 0 0 0 -160px;
  img {
    width: 800px;
  }
  @media (max-width: 1100px) {
    margin: 0px;
    img {
      width: auto;
    }
  }
`
const BodyImage2 = styled.div`
  margin: 0 -160px 0 0px;
  img {
    width: 800px;
  }
  @media (max-width: 1100px) {
    margin: 0px;
    img {
      width: auto;
    }
  }
`
const BodyDescription = styled.div`
  padding-top: 80px;
`
const BodyContentTitle = styled(H3)``
const BodyContentText = styled(P)``
const BodyTextCta = styled.p`
  font-weight: 600;
  font-size: 1em;

  cursor: pointer;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  a {
    color: #febd55;
  }
`

/////MORE SECTION ///////
const MoreSectionWrapper = styled.div``
const MoreSectionContentWrapper = styled.div`
  max-width: 1280px;
  margin: 160px auto;
  @media (max-width: 1100px) {
    padding: 80px 40px 80px 40px;
  }
  @media (max-width: 550px) {
    padding: 0px 40px 0px 40px;
    margin: 80px auto;
  }
`
const MoreSectionTitle = styled(H2)`
  margin-bottom: 80px;
  @media (max-width: 550px) {
    padding: 0px 0px 0px 0px;
    margin: 0px auto;
  }
`
const MoreCardsWrapper = styled.div`
  display: grid;
  --min-column-width: min(320px, 100%);
  grid-template-columns: repeat(
    auto-fill,
    minmax(var(--min-column-width), 1fr)
  );
  gap: 12px;
`
const CardContentWrapper = styled.div``
const CardWrapper = styled.div`
  padding: 24px 40px 24px 40px;
  max-width: 400px;
  border-radius: 4px;

  box-shadow: 0 0.2em 0.8em #124e5d10;
  background-color: #fefefe;
  @media (max-width: 1100px) {
    max-width: auto;
    padding: 20px;
  }
  @media (max-width: 550px) {
    max-width: 320px;
    padding: 20px;
  }
  :hover {
    background-color: #f7f9fa;
  }
`
const CardIcon = styled.div`
  img {
    width: 48px;
    padding: 0;
    margin: 0;
  }
`
const CardTitle = styled(H3)`
  line-height: 1em;
`
const CardText = styled(P)``

////////BOTTOM CTA ////////
const CtaBackground = styled.div`
  background-color: #124e5d;
  width: 100vw;
  position: relative;
  left: -8px;
`
const CtaWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 200px 0 200px 0;
  @media (max-width: 1100px) {
    padding: 100px 48px 100px 48px;
  }
  @media (max-width: 512px) {
    padding: 80px 24px 80px 24px;
  }
`
const CtaIllustartion = styled.div`
  position: absolute;

  align-self: center;
  top: 100px;
`
const CtaContent = styled.div`
  position: relative;
  z-index: 1;
  max-width: 1280px;
  padding: 0 0 0 0;
  @media (max-width: 1100px) {
    padding: 0;
  }
  @media (max-width: 512px) {
    padding: 0px;
  }
`
const CtaTagline = styled(H2)`
  font-size: 3.25em;
  color: #fefefe;
  max-width: 600px;
  margin: 0 0 20px 0;
  @media (max-width: 1100px) {
    padding: 0px;
  }
  @media (max-width: 512px) {
    padding: 0px;
    max-width: 320px;
    font-size: 2.5em;
    margin: 0 0 40px 0;
  }
`
const CtaButtons = styled.div``

const BottomCTAWrapper = styled.div`
  display: grid;
  grid-template-columns: 240px auto;
  grid-gap: 20px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    margin: 0;
    padding: 0px;
  }
  @media (max-width: 550px) {
    grid-template-columns: repeat(1, auto);
    margin: 0;
    padding: 0;
  }
`
