import React from "react"
import { Link, graphql } from "gatsby"
import styled from "styled-components"
import { BLOCKS, INLINES } from "@contentful/rich-text-types"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"

import { ButtonChip } from "../components/Buttons/CtaButton.js"
import Layout from "../components/Layout.js"
import SEO from "../components/seo.js"

import NewHeader from "../components/sections/HeaderNew.js"
import NewHeaderES from "../components/sections/HeaderNewES.js"
import NewHeaderFR from "../components/sections/HeaderNewFR.js"
import Footer from "../components/sections/Footer.js"
import FooterFR from "../components/sections/FooterFR.js"
import FooterES from "../components/sections/FooterES.js"
import ScrollToTop from "../components/Homepage-components/ScrollToTop.js"
import {
  H1,
  H2,
  H3,
  P,
  BlogText,
  QuoteText,
  H4,
} from "../components/styles/TextStyles.js"

import CookiesBar from "../components/sections/cookies.js"

import LightCta from "../components/sections/LightCta.js"
import LightCtaFR from "../components/sections/LightCtaFR.js"
import LightCtaES from "../components/sections/LightCtaES.js"
import DarkCtaFR from "../components/sections/DarkCtaFR.js"
import DarkCtaES from "../components/sections/DarkCtaES.js"
import DarkCta from "../components/sections/DarkCta.js"

class WorkflowTemplate extends React.Component {
  render() {
    const workflow = this.props.data.contentfulWorkflowTemplate
    const cards = this.props.data.allContentfulWorkflowTemplate.edges
    const filteredArray = cards.filter(cardoption => {
      return (
        workflow.language === cardoption.node.language &&
        workflow.title !== cardoption.node.title
      )
    })
    const _ = require("lodash")

    let shuffledArray = _.sampleSize(filteredArray, 3)

    const options = {
      renderNode: {
        [BLOCKS.HEADING_1]: (node, children) => <H1>{children}</H1>,
        [BLOCKS.HEADING_2]: (node, children) => <H2>{children}</H2>,
        [BLOCKS.HEADING_3]: (node, children) => <H3>{children}</H3>,
        [BLOCKS.PARAGRAPH]: (node, children) => <BlogText>{children}</BlogText>,
        [BLOCKS.EMBEDDED_ENTRY]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [BLOCKS.EMBEDDED_ASSET]: (node, children) => (
          <Image>
            <img
              src={`https:${node.data.target.fields.file["en-US"].url}`}
              alt=""
            />
          </Image>
        ),
        [INLINES.EMBEDDED_ENTRY]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [INLINES.HYPERLINK]: (node, children) => {
          if (node.data.uri.includes("player.vimeo.com/video")) {
            return (
              <IframeContainer>
                <iframe
                  title="Unique Title 001"
                  src={node.data.uri}
                  frameBorder="0"
                  allowFullScreen
                ></iframe>
              </IframeContainer>
            )
          } else if (node.data.uri.includes("youtube.com/embed")) {
            return (
              <IframeContainer>
                <iframe
                  title="Unique Title 002"
                  src={node.data.uri}
                  allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
                  frameBorder="0"
                  allowFullScreen
                ></iframe>
              </IframeContainer>
            )
          } else {
            return (
              <Link to={node.data.uri} target="_blank">
                {children}
              </Link>
            )
          }
        },

        [INLINES.ENTRY_HYPERLINK]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
        [INLINES.ASSET_HYPERLINK]: (node, children) => (
          <Link to={node.data.uri} target="_blank">
            {children}
          </Link>
        ),
      },
      renderMark: {},
    }

    var headerMenu
    if (workflow.language === "EN") {
      headerMenu = (
        <NewHeader
          en={`/${workflow.slug}`}
          fr={`/${workflow.slug}-fr`}
          es={`/${workflow.slug}-es`}
        />
      )
    }
    if (workflow.language === "FR") {
      var currentSlug = workflow.slug
      currentSlug = currentSlug.substring(0, currentSlug.length - 3)
      headerMenu = (
        <NewHeaderFR
          en={`/${currentSlug}`}
          fr={`/${currentSlug}-fr`}
          es={`/${currentSlug}-es`}
        />
      )
    }
    if (workflow.language === "ES") {
      var currentSlugEs = workflow.slug
      currentSlugEs = currentSlugEs.substring(0, currentSlugEs.length - 3)
      headerMenu = (
        <NewHeaderES
          en={`/${currentSlugEs}`}
          fr={`/${currentSlugEs}-fr`}
          es={`/${currentSlugEs}-es`}
        />
      )
    }

    var backLink
    if (workflow.language === "EN") {
      backLink = (
        <Link to="/workflowtemplates">
          <BackGroup>
            <BackIcon
              src={require("../../static/images/Icons/arrow_back-24px.svg")}
            />
            <BackLabel>Back to All Templates</BackLabel>
          </BackGroup>
        </Link>
      )
    }
    if (workflow.language === "FR") {
      backLink = (
        <Link to="/workflowtemplates-fr">
          <BackGroup>
            <BackIcon
              src={require("../../static/images/Icons/arrow_back-24px.svg")}
            />
            <BackLabel>Retour à tous les modèles</BackLabel>
          </BackGroup>
        </Link>
      )
    }
    if (workflow.language === "ES") {
      backLink = (
        <Link to="/workflowtemplates-es">
          <BackGroup>
            <BackIcon
              src={require("../../static/images/Icons/arrow_back-24px.svg")}
            />
            <BackLabel>Volver a todas las plantillas</BackLabel>
          </BackGroup>
        </Link>
      )
    }
    //Modal
    var ctaModal
    if (workflow.language === "EN") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCta />
        </CTAClassWrapper>
      )
    }
    if (workflow.language === "FR") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCtaFR />
        </CTAClassWrapper>
      )
    }
    if (workflow.language === "ES") {
      ctaModal = (
        <CTAClassWrapper>
          <LightCtaES />
        </CTAClassWrapper>
      )
    }

    //BottomModal
    var bottomCtaModal
    if (workflow.language === "EN") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCta />
        </BottomCTAWrapper>
      )
    }
    if (workflow.language === "FR") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCtaFR />
        </BottomCTAWrapper>
      )
    }
    if (workflow.language === "ES") {
      bottomCtaModal = (
        <BottomCTAWrapper>
          <DarkCtaES />
        </BottomCTAWrapper>
      )
    }

    var footerMenu
    if (workflow.language === "EN") {
      footerMenu = <Footer />
    }
    if (workflow.language === "FR") {
      footerMenu = <FooterFR />
    }
    if (workflow.language === "ES") {
      footerMenu = <FooterES />
    }
    //Background image for testimonials section
    var background = workflow.testimonialBackgroundImage.file.url
    return (
      <Layout location={this.props.location}>
        <SEO
          title={workflow.metaTitle}
          description={workflow.metaDescription}
          //   mainImage={`https:${workflow.image.file.url}`}
        />
        {headerMenu}
        <BackContainerTop>{backLink}</BackContainerTop>
        {/* <TitleGroup>
          <Title>{workflow.title}</Title>
          <TagsGroup>
            {this.props.data.contentfulWorkflowTemplate.tags.map(
              wftemplatetags => {
                return <ButtonChip>{wftemplatetags.title}</ButtonChip>
              }
            )}
          </TagsGroup>
        </TitleGroup> */}
        <TitleGroup>
          <TextGroup>
            <Title>{workflow.title}</Title>
            <Description>{workflow.metaDescription}</Description>
            <CallToAction>{ctaModal}</CallToAction>
          </TextGroup>
          <ImageGroup>
            {" "}
            <img src={workflow.workflowIcon.file.url} alt="" />
          </ImageGroup>
        </TitleGroup>
        <BodyWrapper>
          <Sidebar>
            <Title2>Categories</Title2>
            <TagsGroup>
              {this.props.data.contentfulWorkflowTemplate.tags.map(
                wftemplatetags => {
                  return <ButtonChip>{wftemplatetags.title}</ButtonChip>
                }
              )}
            </TagsGroup>
          </Sidebar>
          <BodyContent>
            {documentToReactComponents(workflow.content.json, options)}
          </BodyContent>
          {/* <SocialshareButtons>
            <FacebookShareButton
              url={shareUrl + `/${shareSlug}`}
              title={workflow.title}
            >
              <FacebookIcon
                size={48}
                borderRadius="12"
                iconFillColor="#fefefe"
              />
            </FacebookShareButton>
            <TwitterShareButton
              url={shareUrl + `/${shareSlug}`}
              title={workflow.title}
            >
              <TwitterIcon
                size={48}
                borderRadius="12"
                iconFillColor="#fefefe"
              />
            </TwitterShareButton>
            <LinkedinShareButton
              url={shareUrl + `/${shareSlug}`}
              title={workflow.title}
            >
              <LinkedinIcon
                size={48}
                borderRadius="12"
                iconFillColor="#fefefe"
              />
            </LinkedinShareButton>
          </SocialshareButtons> */}
        </BodyWrapper>
        <TestimonialSection style={{ backgroundImage: `url(${background})` }}>
          <TestimonialCardWrapper>
            <TestimonialCardBox>
              <TextBlock>
                <Quotation>"</Quotation>
                <Quote>{workflow.quote.quote}"</Quote>
              </TextBlock>
              <Citation>
                <PersonName>{workflow.citationName} </PersonName>
                <Position>{workflow.citationPosition}</Position>

                <CompanyName>{workflow.citationCompany}</CompanyName>
              </Citation>
            </TestimonialCardBox>
          </TestimonialCardWrapper>
        </TestimonialSection>
        <MoreTemplatesWrapper>
          <MoreSectionTitle>{workflow.templatesSectionTitle}</MoreSectionTitle>
          <WorkflowTemplatesWrapper>
            {shuffledArray.map(wfCards => {
              // if (
              //   workflow.language === wfCards.node.language &&
              //   workflow.title !== wfCards.node.title
              // ) {
              return (
                <Link to={`/${wfCards.node.slug}`} key={wfCards.node.id}>
                  <CardTemplateWrapper>
                    <CardTemplateIcon>
                      {<img src={wfCards.node.workflowIcon.file.url} alt="" />}
                    </CardTemplateIcon>
                    <CardTemplateTitle>{wfCards.node.title}</CardTemplateTitle>
                    <CardTemplateDescription>
                      {wfCards.node.metaDescription}
                    </CardTemplateDescription>
                    <CardDividerLine></CardDividerLine>
                    <CardChipGroup>
                      {wfCards.node.tags.map(templatetags => {
                        return <ButtonChip>{templatetags.title}</ButtonChip>
                      })}
                    </CardChipGroup>
                  </CardTemplateWrapper>
                </Link>
              )
              // }
              // return null
            })}
          </WorkflowTemplatesWrapper>
        </MoreTemplatesWrapper>

        <BottomCallToAction>
          <CtaBackground>
            <img
              style={{ width: "100%" }}
              src="/images/Geo-location.svg"
              alt="Illustration"
            />
          </CtaBackground>
          <CtaContent>
            <CtaText>
              <Tagline style={{ color: "#fefefe" }}>
                {workflow.callToAction}
              </Tagline>
              <CtaGroup>{bottomCtaModal}</CtaGroup>
            </CtaText>
          </CtaContent>
        </BottomCallToAction>
        {footerMenu}
        <ScrollToTop />
        <CookiesBar />
      </Layout>
    )
  }
}

export default WorkflowTemplate

export const WorkflowPostQuery = graphql`
  query WorkflowPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        siteUrl
      }
    }
    allContentfulWorkflowTemplate {
      edges {
        node {
          title
          language
          metaDescription
          slug
          workflowIcon {
            file {
              url
            }
          }
          tags {
            title
          }
        }
      }
    }
    contentfulWorkflowTemplate(slug: { eq: $slug }) {
      metaTitle
      metaDescription
      language
      slug
      title
      content {
        json
      }
      quote {
        quote
      }
      citationName
      citationPosition
      citationCompany
      callToAction
      testimonialBackgroundImage {
        file {
          url
        }
      }
      templatesSectionTitle
      tags {
        title
      }
      workflowIcon {
        file {
          url
        }
      }
    }
  }
`
const BackContainerTop = styled.div`
  padding: 0;

  max-width: 1280px;
  margin: 200px auto 40px auto;
  @media (max-width: 32em) {
    margin: 120px 16px 0 16px;
  }
`

const BackGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  justify-content: flex-start;
  width: 60em;
  margin: 0;
  padding: 0;
  :hover {
    transform: scale(1.1) translate(2.5em, 0);
  }
  @media (max-width: 830px) {
    width: 520px;
    padding: 0 32px;
  }
  @media (max-width: 512px) {
    padding: 0;
    width: 320px;
  }
`
const BackIcon = styled.img`
  align-self: center;
  height: 32px;
  width: 32px;
  margin: 0 0.4em 0 0;
  padding: 0;
`
const BackLabel = styled.div`
  align-self: center;
  font-weight: 600;
  font-size: 16px;
  color: #6c6c6c;
  margin: 0;
  padding: 0;
`

const MoreTemplatesWrapper = styled.div`
  max-width: 1280px;
  margin: 60px auto 120px auto;
  @media (max-width: 830px) {
    max-width: 720px;
    margin: 40px auto 40px auto;
  }
  @media (max-width: 512px) {
    max-width: 350px;
    margin: 40px auto 40px auto;
  }
`
const MoreSectionTitle = styled(H3)`
  @media (max-width: 830px) {
    max-width: 720px;
    margin: 0px 14px 80px 16px;
  }
  @media (max-width: 512px) {
    max-width: 350px;
    margin: 0px 14px 60px 14px;
  }
`

const Title = styled(H1)`
  margin: 0 0 16px 0;
`

const Image = styled.div``
const BodyWrapper = styled.div`
  max-width: 72em;
  margin: 3em auto 4em 324px;
  display: grid;
  grid-template-columns: repeat(2, auto);
  gap: 120px;

  @media (max-width: 830px) {
    max-width: 720px;
    display: grid;
    grid-template-columns: repeat(1, auto);
    gap: 40px;
    margin: 0;
    padding: 0;
  }
  @media (max-width: 512px) {
    max-width: 320px;
    display: grid;
    grid-template-columns: repeat(1, auto);
    gap: 40px;
    margin: 0;
    padding: 0;
  }
`
const BodyContent = styled.div`
  display: grid;
  grid-template-columns: repeat(1, auto);
  justify-self: left;
  max-width: 800px;
  padding: 0;
  img {
    margin: 1em 1em 1em 0;
    padding: 0;
    width: 39em;
    max-height: 40em;
    object-fit: contain;
  }
  a {
    font-size: 1em;
    font-weight: 600;
    line-height: 1.5;
    color: #febd55;
    text-decoration: none;
    cursor: pointer;
    margin: 0;
    padding: 0.1em;
    :hover {
      font-weight: 700;
      border-bottom: none;
      padding-bottom: 0;
    }
  }
  @media (max-width: 830px) {
    margin: 0 32px;
  }
  @media (max-width: 512px) {
    margin: 0 1em 2em 1em;
    width: 19em;
    img {
      margin: 1em 1em 1em 0;
      width: 18.5em;
    }
    p,
    a {
      margin: 0 1em 1em 0;
      width: 15em;
    }
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 1em 1em 1em 0;
      width: 10em;
    }
    ul,
    li {
      margin: 0 10em 0 0.5em !important;
      width: 12em !important;
    }
  }
`

//Testimonial section edits
const TestimonialSection = styled.div`
  max-width: 1280px;
  margin: 240px auto;

  img {
    height: 600px;
    width: 1280px;
  }
  @media (max-width: 450px) {
    max-width: 400px;
    margin: 120px 0 120px 0;
    padding: 40px 0px 40px 0px;
  }
`

const TestimonialCardWrapper = styled.div`
  padding: 72px 0px 72px 80px;
  @media (max-width: 450px) {
    max-width: 360px;
    padding: 0px 0px 0px 0px;
  }
`
const TestimonialCardBox = styled.div`
  max-width: 600px;
  box-shadow: 0 2px 24px 0 rgba(0, 0, 0, 0.08);
  background-color: #fefefe;

  @media (max-width: 450px) {
    max-width: 360px;

    padding: 20px 20px;
  }
`
const TextBlock = styled.div`
  padding: 40px 40px 0px 40px;
  display: grid;
  grid-template-columns: auto auto;
  @media (max-width: 450px) {
    padding: 10px 10px 0 10px;
  }
`
const Quotation = styled(QuoteText)`
  position: absolute;
  left: 500;
  margin-left: -12px;
`
const Quote = styled(QuoteText)``
const Citation = styled.div`
  padding: 0px 40px 0 40px;
  display: grid;
  grid-template-rows: 20px 20px auto;
  grid-gap: 10px;
  @media (max-width: 450px) {
    gap: 20px;
    @media (max-width: 450px) {
      padding: 10px 10px 0 10px;
    }
  }
`
const PersonName = styled.p`
  font-size: 20px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.2;
  letter-spacing: normal;
  color: #2c2c2c;
`

const Position = styled.p`
  font-size: 20px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.5;
  letter-spacing: normal;
  color: #2c2c2c;
  @media (max-width: 450px) {
    line-height: 1;
  }
`
const CompanyName = styled.p`
  font-size: 20px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.5;
  letter-spacing: normal;
  color: #2c2c2c;
`
//Youtube video embed
const IframeContainer = styled.span`
  padding-bottom: 56.25%;
  position: relative;
  display: block;
  width: 100%;

  > iframe {
    height: 100%;
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;
  }
`
//Share buttons styling

/// MORE TEMPLATES SECTION
const WorkflowTemplatesWrapper = styled.div`
  max-width: 1280px;
  margin: 60px auto 120px auto;
  display: grid;
  grid-template-columns: repeat(3, auto);
  gap: 40px;
  @media (max-width: 830px) {
    max-width: 720px;
    margin: 0;
    padding: 0;
    grid-template-columns: repeat(2, auto);
    gap: 30px;
  }
  @media (max-width: 512px) {
    max-width: 320px;
    margin: 0;
    padding: 0;
    grid-template-columns: repeat(1, auto);
    gap: 30px;
  }
`

//CARDS CSS
const CardTemplateWrapper = styled.div`
  width: 400px;
  border: 1px solid #979797;
  border-radius: 5px;
  padding: 40px;
  :hover {
    border: 2px solid #2c2c2c;
  }
  @media (max-width: 830px) {
    padding: 20px;
    width: 320px;
  }
  @media (max-width: 512px) {
    padding: 20px;
    width: 320px;
  }
`
const CardTemplateIcon = styled.div`
  height: 54px;
  width: 54px;
`
const CardTemplateTitle = styled(H4)`
  margin: 20px 0 10px 0;
`
const CardTemplateDescription = styled(P)``
const CardDividerLine = styled.div`
  width: 320px;
  height: 1px;
  background: #d8d8d8;
  padding: 0;
  margin: 40px 0 20px 0;
  @media (max-width: 830px) {
    width: 280px;
  }
  @media (max-width: 512px) {
    width: 280px;
  }
`
const CardChipGroup = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
`
///////////////////
const Sidebar = styled.div``
const TagsGroup = styled.div`
  padding: 0;
  margin: 0;
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 830px) {
    margin: 40px 32px;
  }
  @media (max-width: 512px) {
    margin: 32px 16px;
  }
`
const TitleGroup = styled.div`
  padding: 0;
  max-width: 1280px;
  display: grid;
  grid-template-columns: repeat(2, auto);
  margin: 0px auto 40px auto;
  @media (max-width: 830px) {
    margin: 40px 32px;
    grid-template-columns: repeat(1, auto);
    grid-gap: 48px;
  }
  @media (max-width: 512px) {
    margin: 64px 16px;
    grid-template-columns: repeat(1, auto);
    grid-gap: 48px;
  }
`
const TextGroup = styled.div``
const Title2 = styled(H4)`
  @media (max-width: 830px) {
    margin: 24px 32px;
  }
  @media (max-width: 512px) {
    margin: 24px 16px;
  }
`

const ImageGroup = styled.div`
  img {
    width: 160px;
    height: 160px;
  }
  margin: 0px 0 0 0;
`

const Description = styled(P)`
  margin: 0 0 16px 0;
  max-width: 600px;
`
const CallToAction = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  margin: 40px 0 60px 0px;
  @media (max-width: 450px) {
    margin: 20px 0 0 0;
    flex-direction: column;
    grid-gap: 20px;
  }
`

const BottomCallToAction = styled.div`
  position: relative;
  display: grid;
  background-color: #124e5d;
  width: 100vw;
  left: -8px;
  margin: 200px 0 280px 0px;
  padding: 0;
  @media (max-width: 830px) {
    width: 100vw;
    left: 0;
    margin: 80px 0 0 0px;
  }
  @media (max-width: 450px) {
    width: 370px !important;
    margin: 100px 0px;
    left: 0;
  }
`
const CtaBackground = styled.div`
  position: absolute;
  z-index: 1;
  align-self: center;
  width: 100%;
`

const CtaContent = styled.div`
  position: relative;
  display: grid;
  max-width: 72em;
  margin: 120px 0 160px 280px;
  box-shadow: 0 0.5em 2em #2c2c2c18;

  padding: 0;
  @media (max-width: 830px) {
    margin: 80px 60px 40px 40px;

    padding: 0;
  }
  @media (max-width: 450px) {
    width: 320px;
    margin: 40px 16px 40px 16px;
  }
`
const Tagline = styled(H2)`
  padding: 10px 0px 0px 0px;
  max-width: 900px;
  @media (max-width: 450px) {
    padding: 10px 0px 40px 0px;
    width: auto;
  }
`
const CtaText = styled.div`
  position: relative;
  align-self: center;

  color: #2a2a2a;
  max-width: 56em;

  z-index: 2;
  h2 {
    font-size: 3.25em;
  }
  @media (max-width: 32em) {
    width: 24em;
    margin: 0;
    padding: 0;
    h2 {
      font-size: 2.5em;
      margin: 1.2em 1.2em 1em 0;
    }
  }
  @media (max-width: 450px) {
    max-width: 360px;
    margin: 0;
    padding: 0;
  }
`

const CtaGroup = styled.div`
  margin: -20px 0 0 0;
  max-width: 640px;
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 450px) {
    flex-direction: column;
  }
`
////CTA AT TOP
const CTAClassWrapper = styled.div`
  display: grid;
  grid-template-columns: 240px auto;
  grid-gap: 20px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    margin: 0;
    padding: 0px;
  }
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
    margin: 0;
    padding: 0;
  }
`
////CTA AT BOTTOM
const BottomCTAWrapper = styled.div`
  display: grid;
  grid-template-columns: 240px auto;
  grid-gap: 20px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    margin: 0;
    padding: 0px;
  }
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
    margin: 0;
    padding: 0;
  }
`
