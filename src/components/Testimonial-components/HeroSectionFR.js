import { Link } from "gatsby"
import React from "react"
import styled from "styled-components"
import { DownloadButtonArrow } from "../Buttons/CtaButton"
import { Caption, H1, P } from "../styles/TextStyles"

function HeroSectionFR() {
  return (
    <Wrapper>
      <Content>
        <Title>Ce que nos clients disent de nous</Title>
        <Description>
          Découvrez comment nos clients supervisent en temps réel leurs ventes
          et leurs opérations sur le terrain à l’aide de notre application Web
          et mobile FieldPro. Comprenez comment ils utilisent notre outil pour
          automatiser la gestion de leurs agents commerciaux, identifier les
          clients à visiter et digitaliser l’organisation de leur distribution.
        </Description>
        <CallToAction>
          <Link to="/book-a-demo-fr">
            <DownloadButtonArrow id="bookdemo">
              Demander une demo
            </DownloadButtonArrow>
          </Link>

          <TrialGroup>
            {/* <Link to="/signup-fr">
              <DownloadButtonArrow id="start_trial">
                Essai gratuit
              </DownloadButtonArrow>
            </Link>
            <DisclaimerText>*Sans engagement</DisclaimerText> */}
          </TrialGroup>
        </CallToAction>
      </Content>
    </Wrapper>
  )
}
export default HeroSectionFR

const Wrapper = styled.div`
  background-color: #fefefe;
`
const Content = styled.div`
  max-width: 1234px;
  padding: 300px 40px 200px;
  margin: 0 auto;
  @media (max-width: 450px) {
    padding: 200px 40px 80px;
  }
`
const Title = styled(H1)`
  color: #2c2c2c;
  padding-bottom: 20px;
`
const Description = styled(P)`
  max-width: 880px;
`
const CallToAction = styled.div`
  display: grid;
  grid-template-columns: 350px auto;
  grid-gap: 20px;
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
  }
`
export const TrialGroup = styled.div``
export const DisclaimerText = styled(Caption)`
  color: #6c6c6c;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
