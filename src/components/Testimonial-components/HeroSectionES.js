import { Link } from "gatsby"
import React from "react"
import styled from "styled-components"
import { DownloadButtonArrow } from "../Buttons/CtaButton"
import { Caption, H1, P } from "../styles/TextStyles"

function HeroSectionES() {
  return (
    <Wrapper>
      <Content>
        <Title>Lo que nuestros clientes dicen</Title>
        <Description>
          Conozca cómo nuestros clientes monitorean en tiempo real sus ventas y
          operaciones en campo utilizando nuestra aplicación móvil y web
          FieldPro. Comprenda cómo utilizan nuestra plataforma para la
          automatización de la fuerza de ventas, mapeo de puntos de venta y
          digitalización del manejo de su distribución.
        </Description>
        <CallToAction>
          <Link to="/book-a-demo-es">
            <DownloadButtonArrow id="bookdemo">
              Reserva una demo
            </DownloadButtonArrow>
          </Link>

          <TrialGroup>
            {/* <Link to="/signup-es">
              <DownloadButtonArrow id="start_trial">
                Empiza la prueba gratis
              </DownloadButtonArrow>
            </Link>
            <DisclaimerText> *No se requiere tarjeta de crédito</DisclaimerText> */}
          </TrialGroup>
        </CallToAction>
      </Content>
    </Wrapper>
  )
}
export default HeroSectionES

const Wrapper = styled.div`
  background-color: #fefefe;
`
const Content = styled.div`
  max-width: 1280px;
  padding: 300px 40px 200px;
  margin: 0 auto;
  @media (max-width: 450px) {
    padding: 200px 40px 80px;
  }
`
const Title = styled(H1)`
  color: #2c2c2c;
  padding-bottom: 20px;
`
const Description = styled(P)`
  max-width: 880px;
`
const CallToAction = styled.div`
  display: grid;
  grid-template-columns: 350px auto;
  grid-gap: 20px;
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
  }
`
export const TrialGroup = styled.div``
export const DisclaimerText = styled(Caption)`
  color: #6c6c6c;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
