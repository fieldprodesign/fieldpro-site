import { Link } from "gatsby"
import React from "react"
import styled from "styled-components"
import { DownloadButtonArrow } from "../Buttons/CtaButton"
import { Caption, H1, P } from "../styles/TextStyles"

function HeroSection() {
  return (
    <Wrapper>
      <Content>
        <Title>What Our Customers Have To Say</Title>
        <Description>
          Learn how our customers monitor in real-time their field sales and
          conduct retail audit using our FieldPro web and mobile app. Explore
          how they use our platform for sales force automation, to map outlets,
          and to digitise their road to market processes.
        </Description>
        <CallToAction>
          <Link to="/book-a-demo">
            <DownloadButtonArrow id="bookdemo">Book a Demo</DownloadButtonArrow>
          </Link>

          <TrialGroup>
            {/* <Link to="/signup">
              <DownloadButtonArrow id="start_trial">
                Start Free Trial
              </DownloadButtonArrow>
            </Link>
            <DisclaimerText> *No credit card required</DisclaimerText> */}
          </TrialGroup>
        </CallToAction>
      </Content>
    </Wrapper>
  )
}
export default HeroSection

const Wrapper = styled.div`
  background-color: #fefefe;
`
const Content = styled.div`
  max-width: 1280px;
  padding: 300px 40px 200px;
  margin: 0 auto;
  @media (max-width: 450px) {
    padding: 200px 40px 80px;
  }
`
const Title = styled(H1)`
  color: #2c2c2c;
  padding-bottom: 20px;
`
const Description = styled(P)`
  max-width: 880px;
`
const CallToAction = styled.div`
  display: grid;
  grid-template-columns: 250px auto;
  grid-gap: 20px;
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
  }
`
export const TrialGroup = styled.div``
export const DisclaimerText = styled(Caption)`
  color: #6c6c6c;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
