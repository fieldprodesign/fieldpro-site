import React from "react"
import { Link } from "gatsby"

import Layout from "../Layout"
import { H3, P } from "../styles/TextStyles.js"
import {
  FooterContainer,
  FooterGroup,
  FooterLogo,
  LinkGroup,
  Copyright,
  BottomFooterGroup,
  SocialIconsGroup,
  SocialIcon,
} from "../sections/Footer.js"

const FooterES = () => {
  return (
    <Layout>
      <FooterContainer>
        <FooterGroup>
          <FooterLogo>
            <Link to="https://fieldproapp.com/es">
              <img
                src={require("../../../static/images/optimetriks-logo.png")}
                alt=""
              />
            </Link>
          </FooterLogo>
          <LinkGroup>
            <H3>Soluciones</H3>
            <Link to="/retail-execution-es"> Auditoría punto de venta </Link>
            <Link to="/sales-es"> Automatización de ventas</Link>
            <Link to="/field-force-tracking-es">
              Monitoreo de fuerza de trabajo
            </Link>
            <Link to="/mobilecrm-es"> CRM Móvil</Link>
            <Link to="/mobile-inspection-es"> Inspecciones en terreno</Link>
            <Link to="/workflowbuilder-es">Editeur de workflow</Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Industrias</H3>

            <Link to="/consumer-goods-es">Consumo masivo</Link>
            <Link to="/financial-services-es"> Servicios financieros</Link>
            <Link to="/energy-access-es"> Acceso a la energía</Link>
            <Link to="/agriculture-es"> Agricultura </Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Recursos</H3>
            <Link to="/testimonials-es">Testimonios</Link>
            <Link to="/blog-es">Casos de Estudio</Link>
            <Link to="/partners-es">Ser un socio</Link>
            {/* <Link to="/find-a-partner-es">Encuentra un socio</Link> */}
            <Link to="/workflowtemplates-es">Modelos de workflow</Link>
            <Link
              target="_blank"
              to="https://optimetriks.invisionapp.com/dsm/design/optimetriks-brand"
            >
              Brand Assets
            </Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Nosotros</H3>
            <Link to="/about-es">Sobre Nosotros</Link>
            <Link to="/blog-es">Blog</Link>
            <Link to="/contact-es">Contáctanos</Link>
            <Link to="https://optimetriks.factorialhr.com/">Carreras</Link>
            <Link to="/privacypolicy-es">Política de privacidad</Link>
            <Link to="/terms-es">Términos y condiciones</Link>
          </LinkGroup>

          <LinkGroup>
            <H3>Para Clientes</H3>
            <Link to="https://web.fieldproapp.com/">Login</Link>
            <Link to="https://ayuda.fieldproapp.com/index.html">
              Centro de ayuda
            </Link>
          </LinkGroup>
        </FooterGroup>

        <BottomFooterGroup>
          <Copyright>
            <P>
              © {new Date().getFullYear()} | Optimetriks | Todos los derechos
              reservados
            </P>
          </Copyright>

          <SocialIconsGroup>
            {" "}
            <a
              href="https://play.google.com/store/apps/details?id=com.optimetriks.smala&hl=en&gl=US"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/google-play.svg" alt="Playstore Logo" />
            </a>
            <a
              href="https://www.linkedin.com/company/optimetriks"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/linkedin.svg" alt="LinkedIn Logo" />
            </a>
            <a
              href="https://www.facebook.com/fieldproapp"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/facebook.svg" alt="Facebook Logo" />
            </a>
            <a
              href="https://www.youtube.com/channel/UC3lznqy3g-OCcUvci_FkVzg"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/Play button.svg" alt="Youtube Logo" />
            </a>
            <a
              href="https://twitter.com/optimetriks"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/twitter.svg" alt="Twitter Logo" />
            </a>
          </SocialIconsGroup>
        </BottomFooterGroup>
      </FooterContainer>
    </Layout>
  )
}

export default FooterES
