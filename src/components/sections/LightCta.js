import React from "react"
import styled from "styled-components"
import { DownloadButtonArrow, ScheduleButton } from "../Buttons/CtaButton.js"
import { Caption } from "../styles/TextStyles.js"
import { Link } from "gatsby"
function LightCta(props) {
  return (
    <>
      <Link to="/book-a-demo">
        <ScheduleButton id="bookdemo">Book a Demo</ScheduleButton>
      </Link>
      <TrialGroup>
        <a
          href="https://web.v3.fieldproapp.com/welcome"
          target="_blank"
          rel="noreferrer"
        >
          <DownloadButtonArrow id="start_trial">
            Start Free Trial
          </DownloadButtonArrow>
        </a>
        <DisclaimerTextDark> *No credit card required</DisclaimerTextDark>
      </TrialGroup>
    </>
  )
}
export default LightCta

const TrialGroup = styled.div``
const DisclaimerTextDark = styled(Caption)`
  color: #6c6c6c;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
