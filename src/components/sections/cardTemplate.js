import React from "react"
import styled from "styled-components"
import { ButtonChip } from "../Buttons/CtaButton.js"
import { H4, P } from "../styles/TextStyles.js"
function CardTemplate(props) {
  const { iconPic, title, description, tag, btntag } = props
  return (
    <CardTemplateWrapper>
      <CardTemplateIcon>{iconPic}</CardTemplateIcon>
      <CardTemplateTitle>{title}</CardTemplateTitle>
      <CardTemplateDescription>{description}</CardTemplateDescription>
      <CardDividerLine></CardDividerLine>
      <CardChipGroup>
        <ButtonChip>{tag}</ButtonChip>
        <ButtonChip>{btntag}</ButtonChip>
      </CardChipGroup>
    </CardTemplateWrapper>
  )
}
export default CardTemplate
const CardTemplateWrapper = styled.div`
  width: 400px;
  border: 1px solid #979797;
  border-radius: 5px;
  padding: 40px;
  :hover {
    border: 2px solid #2c2c2c;
  }
  @media (max-width: 830px) {
    padding: 20px;
    width: 320px;
  }
  @media (max-width: 512px) {
    padding: 20px;
    width: 320px;
  }
`
const CardTemplateIcon = styled.div`
  height: 54px;
  width: 54px;
`
const CardTemplateTitle = styled(H4)`
  margin: 20px 0 10px 0;
`
const CardTemplateDescription = styled(P)``
const CardDividerLine = styled.div`
  width: 320px;
  height: 1px;
  background: #d8d8d8;
  padding: 0;
  margin: 40px 0 20px 0;
  @media (max-width: 830px) {
    width: 280px;
  }
  @media (max-width: 512px) {
    width: 280px;
  }
`
const CardChipGroup = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
`
