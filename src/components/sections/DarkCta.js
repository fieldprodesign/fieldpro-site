import React from "react"
import styled from "styled-components"
import {
  DownloadButtonDarkArrow,
  ScheduleButtonDark,
} from "../Buttons/CtaButton.js"
import { Caption } from "../styles/TextStyles.js"
import { Link } from "gatsby"
function DarkCta(props) {
  return (
    <>
      <Link to="/book-a-demo">
        <ScheduleButtonDark id="bookdemo">Book a Demo</ScheduleButtonDark>
      </Link>
      <TrialGroup>
        <a
          href="https://web.v3.fieldproapp.com/welcome"
          target="_blank"
          rel="noreferrer"
        >
          <DownloadButtonDarkArrow id="start_trial">
            Start Free Trial
          </DownloadButtonDarkArrow>
        </a>

        <DisclaimerText> *No credit card required</DisclaimerText>
      </TrialGroup>
    </>
  )
}
export default DarkCta

const TrialGroup = styled.div``
const DisclaimerText = styled(Caption)`
  color: #fefefe;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
