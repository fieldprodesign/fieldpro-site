import React, { useEffect, useRef, useState } from "react"
import styled from "styled-components"
import { Link } from "gatsby"
import { StaticQuery, graphql } from "gatsby"

import { HeaderGroup } from "../styles/TextStyles.js"

function Header(props) {
  /* */
  const [productIsOpen, setProductIsOpen] = useState(false)
  const [rsrcsIsOpen, setRsrcsIsOpen] = useState(false)
  const [industriesIsOpen, setIndustriesIsOpen] = useState(false)
  const [langdropdownIsOpen, setlangdropdownIsOpen] = useState(false)

  const [menuOpen, toggleMenuOpen] = useState(false)

  const [boxShadow, setBoxShadow] = useState(false)
  const navRef = useRef()
  navRef.current = boxShadow
  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > 50
      if (navRef.current !== show) {
        setBoxShadow(show)
      }
    }
    document.addEventListener("scroll", handleScroll)

    return () => {
      document.removeEventListener("scroll", handleScroll)
    }
  }, [])

  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulIndustryPage(filter: { language: { eq: "EN" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
              }
            }
          }
          allContentfulProductPage(filter: { language: { eq: "EN" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
              }
            }
          }
          allContentfulMenu(filter: { language: { eq: "EN Menu" } }) {
            edges {
              node {
                field1 {
                  json
                }
                field2 {
                  json
                }
                field3 {
                  json
                }
                field4 {
                  json
                }
                field5 {
                  json
                }
                field6 {
                  json
                }
                field7 {
                  json
                }
                field8 {
                  json
                }

                subfield52 {
                  json
                }
                subfield53 {
                  json
                }
                subfield54 {
                  json
                }
                subfield55 {
                  json
                }
                subfield61 {
                  json
                }
                subfield62 {
                  json
                }
                subfield63 {
                  json
                }
                subfield64 {
                  json
                }
              }
            }
          }
        }
      `}
      render={data => (
        <HeaderGroup>
          <MenuContainer boxShadow={boxShadow}>
            <MenuGroup>
              <MenuLogo>
                <Link
                  target="_parent"
                  activeClassName="active"
                  to="https://fieldproapp.com/"
                >
                  <img
                    src={require("../../../static/images/fieldpro-logo.png")}
                    alt="logo"
                  />
                </Link>
              </MenuLogo>

              <MenuLinks menuOpen={menuOpen}>
                {/* <Link
                  target="_parent"
                  activeClassName="active"
                  to={
                    data.allContentfulMenu.edges[0].node.field1.json.content[0]
                      .content[1].data.uri
                  }
                >
                  <div class="darkGrey">
                    {
                      data.allContentfulMenu.edges[0].node.field1.json
                        .content[0].content[1].content[0].value
                    }
                  </div>
                </Link> */}
                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setProductIsOpen(!productIsOpen)
                      setRsrcsIsOpen(false)

                      setIndustriesIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                      {
                        data.allContentfulMenu.edges[0].node.field2.json
                          .content[0].content[0].value
                      }
                    </DropDownLabel>
                    <ProductMoreIcon
                      productIsOpen={productIsOpen}
                      src={require("../../../static/images/expand_more.png")}
                    />
                    <ProductLessIcon
                      productIsOpen={productIsOpen}
                      src={require("../../../static/images/expand_less.png")}
                    />
                  </DropDownButton>
                  <ProductDropDown productIsOpen={productIsOpen}>
                    <DropDownGroup>
                      {data.allContentfulProductPage.edges.map(category => {
                        return (
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={`/${category.node.slug}`}
                            class="darkGrey"
                          >
                            {category.node.pageTitleAsSeenOnMenu}
                          </Link>
                        )
                      })}
                    </DropDownGroup>
                  </ProductDropDown>
                </DropDownContainer>

                <Link
                  target="_parent"
                  activeClassName="active"
                  to={
                    data.allContentfulMenu.edges[0].node.field3.json.content[0]
                      .content[1].data.uri
                  }
                >
                  <div class="darkGrey">
                    {
                      data.allContentfulMenu.edges[0].node.field3.json
                        .content[0].content[1].content[0].value
                    }
                  </div>
                </Link>
                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setIndustriesIsOpen(!industriesIsOpen)
                      setRsrcsIsOpen(false)
                      setProductIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                      {
                        data.allContentfulMenu.edges[0].node.field4.json
                          .content[0].content[0].value
                      }
                    </DropDownLabel>
                    <IndustriesMoreIcon
                      industriesIsOpen={industriesIsOpen}
                      src={require("../../../static/images/expand_more.png")}
                    />
                    <IndustriesLessIcon
                      industriesIsOpen={industriesIsOpen}
                      src={require("../../../static/images/expand_less.png")}
                    />
                  </DropDownButton>
                  <IndustriesDropDown industriesIsOpen={industriesIsOpen}>
                    <DropDownGroup>
                      {data.allContentfulIndustryPage.edges.map(category => {
                        return (
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={`/${category.node.slug}`}
                            class="darkGrey"
                          >
                            {category.node.pageTitleAsSeenOnMenu}
                          </Link>
                        )
                      })}
                    </DropDownGroup>
                  </IndustriesDropDown>
                </DropDownContainer>

                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setRsrcsIsOpen(!rsrcsIsOpen)
                      setProductIsOpen(false)

                      setIndustriesIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                      {
                        data.allContentfulMenu.edges[0].node.field5.json
                          .content[0].content[0].value
                      }
                    </DropDownLabel>
                    <RsrcsMoreIcon
                      rsrcsIsOpen={rsrcsIsOpen}
                      src={require("../../../static/images/expand_more.png")}
                    />
                    <RsrcsLessIcon
                      rsrcsIsOpen={rsrcsIsOpen}
                      src={require("../../../static/images/expand_less.png")}
                    />
                  </DropDownButton>
                  <RsrcsDropDown rsrcsIsOpen={rsrcsIsOpen}>
                    <DropDownGroup>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to="/add-ons"
                      >
                        <div class="darkGrey">Add ons</div>
                      </Link>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to={
                          data.allContentfulMenu.edges[0].node.field7.json
                            .content[0].content[1].data.uri
                        }
                      >
                        <div class="darkGrey">
                          {" "}
                          {
                            data.allContentfulMenu.edges[0].node.field7.json
                              .content[0].content[1].content[0].value
                          }
                        </div>
                      </Link>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to={
                          data.allContentfulMenu.edges[0].node.subfield55.json
                            .content[0].content[1].data.uri
                        }
                        class="darkGrey"
                      >
                        {
                          data.allContentfulMenu.edges[0].node.subfield55.json
                            .content[0].content[1].content[0].value
                        }
                      </Link>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to={
                          data.allContentfulMenu.edges[0].node.subfield62.json
                            .content[0].content[1].data.uri
                        }
                        class="darkGrey"
                      >
                        {
                          data.allContentfulMenu.edges[0].node.subfield62.json
                            .content[0].content[1].content[0].value
                        }
                      </Link>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to={
                          data.allContentfulMenu.edges[0].node.subfield52.json
                            .content[0].content[1].data.uri
                        }
                        class="darkGrey"
                      >
                        {
                          data.allContentfulMenu.edges[0].node.subfield52.json
                            .content[0].content[1].content[0].value
                        }
                      </Link>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to={
                          data.allContentfulMenu.edges[0].node.subfield53.json
                            .content[0].content[1].data.uri
                        }
                        class="darkGrey"
                      >
                        {
                          data.allContentfulMenu.edges[0].node.subfield53.json
                            .content[0].content[1].content[0].value
                        }
                      </Link>
                      <Link
                        target="_blank"
                        activeClassName="active"
                        to="https://optimetriks.invisionapp.com/dsm/design/optimetriks-brand"
                      >
                        <div class="darkGrey">Brand Assets</div>
                      </Link>
                    </DropDownGroup>
                  </RsrcsDropDown>
                </DropDownContainer>

                <YellowLink>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://web.fieldproapp.com/"
                    id="login"
                  >
                    <div>Login</div>
                  </Link>
                </YellowLink>

                <MenuButton>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="/book-a-demo"
                  >
                    <button>Book a Demo</button>
                  </Link>
                </MenuButton>
                <MenuLangBtns>
                  <DropDownLangContainer
                    onMouseEnter={() => setlangdropdownIsOpen(true)}
                    onMouseLeave={() => setlangdropdownIsOpen(false)}
                  >
                    <DropDownLangButton>
                      <img
                        style={{ padding: "6px 0px 0 0px" }}
                        src="/images/language_world_icon_grey.svg"
                        alt=""
                      />
                      <DropDownLangLabel> English</DropDownLangLabel>
                    </DropDownLangButton>
                    {langdropdownIsOpen && (
                      <LangDropDown>
                        <DropDownLangGroup>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.en}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src={require("../../../static/images/flag-en.png")}
                                alt=""
                              />
                              English
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.fr}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src={require("../../../static/images/flag-fr.png")}
                                alt=""
                              />
                              Français
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.es}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src={require("../../../static/images/flag-es.png")}
                                alt=""
                              />
                              Espanol
                            </LanguageOption>
                          </Link>
                        </DropDownLangGroup>
                      </LangDropDown>
                    )}
                  </DropDownLangContainer>
                </MenuLangBtns>
                <MenuLangBtnsMobile>
                  <DropDownLangContainer
                    onClick={() => {
                      setlangdropdownIsOpen(!langdropdownIsOpen)
                    }}
                  >
                    <DropDownLangButton>
                      <img
                        style={{ padding: "10px 0px 0 0px" }}
                        src="/images/language_world_icon_grey.svg"
                        alt=""
                      />
                      <DropDownLangLabel> English</DropDownLangLabel>
                    </DropDownLangButton>
                    {langdropdownIsOpen && (
                      <LangDropDown>
                        <DropDownLangGroup>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.en}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "12px 4px 0 0px" }}
                                src={require("../../../static/images/flag-en.png")}
                                alt=""
                              />
                              English
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.fr}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "12px 4px 0 0px" }}
                                src={require("../../../static/images/flag-fr.png")}
                                alt=""
                              />
                              Français
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.es}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              <img
                                style={{ padding: "12px 4px 0 0px" }}
                                src={require("../../../static/images/flag-es.png")}
                                alt=""
                              />
                              Espanol
                            </LanguageOption>
                          </Link>
                        </DropDownLangGroup>
                      </LangDropDown>
                    )}
                  </DropDownLangContainer>
                </MenuLangBtnsMobile>
              </MenuLinks>
            </MenuGroup>
            <MenuIconContainer>
              <MenuIcon
                menuOpen={menuOpen}
                onClick={() => toggleMenuOpen(!menuOpen)}
              >
                <div />
                <div />
                <div />
              </MenuIcon>
            </MenuIconContainer>
          </MenuContainer>
        </HeaderGroup>
      )}
    />
  )
}
export default Header

export const MenuContainer = styled.header`
  display: grid;
  position: fixed;
  top: 0;
  width: 100vw;
  margin: 0;
  padding: 0.25em 0 0 0;
  background: #fefefe;
  box-shadow: ${({ boxShadow }) =>
    boxShadow ? "0 0.5em 1em #2c2c2c07" : "none"};
  z-index: 100;
  @media (max-width: 830px) {
    margin: 0 0 0 -0.5em;
    padding: 0.25em 0 0 0;
  }
  @media (max-width: 512px) {
    margin: 0 0 0 -0.5em;
    padding: 0.25em 0 0 0;
  }
`
export const MenuGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  justify-items: space-between;
  justify-self: center;
  max-width: 80em;
  margin: 2em -0.5em -0.8em 0;
  padding: 0;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    max-width: 672px;
    margin: 2em 1em -1em 0em;
    padding: 0;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(2, auto);
    max-width: 32em;
    margin: 2em 1em -1em 1em;
    padding: 0;
  }
`
export const MenuLogo = styled.div`
  margin: -0.9em 0 0 0em;
  padding: 0;
  :hover {
    a {
      border-bottom: none;
    }
  }
  @media (max-width: 830px) {
    margin: -1em 1em 0 -20em;
  }
  @media (max-width: 512px) {
    margin: -1em 1em 0 -9em;
  }
`
export const MenuLangBtns = styled.div`
  margin: 0 0em 0 1em;
  padding: 0;

  :hover {
    a {
      border-bottom: none;
      padding-bottom: 0;
    }
  }
  :active {
    a {
      border-bottom: none;
      padding-bottom: 0;
    }
  }
  @media (max-width: 830px) {
    display: none;
  }
`
export const MenuLangBtnsMobile = styled.div`
  display: none;
  @media (max-width: 830px) {
    display: grid;
  }
`
export const DropDownLangContainer = styled.div`
  position: relative;
  display: grid;
  align-items: flex-start;
  margin: 0px 0 0 0;
  padding: 0 0 0 0;

  @media (max-width: 830px) {
    margin: 0;
  }
`
export const MenuIconContainer = styled.div`
  visibility: hidden;
  @media (max-width: 830px) {
    visibility: visible;
    position: absolute;
    top: 1.8em;
    right: 1.8em;
  }
`
export const MenuIcon = styled.div`
  cursor: pointer;
  background: transparent;
  border: none;
  display: grid;
  height: 2em;
  outline: thin-dotted;
  z-index: 200;
  div {
    width: 2em;
    height: 0.25em;
    background: #2c2c2c;
    border-radius: 0.5em;
    transform-origin: 0.05em;
    :first-child {
      transform: ${({ menuOpen }) =>
        menuOpen ? "rotate(45deg)" : "rotate(0)"};
    }
    :nth-child(2) {
      opacity: ${({ menuOpen }) => (menuOpen ? "0" : "1")};
    }
    :nth-child(3) {
      transform: ${({ menuOpen }) =>
        menuOpen ? "rotate(-45deg)" : "rotate(0)"};
    }
  }
`
export const MenuLinks = styled.nav`
  display: grid;
  position: relative;
  grid-template-columns: repeat(9, auto);
  grid-gap: 1em;
  align-items: flex-start;
  margin: -0.5em 0 0 20em;
  padding: 0;
  a {
    font-size: 1em;
    font-weight: 600;
    color: #6c6c6c;
  }
  @media (max-width: 830px) {
    display: none;
    grid-template-columns: repeat(1, auto);
    justify-items: flex-start;
    width: 672px;
    display: ${({ menuOpen }) => (menuOpen ? "grid" : "none")};
    margin: 5em 0 20em 5em;
    a {
      font-size: 1.5em;
      font-weight: 800;
    }
  }
  @media (max-width: 512px) {
    display: none;
    grid-template-columns: repeat(1, auto);
    justify-items: flex-start;
    width: 24em;
    display: ${({ menuOpen }) => (menuOpen ? "grid" : "none")};
    margin: 5em 0 20em 5em;
    a {
      font-size: 1.5em;
      font-weight: 800;
    }
  }
`
export const YellowLink = styled.div`
  position: relative;
  display: grid;
  grid-gap: 1em;
  grid-template-columns: repeat(2, auto);
  a {
    color: #febd55;
    opacity: 1;
  }
  @media (max-width: 830px) {
    grid-template-columns: repeat(1, auto);
    justify-items: flex-start;
    margin: 0.3em 0 0 0;
    padding: 0;
    a {
      font-size: 1.5em;
      font-weight: 800;
      line-height: 1.3;
    }
  }
`
export const MenuButton = styled.div`
  margin: -0.5em 0 0 -0.5em;
  :hover {
    a {
      border-bottom: none;
    }
  }
  @media (max-width: 830px) {
    margin: 0.8em 0 0 -1.6em;
  }
`
export const DropDownContainer = styled.div`
  position: relative;
  display: grid;
  align-items: flex-start;
  margin: 0;
  padding: 0;
  @media (max-width: 830px) {
    margin: 0;
  }
`
export const LanguageOption = styled.div`
  display: flex;

  padding: 0;
  margin: 0;
`
export const DropDownLangButton = styled.div`
  position: relative;
  display: grid;
  grid-gap: 4px;
  grid-template-columns: repeat(3, auto);
  cursor: pointer;
  margin: 0 1.1em 0em 0;
  padding: 0;
  :hover {
    border-bottom: none;
    padding-bottom: 0.1em;
  }
  @media (max-width: 830px) {
    display: flex;
    flex-wrap: nowrap;
    margin: 0 1.1em 0 0.1em;
  }
  :hover {
    border-bottom: none;
    padding-bottom: 0;
  }
`
export const DropDownButton = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: repeat(2, auto);
  cursor: pointer;
  margin: 0 1.1em 0.5em 0;
  padding: 0;
  :hover {
    border-bottom: 0.2em solid;
    padding-bottom: 0.1em;
  }
  @media (max-width: 830px) {
    display: flex;
    flex-wrap: nowrap;
    margin: 0 1.1em 0 0.1em;
  }
  :hover {
    border-bottom: none;
    padding-bottom: 0;
  }
`
export const DropDownLangLabel = styled.div`
  position: relative;
  font-size: 1em;
  font-weight: 600;
  line-height: 1.6;
  color: #6c6c6c;
  margin: 0em 000;
  padding: 0;
  :hover {
    font-weight: 800;
  }
  @media (max-width: 830px) {
    justify-self: flex-start;
    font-size: 1.5em;
    font-weight: 800;
  }
`
export const DropDownLabel = styled.div`
  position: relative;
  font-size: 1em;
  font-weight: 600;
  line-height: 1.6;
  color: #6c6c6c;
  margin: 0;
  padding: 0;
  :hover {
    font-weight: 800;
  }
  @media (max-width: 830px) {
    justify-self: flex-start;
    font-size: 1.5em;
    font-weight: 800;
  }
`

export const LangMoreIcon = styled.img`
  visibility: ${({ langdropdownIsOpen }) =>
    langdropdownIsOpen ? "hidden" : "visible"};
  position: absolute;
  justify-self: flex-end;
  align-self: center;
  height: 0.45em;
  margin: 0em -1.1em 0 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    height: 0.6em;
    margin: 0.15em 0 0 0.5em;
    justify-self: flex-start;
  }
`
export const LangLessIcon = styled.img`
  visibility: ${({ langdropdownIsOpen }) =>
    langdropdownIsOpen ? "visible" : "hidden"};
  position: absolute;
  justify-self: flex-end;
  align-self: center;
  height: 0.45em;
  margin: 0.15em -1.1em 0 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    height: 0.6em;
    margin: 0.15em 0 0 -1em;
    justify-self: flex-start;
  }
`
export const ProductMoreIcon = styled.img`
  visibility: ${({ productIsOpen }) => (productIsOpen ? "hidden" : "visible")};
  position: absolute;
  justify-self: flex-end;
  align-self: center;
  height: 0.45em;
  margin: 0.15em -1.1em 0 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    height: 0.6em;
    margin: 0.15em 0 0 0.5em;
    justify-self: flex-start;
  }
`
export const ProductLessIcon = styled.img`
  visibility: ${({ productIsOpen }) => (productIsOpen ? "visible" : "hidden")};
  position: absolute;
  justify-self: flex-end;
  align-self: center;
  height: 0.45em;
  margin: 0.15em -1.1em 0 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    height: 0.6em;
    margin: 0.15em 0 0 -1em;
    justify-self: flex-start;
  }
`
export const IndustriesMoreIcon = styled.img`
  visibility: ${({ industriesIsOpen }) =>
    industriesIsOpen ? "hidden" : "visible"};
  position: absolute;
  justify-self: flex-end;
  align-self: center;
  height: 0.45em;
  margin: 0.15em -1.1em 0 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    height: 0.6em;
    margin: 0.15em 0 0 0.5em;
    justify-self: flex-start;
  }
`
export const IndustriesLessIcon = styled.img`
  visibility: ${({ industriesIsOpen }) =>
    industriesIsOpen ? "visible" : "hidden"};
  position: absolute;
  justify-self: flex-end;
  align-self: center;
  height: 0.45em;
  margin: 0.15em -1.1em 0 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    height: 0.6em;
    margin: 0.15em 0 0 -1em;
    justify-self: flex-start;
  }
`
export const RsrcsMoreIcon = styled.img`
  visibility: ${({ rsrcsIsOpen }) => (rsrcsIsOpen ? "hidden" : "visible")};
  position: absolute;
  justify-self: flex-end;
  align-self: center;
  height: 0.45em;
  margin: 0.15em -1.1em 0 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    height: 0.6em;
    margin: 0.15em 0 0 0.5em;
    justify-self: flex-start;
  }
`
export const RsrcsLessIcon = styled.img`
  visibility: ${({ rsrcsIsOpen }) => (rsrcsIsOpen ? "visible" : "hidden")};
  position: absolute;
  justify-self: flex-end;
  align-self: center;
  height: 0.45em;
  margin: 0.15em -1.1em 0 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    height: 0.6em;
    margin: 0.15em 0 0 -1em;
    justify-self: flex-start;
  }
`
export const DropDownLangGroup = styled.div`
  display: grid;
  position: absolute;
  background: #fefefe;
  white-space: nowrap;
  grid-gap: 0.025em;
  border-radius: 0.25em;
  box-shadow: 0 0.4em 2em #2c2c2c10;
  width: auto;
  margin: 2.3em 0 1em -2em;
  padding: 1.2em 3em 1.3em 1.7em;
  z-index: 100;
  @media (max-width: 830px) {
    position: relative;
    justify-items: flex-start;
    background: none;
    box-shadow: none;
    margin: 0.6em 0 0 1em;
    padding: 0;
  }
`
export const DropDownGroup = styled.div`
  display: grid;
  position: absolute;
  background: #fefefe;
  white-space: nowrap;
  grid-gap: 0.3em;
  border-radius: 0.25em;
  box-shadow: 0 0.4em 2em #2c2c2c10;
  width: auto;
  margin: 2.3em 0 2em -1.5em;
  padding: 1.2em 1.7em 1.3em 1.7em;
  z-index: 100;
  @media (max-width: 830px) {
    position: relative;
    justify-items: flex-start;
    background: none;
    box-shadow: none;
    margin: 0.6em 0 0 1em;
    padding: 0;
  }
`
export const LangDropDown = styled.div`
  position: absolute;
  margin: 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    visibility: visible;

    justify-content: flex-start;
  }
`
export const ProductDropDown = styled.div`
  visibility: ${({ productIsOpen }) => (productIsOpen ? "visible" : "hidden")};
  position: absolute;
  margin: 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    visibility: visible;
    display: ${({ productIsOpen }) => (productIsOpen ? "grid" : "none")};
    justify-content: flex-start;
  }
`
export const IndustriesDropDown = styled.div`
  visibility: ${({ industriesIsOpen }) =>
    industriesIsOpen ? "visible" : "hidden"};
  position: absolute;
  margin: 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    visibility: visible;
    display: ${({ industriesIsOpen }) => (industriesIsOpen ? "grid" : "none")};
    justify-content: flex-start;
  }
`
export const RsrcsDropDown = styled.div`
  visibility: ${({ rsrcsIsOpen }) => (rsrcsIsOpen ? "visible" : "hidden")};
  position: absolute;
  margin: 0;
  padding: 0;
  @media (max-width: 830px) {
    position: relative;
    visibility: visible;
    display: ${({ rsrcsIsOpen }) => (rsrcsIsOpen ? "grid" : "none")};
    justify-content: flex-start;
  }
`
