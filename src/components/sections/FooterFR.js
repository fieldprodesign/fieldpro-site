import React from "react"
import { Link } from "gatsby"

import Layout from "../Layout"
import { H3, P } from "../styles/TextStyles.js"
import {
  FooterContainer,
  FooterGroup,
  FooterLogo,
  LinkGroup,
  Copyright,
  BottomFooterGroup,
  SocialIconsGroup,
  SocialIcon,
} from "../sections/Footer.js"

const FooterFR = () => {
  return (
    <Layout>
      <FooterContainer>
        <FooterGroup>
          <FooterLogo>
            <Link to="https://fieldproapp.com/fr">
              <img
                src={require("../../../static/images/optimetriks-logo.png")}
                alt=""
              />
            </Link>
          </FooterLogo>
          <LinkGroup>
            <H3>Solutions</H3>
            <Link to="/workflowbuilder-fr">Editeur de Workflow</Link>
            <Link to="/mobile-inspection-fr"> Inspection Terrain</Link>
            <Link to="/retail-execution-fr"> Exécution Merchandising</Link>
            <Link to="/sales-fr"> Digitalisation des ventes</Link>
            <Link to="/mobilecrm-fr"> CRM mobile</Link>
            <Link to="/field-team-management-fr">
              Gestion de la force de vente
            </Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Secteurs</H3>

            <Link to="/consumer-goods-fr"> Biens de consommation</Link>
            <Link to="/financial-services-fr"> Services financiers</Link>
            <Link to="/energy-access-fr"> Accès à l'énergie</Link>
            <Link to="/agriculture-fr"> Agriculture</Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Resources</H3>
            <Link to="/testimonials-fr">Témoignages</Link>
            <Link to="/blog-fr">Cas d’usage clients</Link>
            <Link to="/partners-fr">Devenir partenaire</Link>
            {/* <Link to="/find-a-partner-fr">Trouver un partenaire</Link> */}
            <Link to="/workflowtemplates-fr">Modèles de workflow</Link>
            <Link
              target="_blank"
              to="https://optimetriks.invisionapp.com/dsm/design/optimetriks-brand"
            >
              Brand Assets
            </Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Entreprise</H3>
            <Link to="/about-fr">A propos de nous</Link>
            <Link to="/blog-fr">Blog</Link>
            <Link to="/contact-fr">Nous-contacter</Link>
            <Link to="https://optimetriks.factorialhr.com/">Recrutement</Link>
            <Link to="/privacypolicy-fr">Politique de Confidentialité</Link>
            <Link to="/terms-fr">Conditions Générales d'Utilisation</Link>
          </LinkGroup>
          <LinkGroup>
            <H3>Clients</H3>
            <Link to="https://web.fieldproapp.com/">
              Se connecter à mon compte
            </Link>
            <Link to="https://aide.fieldproapp.com/index.html">
              Centre d'aide
            </Link>
          </LinkGroup>
        </FooterGroup>

        <BottomFooterGroup>
          <Copyright>
            <P>
              © {new Date().getFullYear()} | Optimetriks | Tous droits réservés{" "}
            </P>
          </Copyright>

          <SocialIconsGroup>
            {" "}
            <a
              href="https://play.google.com/store/apps/details?id=com.optimetriks.smala&hl=en&gl=US"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/google-play.svg" alt="Playstore Logo" />
            </a>
            <a
              href="https://www.linkedin.com/company/optimetriks"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/linkedin.svg" alt="LinkedIn Logo" />
            </a>
            <a
              href="https://www.facebook.com/fieldproapp"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/facebook.svg" alt="Facebook Logo" />
            </a>
            <a
              href="https://www.youtube.com/channel/UC3lznqy3g-OCcUvci_FkVzg"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/Play button.svg" alt="Youtube Logo" />
            </a>
            <a
              href="https://twitter.com/optimetriks"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SocialIcon src="/images/twitter.svg" alt="Twitter Logo" />
            </a>
          </SocialIconsGroup>
        </BottomFooterGroup>
      </FooterContainer>
    </Layout>
  )
}

export default FooterFR
