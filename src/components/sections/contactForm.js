import React, { Component } from "react"
import styled from "styled-components"
import { ScheduleButton } from "../Buttons/CtaButton"
import { Label, H2, H3 } from "../styles/TextStyles"
class ContactForm extends Component {
  constructor(props) {
    super(props)

    this.state = { value: "" }
    this.state = { submitting: false }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  //handle changes in input fileds
  handleChange(event) {
    this.setState({ value: event.target.value })
  }
  //submit to sheets
  handleSubmit(event) {
    event.preventDefault()
    const data = new FormData(event.target)

    fetch(
      "https://script.google.com/macros/s/AKfycbzOHI1aUWy5zJeLwkn_TeqdqMhLP7ZvdiW5OuMNAX8orlZKJesAVJr4K5pOreICYYU/exec",
      {
        method: "POST",
        body: data,
      }
    )
    //reset the form
    document.getElementById("contactform").reset()
    //Confirmation message
    this.setState({
      submitting: true,
    })
    // //make the message dissapear
    // setTimeout(() => {
    //   this.setState({
    //     submitting: false,
    //   })
    // }, 3000)
  }
  handleClick() {
    this.setState({
      submitting: false,
    })
  }

  render() {
    const sucessMessage = (
      <SuceessMessageWrapper>
        <Close onClick={this.handleClick}>&times;</Close>
        <SucessMsg>
          We appreciate you contacting us. We will get back in touch with you
          soon!
        </SucessMsg>
      </SuceessMessageWrapper>
    )
    return (
      <FormBody>
        <FormTitle>Get in Touch</FormTitle>
        <form id="contactform" method="post" onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label>
              Full Names<span style={{ color: "red" }}>*</span>
            </Label>
            <InputBox
              type="text"
              name="Name"
              id="Name"
              placeholder="Name"
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>
              Email Address<span style={{ color: "red" }}>*</span>
            </Label>
            <InputBox
              type="email"
              name="Email"
              id="email"
              placeholder="yourworkemail@yourcompany.com"
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>
              Phone Number<span style={{ color: "red" }}>*</span>
            </Label>
            <InputBox
              type="text"
              name="Tel"
              id=""
              placeholder="0700000000"
              required
            />
          </FormGroup>

          <FormGroup>
            <Label>
              Message<span style={{ color: "red" }}>*</span>
            </Label>
            <TextArea
              name="Message"
              type="text"
              id=""
              placeholder="Write your message here"
            />
          </FormGroup>

          <ScheduleButton
            id="contactsend"
            style={{
              background: "#febd55",
              color: "#fefefe",
              width: "auto",
            }}
            type="submit"
            value={this.state.value}
            onChange={this.handleChange}
          >
            Send
          </ScheduleButton>
        </form>
        {this.state.submitting ? sucessMessage : ""}
      </FormBody>
    )
  }
}

export default ContactForm

export const FormBody = styled.div`
  @media (max-width: 450px) {
    padding: 0 20px 0 20px;
  }
`
export const FormTitle = styled(H2)`
  margin: 0;
  padding: 20px 0 0 0;
  max-width: 600px;
`
export const FormGroup = styled.div`
  padding-top: 20px;
  display: grid;
  gap: 4px;
`
export const TextArea = styled.textarea`
  background-color: #fefefe;
  font-size: 1.2em;
  margin: 0 0 20px 0;
  padding: 1em 1em;
  height: 6em;
  width: 544px;
  border: 0.1em solid #c6c6c6;
  border-radius: 0.2em;
  @media (max-width: 450px) {
    max-width: 300px;
  }
`
export const InputBox = styled.input`
  width: 544px;
  margin: 8px 0 0 0;
  padding: 0 1em;
  height: 3em;
  border-style: solid;
  border-color: #c6c6c6;
  border-width: 0.1em;
  border-radius: 0.2em;
  font-size: 1.2em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  @media (max-width: 450px) {
    max-width: 300px;
  }
`

//Sucess message container
export const SuceessMessageWrapper = styled.div`
  max-width: 550px;
  margin: 1.5em 0 0em 0;
  background-color: #f5f5f5;
`
export const SucessMsg = styled(H3)`
  padding: 2em 1.5em 2em 1.3em;
  border-radius: 0.3em;
  @media (max-width: 450px) {
    max-width: 320px;
    padding: 2em 0.5em 2em 0.5em;
  }
`
export const Close = styled.div`
  padding: 0 0.5em 0 0;
  color: #aaa;
  font-size: 48px;
  float: right;
  font-weight: bold;
  :hover,
  focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
  }
  @media (max-width: 450px) {
    max-width: 320px;
  }
`
