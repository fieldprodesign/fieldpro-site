import React from "react"
import styled from "styled-components"
import {
  DownloadButtonDarkArrow,
  ScheduleButtonDark,
} from "../Buttons/CtaButton.js"
import { Caption } from "../styles/TextStyles.js"
import { Link } from "gatsby"
function DarkCtaES(props) {
  return (
    <>
      <Link to="/book-a-demo-es">
        <ScheduleButtonDark id="bookdemo">Reserva una demo</ScheduleButtonDark>
      </Link>
      <TrialGroup>
        <a
          href="https://web.v3.fieldproapp.com/welcome"
          target="_blank"
          rel="noreferrer"
        >
          <DownloadButtonDarkArrow id="start_trial">
            Empieza la prueba gratis
          </DownloadButtonDarkArrow>
        </a>

        <DisclaimerText>*No se requiere tarjeta de crédito</DisclaimerText>
      </TrialGroup>
    </>
  )
}
export default DarkCtaES

const TrialGroup = styled.div``
const DisclaimerText = styled(Caption)`
  color: #fefefe;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
