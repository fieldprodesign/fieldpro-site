import React from "react"
import styled from "styled-components"
import { DownloadButtonArrow, ScheduleButton } from "../Buttons/CtaButton.js"
import { Caption } from "../styles/TextStyles.js"
import { Link } from "gatsby"
function LightCtaES(props) {
  return (
    <>
      <Link to="/book-a-demo-es">
        <ScheduleButton id="bookdemo">Reserva una demo</ScheduleButton>
      </Link>
      <TrialGroup>
        <a
          href="https://web.v3.fieldproapp.com/welcome"
          target="_blank"
          rel="noreferrer"
        >
          <DownloadButtonArrow id="start_trial">
            Empieza la prueba gratis
          </DownloadButtonArrow>
        </a>
        <DisclaimerTextDark>
          {" "}
          *No se requiere tarjeta de crédito
        </DisclaimerTextDark>
      </TrialGroup>
    </>
  )
}
export default LightCtaES

const TrialGroup = styled.div``
const DisclaimerTextDark = styled(Caption)`
  color: #6c6c6c;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
