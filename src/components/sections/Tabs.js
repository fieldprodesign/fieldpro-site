import { useState } from "react"
import React from "react"
import styled from "styled-components"
import "../styles/buttons.css"
import { BodyMain, H2, P } from "../styles/TextStyles.js"

function TabsBar() {
  const [toggleState, setToggleState] = useState(1)

  const toggleTab = index => {
    setToggleState(index)
  }

  return (
    <Container>
      <BlocTabs>
        <button
          className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(1)}
        >
          FRANCE
        </button>
        <button
          className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(2)}
        >
          NIGERIA
        </button>
        <button
          className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(3)}
        >
          KENYA
        </button>
        <button
          className={toggleState === 4 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(4)}
        >
          SENEGAL
        </button>
        <button
          className={toggleState === 5 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(5)}
        >
          GHANA
        </button>
        <button
          className={toggleState === 6 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(6)}
        >
          MEXICO
        </button>
      </BlocTabs>
      <ContentTabs>
        <div
          className={toggleState === 1 ? "content  active-content" : "content"}
        >
          <OfficeWrapper>
            <OfficeDetails>
              <OfficeTitle>Paris Office</OfficeTitle>
              <OfficeBlock1>
                <OfficePhone>
                  <OfficePhoneTitle>WHATSAPP US</OfficePhoneTitle>
                  <OfficePhoneValue>
                    {" "}
                    <a
                      href="https://wa.me/254796142344"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      +254796142344
                    </a>
                  </OfficePhoneValue>
                </OfficePhone>
                <OfficeAdress>
                  <OfficeAdressTitle> ADDRESS</OfficeAdressTitle>
                  <OfficeAdressValue>
                    {" "}
                    124 Rue Réaumur, 75002 Paris, France
                  </OfficeAdressValue>
                </OfficeAdress>
              </OfficeBlock1>
              <OfficeEmail>
                {" "}
                <OfficeEmailTitle>EMAIL</OfficeEmailTitle>
                <OfficeEmailValue>sales@optimetriks.com</OfficeEmailValue>
              </OfficeEmail>
            </OfficeDetails>
            <OfficeMap>
              <iframe
                title="Paris Office Map"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.475398881798!2d2.3420960159032336!3d48.86821320796878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e3d0d66a73f%3A0x801548e89767fed!2s124%20Rue%20R%C3%A9aumur%2C%2075002%20Paris%2C%20France!5e0!3m2!1sen!2ske!4v1666786284157!5m2!1sen!2ske"
                width="670"
                height="600"
                style={{ border: "0" }}
                allowfullscreen=""
                loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"
              ></iframe>
            </OfficeMap>
          </OfficeWrapper>
        </div>
        <div
          className={toggleState === 2 ? "content  active-content" : "content"}
        >
          <OfficeWrapper>
            <OfficeDetails>
              <OfficeTitle>Lagos Office</OfficeTitle>
              <OfficeBlock1>
                <OfficePhone>
                  <OfficePhoneTitle>WHATSAPP US</OfficePhoneTitle>
                  <OfficePhoneValue>
                    {" "}
                    <a
                      href="https://wa.me/254796142344"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      +33 789 456 567
                    </a>
                  </OfficePhoneValue>
                </OfficePhone>
                <OfficeAdress>
                  <OfficeAdressTitle> ADDRESS</OfficeAdressTitle>
                  <OfficeAdressValue>
                    {" "}
                    Plot 5, Etal Avenue Lagos ng
                  </OfficeAdressValue>
                </OfficeAdress>
              </OfficeBlock1>
              <OfficeEmail>
                {" "}
                <OfficeEmailTitle>EMAIL</OfficeEmailTitle>
                <OfficeEmailValue>sales@optimetriks.com</OfficeEmailValue>
              </OfficeEmail>
            </OfficeDetails>
            <OfficeMap>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.4336190365907!2d3.3657867147710676!3d6.592904495232111!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b92431453f81d%3A0xbba1c33d9f6ec74d!2s5%20Etal%20Ave%2C%20Opebi%20101233%2C%20Lagos%2C%20Nigeria!5e0!3m2!1sen!2ske!4v1666791735749!5m2!1sen!2ske"
                width="670"
                title="Lagos Office Map"
                height="600"
                style={{ border: "0" }}
                allowfullscreen=""
                loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"
              ></iframe>
            </OfficeMap>
          </OfficeWrapper>
        </div>

        <div
          className={toggleState === 3 ? "content  active-content" : "content"}
        >
          <OfficeWrapper>
            <OfficeDetails>
              <OfficeTitle>Nairobi Office</OfficeTitle>
              <OfficeBlock1>
                <OfficePhone>
                  <OfficePhoneTitle>WHATSAPP US</OfficePhoneTitle>
                  <OfficePhoneValue>
                    {" "}
                    <a
                      href="https://wa.me/254796142344"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      +254796142344
                    </a>
                  </OfficePhoneValue>
                </OfficePhone>
                <OfficeAdress>
                  <OfficeAdressTitle> ADDRESS</OfficeAdressTitle>
                  <OfficeAdressValue>
                    {" "}
                    Marple Court, Galana Rd Kilimani
                  </OfficeAdressValue>
                </OfficeAdress>
              </OfficeBlock1>
              <OfficeEmail>
                {" "}
                <OfficeEmailTitle>EMAIL</OfficeEmailTitle>
                <OfficeEmailValue>sales@optimetriks.com</OfficeEmailValue>
              </OfficeEmail>
            </OfficeDetails>
            <OfficeMap>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8066960529736!2d36.78102191534037!3d-1.2902678359958961!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f11a14c0cafb3%3A0x45d82bb305d28443!2sMaple%20Court!5e0!3m2!1sen!2ske!4v1666790349663!5m2!1sen!2ske"
                title="Nairobi Office Map"
                width="670"
                height="600"
                style={{ border: "0" }}
                allowfullscreen=""
                loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"
              ></iframe>
            </OfficeMap>
          </OfficeWrapper>
        </div>
        <div
          className={toggleState === 4 ? "content  active-content" : "content"}
        >
          <OfficeWrapper>
            <OfficeDetails>
              <OfficeTitle>Dakar Office</OfficeTitle>
              <OfficeBlock1>
                <OfficePhone>
                  <OfficePhoneTitle>WHATSAPP US</OfficePhoneTitle>
                  <OfficePhoneValue>
                    {" "}
                    <a
                      href="https://wa.me/254796142344"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      +254796142344
                    </a>
                  </OfficePhoneValue>
                </OfficePhone>
                <OfficeAdress>
                  <OfficeAdressTitle> ADDRESS</OfficeAdressTitle>
                  <OfficeAdressValue>
                    {" "}
                    Cité Keur Gorgui, 131 Rond point sn
                  </OfficeAdressValue>
                </OfficeAdress>
              </OfficeBlock1>
              <OfficeEmail>
                {" "}
                <OfficeEmailTitle>EMAIL</OfficeEmailTitle>
                <OfficeEmailValue>sales@optimetriks.com</OfficeEmailValue>
              </OfficeEmail>
            </OfficeDetails>
            <OfficeMap>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3859.031276566415!2d-17.470957385158503!3d14.710823389732399!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xec172a5ade0e501%3A0xde71e951e5b2b66c!2sInternational%20Coworking%20Africa%20Outsourcing!5e0!3m2!1sen!2ske!4v1666791922404!5m2!1sen!2ske"
                width="670"
                title="Dakar Office Map"
                height="600"
                style={{ border: "0" }}
                allowfullscreen=""
                loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"
              ></iframe>
            </OfficeMap>
          </OfficeWrapper>
        </div>
        <div
          className={toggleState === 5 ? "content  active-content" : "content"}
        >
          <OfficeWrapper>
            <OfficeDetails>
              <OfficeTitle>Accra Office</OfficeTitle>
              <OfficeBlock1>
                <OfficePhone>
                  <OfficePhoneTitle>WHATSAPP US</OfficePhoneTitle>
                  <OfficePhoneValue>
                    {" "}
                    <a
                      href="https://wa.me/254796142344"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      +254796142344
                    </a>
                  </OfficePhoneValue>
                </OfficePhone>
                <OfficeAdress>
                  <OfficeAdressTitle> ADDRESS</OfficeAdressTitle>
                  <OfficeAdressValue>
                    {" "}
                    124 Rue Réaumur, 75002 Paris, France
                  </OfficeAdressValue>
                </OfficeAdress>
              </OfficeBlock1>
              <OfficeEmail>
                {" "}
                <OfficeEmailTitle>EMAIL</OfficeEmailTitle>
                <OfficeEmailValue>sales@optimetriks.com</OfficeEmailValue>
              </OfficeEmail>
            </OfficeDetails>
            <OfficeMap>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.475398881798!2d2.3420960159032336!3d48.86821320796878!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e3d0d66a73f%3A0x801548e89767fed!2s124%20Rue%20R%C3%A9aumur%2C%2075002%20Paris%2C%20France!5e0!3m2!1sen!2ske!4v1666786284157!5m2!1sen!2ske"
                width="670"
                height="600"
                title="Accra Office Map"
                style={{ border: "0" }}
                allowfullscreen=""
                loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"
              ></iframe>
            </OfficeMap>
          </OfficeWrapper>
        </div>
        <div
          className={toggleState === 6 ? "content  active-content" : "content"}
        >
          <OfficeWrapper>
            <OfficeDetails>
              <OfficeTitle>Mexico Office</OfficeTitle>
              <OfficeBlock1>
                <OfficePhone>
                  <OfficePhoneTitle>WHATSAPP US</OfficePhoneTitle>
                  <OfficePhoneValue>
                    {" "}
                    <a
                      href="https://wa.me/254796142344"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      +254796142344
                    </a>
                  </OfficePhoneValue>
                </OfficePhone>
                <OfficeAdress>
                  <OfficeAdressTitle> ADDRESS</OfficeAdressTitle>
                  <OfficeAdressValue>
                    {" "}
                    Protasio Tagle N°104 11850 Mexcio mx
                  </OfficeAdressValue>
                </OfficeAdress>
              </OfficeBlock1>
              <OfficeEmail>
                {" "}
                <OfficeEmailTitle>EMAIL</OfficeEmailTitle>
                <OfficeEmailValue>sales@optimetriks.com</OfficeEmailValue>
              </OfficeEmail>
            </OfficeDetails>
            <OfficeMap>
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.024231303001!2d-99.18659958509359!3d19.4113589868963!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1ff5e9e06af37%3A0x2eb175abd96dc90e!2sCalle%20Gob.Protasio%20Tagle%20104%2C%20San%20Miguel%20Chapultepec%20II%20Secc%2C%20Miguel%20Hidalgo%2C%2011850%20Ciudad%20de%20M%C3%A9xico%2C%20CDMX%2C%20Mexico!5e0!3m2!1sen!2ske!4v1666792120346!5m2!1sen!2ske"
                width="670"
                height="600"
                title="Mexico Office Map"
                style={{ border: "0" }}
                allowfullscreen=""
                loading="lazy"
                referrerpolicy="no-referrer-when-downgrade"
              ></iframe>
            </OfficeMap>
          </OfficeWrapper>
        </div>
      </ContentTabs>
    </Container>
  )
}

export default TabsBar
export const Container = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;

  margin: 100px auto 0;
  word-break: break-all;
`
export const BlocTabs = styled.div`
  display: flex;

  @media (max-width: 450px) {
    width: 360px;
    overflow: auto;
    white-space: nowrap;
  }
`
export const ContentTabs = styled.div`
  flex-grow: 1;
`
const OfficeWrapper = styled.div`
  height: 600px;
  background-color: #fefefe;
  filter: drop-shadow(0px 3.32171px 39.8605px rgba(0, 0, 0, 0.08));

  display: grid;
  grid-template-columns: 600px auto;
  grid-gap: 12px;
  @media (max-width: 830px) {
    display: grid;
    grid-template-columns: repeat(1, auto);
    height: auto;
    width: 670px;
    margin: 0 20px;
    padding: 0;
  }
  @media (max-width: 450px) {
    display: grid;
    grid-template-columns: repeat(1, auto);
    height: auto;
    width: 360px;
  }
`
const OfficeDetails = styled.div`
  padding: 40px 0 0 40px;
`
const OfficeTitle = styled(H2)``
const OfficeBlock1 = styled.div`
  display: grid;
  grid-template-columns: auto auto;
  grid-gap: 40px;
  margin: 32px 0 40px 0;

  @media (max-width: 830px) {
    display: grid;
    grid-template-columns: repeat(2, auto);
  }
  @media (max-width: 450px) {
    display: grid;
    grid-template-columns: repeat(1, auto);
    margin: 0 0 20px 0;
    grid-gap: 20px;
    height: auto;
  }
`
const OfficePhone = styled.div``
const OfficeAdress = styled.div``
const OfficePhoneTitle = styled(BodyMain)``
const OfficePhoneValue = styled(P)``
const OfficeAdressTitle = styled(BodyMain)``
const OfficeAdressValue = styled(P)`
  max-width: 220px;
  @media (max-width: 830px) {
    max-width: 200px;
  }
`
const OfficeEmailTitle = styled(BodyMain)``
const OfficeEmailValue = styled(P)``

const OfficeEmail = styled.div``
const OfficeMap = styled.div`
  @media (max-width: 450px) {
    iframe {
      width: 360px;
    }
  }
`
