import React, { Component } from "react"
import { CountryDropdown } from "react-country-region-selector"
import styled from "styled-components"
import { DownloadButton, DownloadButtonDarkArrow } from "../Buttons/CtaButton"

import { Label, H3, P, Button } from "../styles/TextStyles"
class BecomeaPartnerDark extends Component {
  constructor(props) {
    super(props)
    this.state = { country: "" }
    this.state = { value: "" }
    this.state = { showModal: false }
    this.state = { showSecondModal: false }
    this.handleClick = this.handleClick.bind(this)
    this.handleSecondModal = this.handleSecondModal.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  //First Modal onclick to open if closed and vice versa
  handleClick() {
    this.setState(prevState => ({
      showModal: !prevState.showModal,
    }))
  }

  //handle changes in input fileds
  handleChange(event) {
    this.setState({ value: event.target.value })
  }
  //submit to sheets
  handleSubmit(event) {
    const data = new FormData(event.target)

    fetch(
      "https://script.google.com/macros/s/AKfycbw1fs1J7oY9gAl6rabS1I50roVPTDV-0T0npO8nybL9OibGx_BNm0rhrkJ2SkwOGbWC/exec",
      {
        method: "POST",
        body: data,
      }
    )
    //rest the form
    document.getElementById("leadform").reset()
    //close the first modal
    this.setState(prevState => ({
      showModal: !prevState.showModal,
    }))
    //open the second modal
    this.setState(prevState => ({
      showSecondModal: !prevState.showSecondModal,
    }))
  }
  //Allow user to selct country in input fileds
  selectCountry(val) {
    this.setState({ country: val })
  }
  //Second pop-up modal
  handleSecondModal() {
    this.setState(prevState => ({
      showSecondModal: !prevState.showSecondModal,
    }))
  }
  render() {
    const { country } = this.state
    const modaltwo = (
      <>
        <SmallModal>
          <SmallModalContent>
            <ModalTitle>Thank you for your interest</ModalTitle>
            <Description>
              Someone from our sales team will be in touch shortly.Feel free to
              contact us at
              <span style={{ fontWeight: "bold" }}>
                {" "}
                sales@optimetriks.com{" "}
              </span>
              if you have any questions.
            </Description>
            <Button
              style={{ color: "#fefefe", margin: "0" }}
              onClick={this.handleSecondModal}
            >
              Close
            </Button>
          </SmallModalContent>
        </SmallModal>
      </>
    )
    const modal = (
      <>
        <Modal>
          <ModalContent>
            <ModalHeader>
              <Close onClick={this.handleClick}>&times;</Close>
              <ModalTitle>Become a Partner</ModalTitle>

              <Description>
                Fill the form below to sign up to our FieldPro partners program.
              </Description>
            </ModalHeader>
            <ModalBody>
              <form id="leadform" method="post" onSubmit={this.handleSubmit}>
                <NameBlock>
                  <FormGroup>
                    <Label>
                      First Name<span style={{ color: "red" }}>*</span>
                    </Label>
                    <InputSmall
                      type="text"
                      name="FirstName"
                      id="name"
                      required
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label>
                      Last Name<span style={{ color: "red" }}>*</span>
                    </Label>
                    <InputSmall
                      type="text"
                      name="LastName"
                      id="name"
                      required
                    />
                  </FormGroup>
                </NameBlock>
                <FormGroup>
                  <Label>
                    Company Name <span style={{ color: "red" }}>*</span>
                  </Label>
                  <InputBox name="Company" type="text" id="" required />
                </FormGroup>
                <FormGroup>
                  <Label>
                    Work Email<span style={{ color: "red" }}>*</span>
                  </Label>
                  <InputBox
                    type="email"
                    name="WorkEmail"
                    id="email"
                    placeholder="yourworkemail@yourcompany.com"
                    required
                  />
                </FormGroup>
                <PhoneBlock>
                  <FormGroup>
                    <Label>
                      Country<span style={{ color: "red" }}>*</span>
                    </Label>
                    <CountryDropdown
                      name="CountryCode"
                      value={country}
                      onChange={val => this.selectCountry(val)}
                      style={styles.countryStyle}
                      required
                    />
                  </FormGroup>
                  <FormGroup>
                    <Label>
                      Phone Number<span style={{ color: "red" }}>*</span>
                    </Label>
                    <InputSmall
                      type="text"
                      name="PhoneNumber"
                      id=""
                      placeholder="0700000000"
                      required
                    />
                  </FormGroup>
                </PhoneBlock>

                <FormGroup>
                  <Label>How did you hear about us?</Label>
                  <Item>
                    <RadioButton
                      type="radio"
                      name="Medium"
                      value="SocialMedia"
                      onChange={this.handleChange}
                    />
                    <RadioButtonLabel />
                    <div>Social Media (Linkedin, Facebook, Twitter).</div>
                  </Item>
                  <Item>
                    <RadioButton
                      type="radio"
                      name="Medium"
                      value="SearchEngine"
                      onChange={this.handleChange}
                    />
                    <RadioButtonLabel />
                    <div>Search engine (Google)</div>
                  </Item>
                  <Item>
                    <RadioButton
                      type="radio"
                      name="Medium"
                      value="Recommended"
                      onChange={this.handleChange}
                    />
                    <RadioButtonLabel />
                    <div>Recommended by a vendor or Fieldpro client</div>
                  </Item>
                  <Item>
                    <RadioButton
                      id="other"
                      type="radio"
                      name="Medium"
                      value="other"
                      onChange={this.handleChange}
                    />
                    <RadioButtonLabel />
                    <div>Other</div>

                    <OtherField
                      type="text"
                      name="MediumOther"
                      onChange={this.handleChange}
                    />
                  </Item>
                </FormGroup>

                <DownloadButton
                  id="partner_btn"
                  type="submit"
                  value={this.state.value}
                  onChange={this.handleChange}
                >
                  Become a Partner
                </DownloadButton>
              </form>
            </ModalBody>
            <ModalFooter>
              <Privacy>
                We value your privacy; Your information will only be used for
                our own internal processes and will not be shared with any
                external party
              </Privacy>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    )
    return (
      <Wrapper>
        <DownloadButtonDarkArrow id="becomepartner" onClick={this.handleClick}>
          Become a Partner
        </DownloadButtonDarkArrow>
        {this.state.showModal ? modal : ""}
        {this.state.showSecondModal ? modaltwo : ""}
      </Wrapper>
    )
  }
}

export default BecomeaPartnerDark
const Wrapper = styled.div``
const Modal = styled.div`
  position: fixed;
  z-index: 1;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgb(0, 0, 0);
  background-color: rgba(0, 0, 0, 0.4);
  margin: 0 auto;
  @media (max-width: 450px) {
    max-width: 400px;
  }
`

const styles = {
  countryStyle: {
    backgroundColor: "#fefefe",
    fontSize: "1.2em",
    margin: "0",
    padding: "0 1em",
    height: "3em",
    width: "262px",
    border: "0.1em solid #2c2c2c",
    borderRadius: "0.2em",
  },
}
const ModalHeader = styled.div``
const ModalTitle = styled(H3)`
  padding-top: 40px;
`
const Description = styled(P)`
  @media (max-width: 450px) {
    max-width: 350px;
  }
`
const ModalBody = styled.div``
const ModalContent = styled.div`
  width: 960px;
  height: 1300px;
  border-radius: 8px;
  box-shadow: 0 0 48px 0 rgba(0, 0, 0, 0.16);
  background-color: #fefefe;
  padding: 20px 80px 80px 80px;
  margin: 10% auto;
  align-items: center;
  @media (max-width: 450px) {
    height: 1500px;
    max-width: 360px;
    margin: 20% 0;
    padding: 10px 20px 20px 20px;
  }
`
const FormGroup = styled.div`
  padding: 12px 0 12px 0;
  display: grid;
  gap: 4px;
`
const InputBox = styled.input`
  width: 800px;
  margin: 0;
  padding: 0 1em;
  height: 3em;
  border-style: solid;
  border-color: #2c2c2c;
  border-width: 0.1em;
  border-radius: 0.2em;
  font-size: 1.2em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  @media (max-width: 450px) {
    max-width: 300px;
  }
`
const InputSmall = styled.input`
  margin: 0;
  padding: 0 1em;
  height: 3em;
  width: 380px;
  border-style: solid;
  border-color: #2c2c2c;
  border-width: 0.1em;
  border-radius: 0.2em;
  font-size: 1.2em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  @media (max-width: 450px) {
    width: 300px;
  }
`
const NameBlock = styled.div`
  display: grid;
  grid-template-columns: 380px auto;
  gap: 40px;
  @media (max-width: 450px) {
    max-width: 300px;
    grid-template-columns: auto;
    gap: 10px;
  }
`

const PhoneBlock = styled.div`
  display: grid;
  grid-template-columns: 380px auto;
  gap: 40px;
  @media (max-width: 450px) {
    max-width: 300px;
    grid-template-columns: auto;
    gap: 10px;
  }
`
const ModalFooter = styled.div``
const Privacy = styled.p`
  @media (max-width: 450px) {
    max-width: 350px;
  }
`
const Close = styled.div`
  color: #aaa;
  font-size: 64px;
  float: right;
  font-weight: bold;
  :hover,
  focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
  }
`
//second Modal
const SmallModal = styled.div`
  position: fixed;
  z-index: 1;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgb(0, 0, 0);
  background-color: rgba(0, 0, 0, 0.4);
  margin: 0 auto;
  @media (max-width: 450px) {
    max-width: 400px;
  }
`
const SmallModalContent = styled.div`
  width: 620px;
  height: 450px;
  border-radius: 8px;
  box-shadow: 0 0 48px 0 rgba(0, 0, 0, 0.16);
  background-color: #fefefe;
  padding-left: 32px;
  padding-right: 24px;
  position: absolute;
  top: 400px;
  left: 650px;

  @media (max-width: 450px) {
    height: 600px;
    max-width: 360px;
    margin: 20% 3%;
  }
`
const RadioButtonLabel = styled.label`
  position: absolute;
  top: 25%;
  left: 4px;
  width: 24px;
  height: 24px;
  border-radius: 50%;
  background: white;
  border: 1px solid #ccc;
`
export const Item = styled.div`
  display: flex;
  align-items: center;
  height: 48px;
  position: relative;
  box-sizing: border-box;
  margin-bottom: 12px;
  @media (max-width: 450px) {
    max-width: 320px;
  }
`
const RadioButton = styled.input`
  opacity: 0;
  z-index: 1;
  cursor: pointer;
  width: 25px;
  height: 25px;
  margin-right: 10px;
  &:hover ~ ${RadioButtonLabel} {
    background: #ffeed4;
    &::after {
      display: block;
      color: white;
      width: 12px;
      height: 12px;
      margin: 4px;
    }
  }
  &:checked + ${Item} {
    background: #fefefe;
    border: 2px solid #88a6ee;
  }
  &:checked + ${RadioButtonLabel} {
    background: #124e5d;
    border: 4px solid #88a6ae;
    &::after {
      display: block;
      color: white;
      width: 12px;
      height: 12px;
      margin: 4px;
    }
  }
`
export const OtherField = styled.input`
  opacity: 0;
  max-height: 0;
  overflow: hidden;
  transform: scale(0.8);
  transition: 0.5s;
  input[type="radio"]:checked ~ &,
  input[type="checkbox"]:checked ~ & {
    opacity: 1;
    max-height: 100px;
    overflow: visible;
    padding: 10px 20px;
    transform: scale(1);
  }
  margin: 0 20px 0 30px;
  padding: 0 1em;
  height: 3em;
  width: 385px;
  border: solid 1px #979797;
  border-radius: 0.2em;
  font-size: 1.2em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  @media (max-width: 450px) {
    margin: 0 10px 0 10px;
    width: 200px;
  }
`
