import React, { Component } from "react"
import { InlineWidget } from "react-calendly"
import { ScheduleButtonDark } from "../Buttons/CtaButton"
import styled from "styled-components"

class LeadFormDark extends Component {
  constructor(props) {
    super(props)
    this.state = { showModal: false }
    this.handleClick = this.handleClick.bind(this)
  }
  //First Modal onclick to open if closed and vice versa
  handleClick() {
    this.setState(prevState => ({
      showModal: !prevState.showModal,
    }))
  }
  render() {
    const calendlyWidget = (
      <Modal>
        <ModalContent>
          <Close onClick={this.handleClick}>&times;</Close>
          <InlineWidget
            styles={{
              height: "1000px",
              width: "auto",
              zIndex: "1",
              overflow: "hidden",
            }}
            url="https://calendly.com/jkaruti/fieldpro"
          />
        </ModalContent>
      </Modal>
    )
    return (
      <Wrapper>
        <ScheduleButtonDark onClick={this.handleClick}>
          Schedule a Demo
        </ScheduleButtonDark>
        {this.state.showModal ? calendlyWidget : ""}
      </Wrapper>
    )
  }
}
export default LeadFormDark
export const Wrapper = styled.div``
export const Modal = styled.div`
  position: fixed;
  z-index: 1;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgb(0, 0, 0);
  background-color: rgba(0, 0, 0, 0.4);
  margin: 0 auto;
`
export const ModalContent = styled.div`
  padding: 10px 80px 0 80px;
  margin: 10% auto;

  @media (max-width: 450px) {
    padding: 60px 10px 0 20px;
    margin: 20% 3%;
  }
`
export const Close = styled.div`
  color: #fefefe;
  font-size: 84px;
  float: right;
  font-weight: bold;
  margin: -40px 0 40px 0;
  :hover,
  focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
  }
  @media (max-width: 450px) {
    font-size: 48px;
    margin: -80px 0 0 0;
  }
`
