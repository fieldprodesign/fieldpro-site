import React from "react"
import CookieConsent from "react-cookie-consent"
import { useLocation } from "@reach/router" // this helps tracking the location
import { initializeAndTrack } from "gatsby-plugin-gdpr-cookies"
import { P } from "../styles/TextStyles.js"
import { Link } from "gatsby"
import styled from "styled-components"
function CookiesBar() {
  const location = useLocation()
  return (
    <CookieConsent
      enableDeclineButton
      flipButtons
      location="bottom"
      buttonText=" I Accept"
      declineButtonText=" I Decline"
      declineButtonStyle={{
        background: "none",
        borderColor: "#124e5d",
        borderWidth: "0.15em",
        borderRadius: "0.2em",
        borderStyle: "solid",
        color: "#124e5d",
        padding: "0.5em 1em",
        margin: "30px 20px 30px 0",
      }}
      cookieName="gatsby-gdpr-google-analytics"
      buttonStyle={{
        background: "#124e5d",
        borderRadius: "0.2em",
        color: "#fefefe",
        padding: "0.5em 1em",
        fontWeight: "bold",
        margin: "30px 30px 30px 24px",
      }}
      style={{
        background: "#C3D2D6",
        color: "#2c2c2c",
        boxShadow: "0 0.5em 2em #00000018",
        display: "flex",
        flexWrap: "wrap",

        justifyContent: "flex-start",
        left: "0",
        position: "fixed",
        width: "100%",
        alignItems: "baseline",
      }}
      contentStyle={{
        display: "grid",
        margin: "10px",
        maxWidth: "1280px",
      }}
      onAccept={() => {
        initializeAndTrack(location)
      }}
    >
      <PolicyContent>
        <P>
          {" "}
          We use cookies to personalise your experience. Read more about our
          <Link to="/cookiepolicy">
            <span style={{ color: "#124e5d", fontWeight: "600" }}>
              {" "}
              cookie policy.
            </span>
          </Link>
        </P>
      </PolicyContent>
    </CookieConsent>
  )
}
export default CookiesBar
const PolicyContent = styled.div`
  margin: 0 0 0 320px;
  @media (max-width: 450px) {
    max-width: 320px;
    margin: 0 10px -40px 20px;
  }
`
