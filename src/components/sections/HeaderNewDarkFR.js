import React, { useEffect, useRef, useState } from "react"

import { Link } from "gatsby"
import { StaticQuery, graphql } from "gatsby"

import { HeaderGroup } from "../styles/TextStyles.js"
import {
  HeaderContainer,
  HeaderContentWrapper,
  LogoContainer,
  DropDownContainer,
  DropDownButton,
  MenuItemsContainer,
  WhyMoreIcon,
  WhyLessIcon,
  WhyDropDown,
  DropDownGroup,
  WhyFieldProMenuGroup,
  ForyourIndustryContainer,
  MenuSectionTitle,
  HorizontalSeparator,
  ForyourWorkflowContainer,
  FeatureMoreIcon,
  FeatureLessIcon,
  FeatureDropDown,
  MenuRow,
  MenuCard,
  IconPicture,
  MenuText,
  TitleText,
  DescText,
  MenuItem,
  ResourcesMoreIcon,
  ResourcesLessIcon,
  ResourcesDropDown,
  LoginContainer,
  MenuLangBtns,
  DropDownLangContainer,
  DropDownLangButton,
  LangDropDown,
  DropDownLangGroup,
  LanguageOption,
  SeparatorContainer,
  Separator,
  DropDownLabel,
  ClientsMoreIcon,
  ClientsLessIcon,
  ClientsDropDown,
  CTAContainer,
  MenuCTAbutton,
  MenuIconContainer,
  MenuIcon,
} from "../sections/HeaderNewDark.js"

function NewHeaderDarkFR(props) {
  const [WhyIsOpen, setWhyIsOpen] = useState(false)
  const [FeatureIsOpen, setFeatureIsOpen] = useState(false)
  const [ClientsIsOpen, setClientsIsOpen] = useState(false)
  const [ResourcesIsOpen, setResourcesIsOpen] = useState(false)
  const [langdropdownIsOpen, setlangdropdownIsOpen] = useState(false)
  const [menuOpen, toggleMenuOpen] = useState(false)

  const [boxShadow, setBoxShadow] = useState(false)
  const navRef = useRef()
  navRef.current = boxShadow
  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > 50
      if (navRef.current !== show) {
        setBoxShadow(show)
      }
    }
    document.addEventListener("scroll", handleScroll)

    return () => {
      document.removeEventListener("scroll", handleScroll)
    }
  }, [])
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulFeaturesPage(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
                pageShortDescriptionAsSeenOnMenu
                pageIconAsSeenOnMenu {
                  file {
                    url
                  }
                }
              }
            }
          }
          allContentfulIndustryPage(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
              }
            }
          }
          allContentfulProductPage(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
              }
            }
          }
          allContentfulNewMenu(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                menuItemFive {
                  json
                }
                menuItemFour {
                  json
                }
                menuFourSubitem1 {
                  json
                }
                menuFourSubitem2 {
                  json
                }
                menuFourSubitem3 {
                  json
                }
                menuFourSubitem4 {
                  json
                }
                menuFourSubitem5 {
                  json
                }
                menuFourSubitem6 {
                  json
                }
                menuItemOne {
                  json
                }
                menuItemSix {
                  json
                }
                menuItemThree {
                  json
                }
                menuSixSubitems {
                  json
                }
                menuItemTwo {
                  json
                }
              }
            }
          }
        }
      `}
      render={data => (
        <HeaderGroup>
          <HeaderContainer boxShadow={boxShadow}>
            <HeaderContentWrapper>
              <LogoContainer>
                <Link
                  target="_parent"
                  activeClassName="active"
                  to="https://fieldproapp.com/"
                >
                  <img
                    src={require("/static/images/Logo - FieldPro Mark + Type Inverse.png")}
                    alt="logo"
                  />
                </Link>
              </LogoContainer>
              <MenuItemsContainer menuOpen={menuOpen}>
                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setWhyIsOpen(!WhyIsOpen)
                      setFeatureIsOpen(false)
                      setClientsIsOpen(false)
                      setResourcesIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                      {
                        data.allContentfulNewMenu.edges[0].node.menuItemOne.json
                          .content[0].content[0].value
                      }
                    </DropDownLabel>
                    <WhyMoreIcon
                      WhyIsOpen={WhyIsOpen}
                      src={require("/static/images/expand_more_white.png")}
                    />
                    <WhyLessIcon
                      WhyIsOpen={WhyIsOpen}
                      src={require("/static/images/expand_less_white.png")}
                    />
                  </DropDownButton>
                  <WhyDropDown WhyIsOpen={WhyIsOpen}>
                    <DropDownGroup>
                      <WhyFieldProMenuGroup>
                        <ForyourIndustryContainer>
                          <MenuSectionTitle>
                            {" "}
                            FOR YOUR INDUSTRY
                          </MenuSectionTitle>

                          <HorizontalSeparator></HorizontalSeparator>
                          {data.allContentfulIndustryPage.edges.map(
                            category => {
                              return (
                                <Link
                                  target="_parent"
                                  activeClassName="active"
                                  to={`/${category.node.slug}`}
                                  class="onGreenBackground"
                                >
                                  {category.node.pageTitleAsSeenOnMenu}
                                </Link>
                              )
                            }
                          )}
                        </ForyourIndustryContainer>
                        <ForyourWorkflowContainer>
                          <MenuSectionTitle> FOR YOUR USECASE</MenuSectionTitle>

                          <HorizontalSeparator></HorizontalSeparator>
                          {data.allContentfulProductPage.edges.map(category => {
                            return (
                              <Link
                                target="_parent"
                                activeClassName="active"
                                to={`/${category.node.slug}`}
                                class="onGreenBackground"
                              >
                                {category.node.pageTitleAsSeenOnMenu}
                              </Link>
                            )
                          })}
                        </ForyourWorkflowContainer>
                      </WhyFieldProMenuGroup>
                    </DropDownGroup>
                  </WhyDropDown>
                </DropDownContainer>
                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setFeatureIsOpen(!FeatureIsOpen)

                      setClientsIsOpen(false)
                      setResourcesIsOpen(false)
                      setWhyIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                      {
                        data.allContentfulNewMenu.edges[0].node.menuItemTwo.json
                          .content[0].content[0].value
                      }
                    </DropDownLabel>
                    <FeatureMoreIcon
                      FeatureIsOpen={FeatureIsOpen}
                      src={require("/static/images/expand_more_white.png")}
                    />
                    <FeatureLessIcon
                      FeatureIsOpen={FeatureIsOpen}
                      src={require("/static/images/expand_less_white.png")}
                    />
                  </DropDownButton>
                  <FeatureDropDown FeatureIsOpen={FeatureIsOpen}>
                    <DropDownGroup>
                      <MenuRow>
                        {data.allContentfulFeaturesPage.edges.map(category => {
                          return (
                            <Link
                              target="_parent"
                              to={`/${category.node.slug}`}
                            >
                              <MenuCard>
                                <IconPicture>
                                  <img
                                    src={
                                      category.node.pageIconAsSeenOnMenu.file
                                        .url
                                    }
                                    alt="FieldPro"
                                  />
                                </IconPicture>
                                <MenuText>
                                  <TitleText>
                                    {" "}
                                    {category.node.pageTitleAsSeenOnMenu}
                                  </TitleText>
                                  <DescText>
                                    {
                                      category.node
                                        .pageShortDescriptionAsSeenOnMenu
                                    }
                                  </DescText>
                                </MenuText>
                              </MenuCard>
                            </Link>
                          )
                        })}
                      </MenuRow>
                    </DropDownGroup>
                  </FeatureDropDown>
                </DropDownContainer>
                <MenuItem>
                  <Link
                    activeClassName="active"
                    to={
                      data.allContentfulNewMenu.edges[0].node.menuItemThree.json
                        .content[0].content[1].data.uri
                    }
                  >
                    <div class="onGreenBackground">
                      {
                        data.allContentfulNewMenu.edges[0].node.menuItemThree
                          .json.content[0].content[1].content[0].value
                      }
                    </div>
                  </Link>
                </MenuItem>

                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setResourcesIsOpen(!ResourcesIsOpen)

                      setFeatureIsOpen(false)
                      setClientsIsOpen(false)
                      setWhyIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                      {
                        data.allContentfulNewMenu.edges[0].node.menuItemFour
                          .json.content[0].content[0].value
                      }
                    </DropDownLabel>
                    <ResourcesMoreIcon
                      ResourcesIsOpen={ResourcesIsOpen}
                      src={require("/static/images/expand_more_white.png")}
                    />
                    <ResourcesLessIcon
                      ResourcesIsOpen={ResourcesIsOpen}
                      src={require("/static/images/expand_less_white.png")}
                    />
                  </DropDownButton>
                  <ResourcesDropDown ResourcesIsOpen={ResourcesIsOpen}>
                    <DropDownGroup>
                      <Link
                        activeClassName="active"
                        to={
                          data.allContentfulNewMenu.edges[0].node
                            .menuFourSubitem1.json.content[0].content[1].data
                            .uri
                        }
                      >
                        <div class="onGreenBackground">
                          {
                            data.allContentfulNewMenu.edges[0].node
                              .menuFourSubitem1.json.content[0].content[1]
                              .content[0].value
                          }
                        </div>
                      </Link>
                      <Link
                        activeClassName="active"
                        to={
                          data.allContentfulNewMenu.edges[0].node
                            .menuFourSubitem2.json.content[0].content[1].data
                            .uri
                        }
                      >
                        <div class="onGreenBackground">
                          {" "}
                          {
                            data.allContentfulNewMenu.edges[0].node
                              .menuFourSubitem2.json.content[0].content[1]
                              .content[0].value
                          }
                        </div>
                      </Link>
                      <Link
                        activeClassName="active"
                        to={
                          data.allContentfulNewMenu.edges[0].node
                            .menuFourSubitem3.json.content[0].content[1].data
                            .uri
                        }
                      >
                        <div class="onGreenBackground">
                          {" "}
                          {
                            data.allContentfulNewMenu.edges[0].node
                              .menuFourSubitem3.json.content[0].content[1]
                              .content[0].value
                          }
                        </div>
                      </Link>
                      <Link
                        activeClassName="active"
                        to={
                          data.allContentfulNewMenu.edges[0].node
                            .menuFourSubitem4.json.content[0].content[1].data
                            .uri
                        }
                      >
                        <div class="onGreenBackground">
                          {" "}
                          {
                            data.allContentfulNewMenu.edges[0].node
                              .menuFourSubitem4.json.content[0].content[1]
                              .content[0].value
                          }
                        </div>
                      </Link>
                      <Link
                        activeClassName="active"
                        to={
                          data.allContentfulNewMenu.edges[0].node
                            .menuFourSubitem5.json.content[0].content[1].data
                            .uri
                        }
                      >
                        <div class="onGreenBackground">
                          {" "}
                          {
                            data.allContentfulNewMenu.edges[0].node
                              .menuFourSubitem5.json.content[0].content[1]
                              .content[0].value
                          }
                        </div>
                      </Link>
                      <Link
                        activeClassName="active"
                        to={
                          data.allContentfulNewMenu.edges[0].node
                            .menuFourSubitem6.json.content[0].content[1].data
                            .uri
                        }
                      >
                        <div class="onGreenBackground">
                          {" "}
                          {
                            data.allContentfulNewMenu.edges[0].node
                              .menuFourSubitem6.json.content[0].content[1]
                              .content[0].value
                          }
                        </div>
                      </Link>
                    </DropDownGroup>
                  </ResourcesDropDown>
                </DropDownContainer>
              </MenuItemsContainer>

              <LoginContainer menuOpen={menuOpen}>
                <MenuLangBtns>
                  <DropDownLangContainer
                    onClick={() => {
                      setlangdropdownIsOpen(!langdropdownIsOpen)
                    }}
                  >
                    <DropDownLangButton>
                      <img
                        src={require("/static/images/World_white.svg")}
                        alt=""
                      />
                    </DropDownLangButton>
                    {langdropdownIsOpen && (
                      <LangDropDown>
                        <DropDownLangGroup>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.en}
                            class="onGreenBackground"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src={require("/static/images/flag-en.png")}
                                alt=""
                              />
                              EN
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.fr}
                            class="onGreenBackground"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src={require("/static/images/flag-fr.png")}
                                alt=""
                              />
                              FR
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.es}
                            class="onGreenBackground"
                          >
                            <LanguageOption>
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src={require("/static/images/flag-es.png")}
                                alt=""
                              />
                              ES
                            </LanguageOption>
                          </Link>
                        </DropDownLangGroup>
                      </LangDropDown>
                    )}
                  </DropDownLangContainer>
                </MenuLangBtns>

                <SeparatorContainer>
                  {" "}
                  <Separator></Separator>
                </SeparatorContainer>

                {/* <MenuItem>
                  <Link
                    activeClassName="active"
                    to={
                      data.allContentfulNewMenu.edges[0].node.menuItemFive.json
                        .content[0].content[1].data.uri
                    }
                  >
                    <div class="onGreenBackground">
                      {" "}
                      {
                        data.allContentfulNewMenu.edges[0].node.menuItemFive
                          .json.content[0].content[1].content[0].value
                      }
                    </div>
                  </Link>
                </MenuItem> */}
                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setClientsIsOpen(!ClientsIsOpen)

                      setFeatureIsOpen(false)
                      setResourcesIsOpen(false)
                      setWhyIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                      {
                        data.allContentfulNewMenu.edges[0].node.menuItemSix.json
                          .content[0].content[0].value
                      }
                    </DropDownLabel>
                    <ClientsMoreIcon
                      ClientsIsOpen={ClientsIsOpen}
                      src={require("/static/images/expand_more_white.png")}
                    />
                    <ClientsLessIcon
                      ClientsIsOpen={ClientsIsOpen}
                      src={require("/static/images/expand_less_white.png")}
                    />
                  </DropDownButton>
                  <ClientsDropDown ClientsIsOpen={ClientsIsOpen}>
                    <DropDownGroup>
                      {data.allContentfulNewMenu.edges[0].node.menuSixSubitems.json.content.map(
                        category => {
                          return (
                            <Link
                              target="_parent"
                              activeClassName="active"
                              to={category.content[1].data.uri}
                              class="onGreenBackground"
                            >
                              {category.content[1].content[0].value}
                            </Link>
                          )
                        }
                      )}
                    </DropDownGroup>
                  </ClientsDropDown>
                </DropDownContainer>
                <CTAContainer>
                  <a
                    href="https://web.v3.fieldproapp.com/welcome"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <MenuCTAbutton id="start_trial">
                      Essai gratuit
                    </MenuCTAbutton>
                  </a>
                </CTAContainer>
              </LoginContainer>
            </HeaderContentWrapper>
            <MenuIconContainer>
              <MenuIcon
                menuOpen={menuOpen}
                onClick={() => toggleMenuOpen(!menuOpen)}
              >
                <div />
                <div />
                <div />
              </MenuIcon>
            </MenuIconContainer>
          </HeaderContainer>
        </HeaderGroup>
      )}
    ></StaticQuery>
  )
}

export default NewHeaderDarkFR
