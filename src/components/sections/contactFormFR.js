import React, { Component } from "react"

import { ScheduleButton } from "../Buttons/CtaButton.js"
import { Label } from "../styles/TextStyles.js"
import {
  SucessMsg,
  FormBody,
  FormTitle,
  FormGroup,
  TextArea,
  InputBox,
  Close,
  SuceessMessageWrapper,
} from "../sections/contactForm.js"
class ContactFormFR extends Component {
  constructor(props) {
    super(props)

    this.state = { value: "" }
    this.state = { submitting: false }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  //handle changes in input fileds
  handleChange(event) {
    this.setState({ value: event.target.value })
  }
  //submit to sheets
  handleSubmit(event) {
    event.preventDefault()
    const data = new FormData(event.target)

    fetch(
      "https://script.google.com/macros/s/AKfycbzOHI1aUWy5zJeLwkn_TeqdqMhLP7ZvdiW5OuMNAX8orlZKJesAVJr4K5pOreICYYU/exec",
      {
        method: "POST",
        body: data,
      }
    )
    //reset the form
    document.getElementById("contactform").reset()
    //Confirmation message
    this.setState({
      submitting: true,
    })
    // //make the message dissapear
    // setTimeout(() => {
    //   this.setState({
    //     submitting: false,
    //   })
    // }, 3000)
  }
  handleClick() {
    this.setState({
      submitting: false,
    })
  }

  render() {
    const sucessMessage = (
      <SuceessMessageWrapper>
        <Close onClick={this.handleClick}>&times;</Close>
        <SucessMsg>
          Nous vous remercions de nous avoir contactés, nous vous recontacterons
          bientôt!
        </SucessMsg>
      </SuceessMessageWrapper>
    )
    return (
      <FormBody>
        <FormTitle>Nous sommes à l’écoute de vos besoins</FormTitle>
        <form id="contactform" method="post" onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label>
              Nom<span style={{ color: "red" }}>*</span>
            </Label>
            <InputBox
              type="text"
              name="Name"
              id="Name"
              placeholder="Name"
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>
              Addresse email<span style={{ color: "red" }}>*</span>
            </Label>
            <InputBox
              type="email"
              name="Email"
              id="email"
              placeholder="yourworkemail@yourcompany.com"
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>
              Número de teléfono<span style={{ color: "red" }}>*</span>
            </Label>
            <InputBox
              type="text"
              name="Tel"
              id=""
              placeholder="0700000000"
              required
            />
          </FormGroup>

          <FormGroup>
            <Label>
              Message<span style={{ color: "red" }}>*</span>
            </Label>
            <TextArea
              name="Message"
              type="text"
              id=""
              placeholder="Ecrivez votre message ici"
            />
          </FormGroup>

          <ScheduleButton
            id="contactsend"
            style={{
              background: "#febd55",
              color: "#fefefe",
            }}
            type="submit"
            value={this.state.value}
            onChange={this.handleChange}
          >
            Envoyer
          </ScheduleButton>
        </form>
        {this.state.submitting ? sucessMessage : ""}
      </FormBody>
    )
  }
}

export default ContactFormFR
