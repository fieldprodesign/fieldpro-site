import React, { Component } from "react"

import { ScheduleButton } from "../Buttons/CtaButton"
import { Label } from "../styles/TextStyles"
import {
  SucessMsg,
  FormBody,
  FormTitle,
  FormGroup,
  InputBox,
  TextArea,
  Close,
  SuceessMessageWrapper,
} from "../sections/contactForm.js"
class ContactFormES extends Component {
  constructor(props) {
    super(props)

    this.state = { value: "" }
    this.state = { submitting: false }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  //handle changes in input fileds
  handleChange(event) {
    this.setState({ value: event.target.value })
  }
  //submit to sheets
  handleSubmit(event) {
    event.preventDefault()
    const data = new FormData(event.target)

    fetch(
      "https://script.google.com/macros/s/AKfycbzOHI1aUWy5zJeLwkn_TeqdqMhLP7ZvdiW5OuMNAX8orlZKJesAVJr4K5pOreICYYU/exec",
      {
        method: "POST",
        body: data,
      }
    )
    //reset the form
    document.getElementById("contactform").reset()
    //Confirmation message
    this.setState({
      submitting: true,
    })
    // //make the message dissapear
    // setTimeout(() => {
    //   this.setState({
    //     submitting: false,
    //   })
    // }, 3000)
  }
  handleClick() {
    this.setState({
      submitting: false,
    })
  }

  render() {
    const sucessMessage = (
      <SuceessMessageWrapper>
        <Close onClick={this.handleClick}>&times;</Close>
        <SucessMsg>
          Agradecemos que se ponga en contacto con nosotros ¡Nos pondremos en
          contacto con usted pronto!
        </SucessMsg>
      </SuceessMessageWrapper>
    )
    return (
      <FormBody>
        <FormTitle>
          Hablemos sobre las necesidades de tu Fuerza de Ventas
        </FormTitle>
        <form id="contactform" method="post" onSubmit={this.handleSubmit}>
          <FormGroup>
            <Label>
              Nombre<span style={{ color: "red" }}>*</span>
            </Label>
            <InputBox
              type="text"
              name="Name"
              id="Name"
              placeholder="Name"
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>
              Email Address<span style={{ color: "red" }}>*</span>
            </Label>
            <InputBox
              type="email"
              name="Email"
              id="email"
              placeholder="yourworkemail@yourcompany.com"
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>
              úmero de teléfono<span style={{ color: "red" }}>*</span>
            </Label>
            <InputBox
              type="text"
              name="Tel"
              id=""
              placeholder="0700000000"
              required
            />
          </FormGroup>

          <FormGroup>
            <Label>
              Mensaje<span style={{ color: "red" }}>*</span>
            </Label>
            <TextArea
              name="Message"
              type="text"
              id=""
              placeholder="Escriba su mensaje aquí"
            />
          </FormGroup>

          <ScheduleButton
            id="contactsend"
            style={{
              background: "#febd55",
              color: "#fefefe",
            }}
            type="submit"
            value={this.state.value}
            onChange={this.handleChange}
          >
            Enviar
          </ScheduleButton>
        </form>
        {this.state.submitting ? sucessMessage : ""}
      </FormBody>
    )
  }
}

export default ContactFormES
