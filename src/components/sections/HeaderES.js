import React, { useEffect, useRef, useState } from "react"
import { Link } from "gatsby"
import { StaticQuery, graphql } from "gatsby"

import { HeaderGroup } from "../styles/TextStyles.js"

import {
  MenuContainer,
  MenuGroup,
  MenuLogo,
  MenuLangBtns,
  MenuIconContainer,
  MenuIcon,
  MenuLinks,
  YellowLink,
  MenuButton,
  DropDownContainer,
  DropDownButton,
  DropDownLabel,
  ProductMoreIcon,
  ProductLessIcon,
  RsrcsMoreIcon,
  RsrcsLessIcon,
  IndustriesMoreIcon,
  IndustriesLessIcon,
  DropDownGroup,
  ProductDropDown,
  RsrcsDropDown,
  IndustriesDropDown,
  DropDownLangContainer,
  DropDownLangButton,
  DropDownLangLabel,
  LangDropDown,
  DropDownLangGroup,
  LanguageOption,
  MenuLangBtnsMobile,
} from "../sections/Header.js"

function HeaderES(props) {
  /* */
  const [productIsOpen, setProductIsOpen] = useState(false)
  const [industriesIsOpen, setIndustriesIsOpen] = useState(false)
  const [rsrcsIsOpen, setRsrcsIsOpen] = useState(false)
  const [langdropdownIsOpen, setlangdropdownIsOpen] = useState(false)

  const [menuOpen, toggleMenuOpen] = useState(false)

  const [boxShadow, setBoxShadow] = useState(false)
  const navRef = useRef()
  navRef.current = boxShadow
  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > 50
      if (navRef.current !== show) {
        setBoxShadow(show)
      }
    }
    document.addEventListener("scroll", handleScroll)

    return () => {
      document.removeEventListener("scroll", handleScroll)
    }
  }, [])
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulIndustryPage(filter: { language: { eq: "ES" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
              }
            }
          }
          allContentfulProductPage(filter: { language: { eq: "ES" } }) {
            edges {
              node {
                slug
                pageTitleAsSeenOnMenu
              }
            }
          }
          allContentfulMenu(filter: { language: { eq: "ES Menu" } }) {
            edges {
              node {
                field1 {
                  json
                }
                field2 {
                  json
                }
                field3 {
                  json
                }
                field4 {
                  json
                }
                field5 {
                  json
                }
                field6 {
                  json
                }
                field7 {
                  json
                }
                field8 {
                  json
                }

                subfield52 {
                  json
                }
                subfield53 {
                  json
                }
                subfield54 {
                  json
                }
                subfield55 {
                  json
                }
                subfield61 {
                  json
                }
                subfield62 {
                  json
                }
                subfield63 {
                  json
                }
                subfield64 {
                  json
                }
              }
            }
          }
        }
      `}
      render={data => (
        <HeaderGroup>
          <MenuContainer boxShadow={boxShadow}>
            <MenuGroup>
              <MenuLogo>
                <Link
                  target="_parent"
                  activeClassName="active"
                  to="https://fieldproapp.com/es"
                >
                  <img
                    src={require("../../../static/images/fieldpro-logo.png")}
                    alt="logo"
                  />
                </Link>
              </MenuLogo>

              <MenuLinks menuOpen={menuOpen}>
                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setProductIsOpen(!productIsOpen)
                      setRsrcsIsOpen(false)

                      setIndustriesIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                      {
                        data.allContentfulMenu.edges[0].node.field2.json
                          .content[0].content[0].value
                      }
                    </DropDownLabel>
                    <ProductMoreIcon
                      productIsOpen={productIsOpen}
                      src={require("../../../static/images/expand_more.png")}
                    />
                    <ProductLessIcon
                      productIsOpen={productIsOpen}
                      src={require("../../../static/images/expand_less.png")}
                    />
                  </DropDownButton>
                  <ProductDropDown productIsOpen={productIsOpen}>
                    <DropDownGroup>
                      {data.allContentfulProductPage.edges.map(category => {
                        return (
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={`/${category.node.slug}`}
                            class="darkGrey"
                          >
                            {category.node.pageTitleAsSeenOnMenu}
                          </Link>
                        )
                      })}
                    </DropDownGroup>
                  </ProductDropDown>
                </DropDownContainer>
                <Link
                  target="_parent"
                  activeClassName="active"
                  to={
                    data.allContentfulMenu.edges[0].node.field3.json.content[0]
                      .content[1].data.uri
                  }
                >
                  <div class="darkGrey">
                    {
                      data.allContentfulMenu.edges[0].node.field3.json
                        .content[0].content[1].content[0].value
                    }
                  </div>
                </Link>
                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setIndustriesIsOpen(!industriesIsOpen)
                      setRsrcsIsOpen(false)
                      setProductIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                      {
                        data.allContentfulMenu.edges[0].node.field4.json
                          .content[0].content[0].value
                      }
                    </DropDownLabel>
                    <IndustriesMoreIcon
                      industriesIsOpen={industriesIsOpen}
                      src={require("../../../static/images/expand_more.png")}
                    />
                    <IndustriesLessIcon
                      industriesIsOpen={industriesIsOpen}
                      src={require("../../../static/images/expand_less.png")}
                    />
                  </DropDownButton>
                  <IndustriesDropDown industriesIsOpen={industriesIsOpen}>
                    <DropDownGroup>
                      {data.allContentfulIndustryPage.edges.map(category => {
                        return (
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={`/${category.node.slug}`}
                            class="darkGrey"
                          >
                            {category.node.pageTitleAsSeenOnMenu}
                          </Link>
                        )
                      })}
                    </DropDownGroup>
                  </IndustriesDropDown>
                </DropDownContainer>

                <DropDownContainer>
                  <DropDownButton
                    onClick={() => {
                      setRsrcsIsOpen(!rsrcsIsOpen)
                      setProductIsOpen(false)

                      setIndustriesIsOpen(false)
                    }}
                  >
                    <DropDownLabel>
                      {
                        data.allContentfulMenu.edges[0].node.field5.json
                          .content[0].content[0].value
                      }
                    </DropDownLabel>
                    <RsrcsMoreIcon
                      rsrcsIsOpen={rsrcsIsOpen}
                      src={require("../../../static/images/expand_more.png")}
                    />
                    <RsrcsLessIcon
                      rsrcsIsOpen={rsrcsIsOpen}
                      src={require("../../../static/images/expand_less.png")}
                    />
                  </DropDownButton>
                  <RsrcsDropDown rsrcsIsOpen={rsrcsIsOpen}>
                    <DropDownGroup>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to="/add-ons-es"
                      >
                        <div class="darkGrey">Add ons</div>
                      </Link>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to={
                          data.allContentfulMenu.edges[0].node.field7.json
                            .content[0].content[1].data.uri
                        }
                      >
                        <div class="darkGrey">
                          {" "}
                          {
                            data.allContentfulMenu.edges[0].node.field7.json
                              .content[0].content[1].content[0].value
                          }
                        </div>
                      </Link>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to={
                          data.allContentfulMenu.edges[0].node.subfield55.json
                            .content[0].content[1].data.uri
                        }
                        class="darkGrey"
                      >
                        {
                          data.allContentfulMenu.edges[0].node.subfield55.json
                            .content[0].content[1].content[0].value
                        }
                      </Link>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to={
                          data.allContentfulMenu.edges[0].node.subfield62.json
                            .content[0].content[1].data.uri
                        }
                        class="darkGrey"
                      >
                        {
                          data.allContentfulMenu.edges[0].node.subfield62.json
                            .content[0].content[1].content[0].value
                        }
                      </Link>

                      <Link
                        target="_parent"
                        activeClassName="active"
                        to={
                          data.allContentfulMenu.edges[0].node.subfield52.json
                            .content[0].content[1].data.uri
                        }
                        class="darkGrey"
                      >
                        {
                          data.allContentfulMenu.edges[0].node.subfield52.json
                            .content[0].content[1].content[0].value
                        }
                      </Link>
                      <Link
                        target="_parent"
                        activeClassName="active"
                        to={
                          data.allContentfulMenu.edges[0].node.subfield53.json
                            .content[0].content[1].data.uri
                        }
                        class="darkGrey"
                      >
                        {
                          data.allContentfulMenu.edges[0].node.subfield53.json
                            .content[0].content[1].content[0].value
                        }
                      </Link>
                      <Link
                        target="_blank"
                        activeClassName="active"
                        to="https://optimetriks.invisionapp.com/dsm/design/optimetriks-brand"
                      >
                        <div class="darkGrey">Brand Assets</div>
                      </Link>
                    </DropDownGroup>
                  </RsrcsDropDown>
                </DropDownContainer>

                <YellowLink>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="https://web.fieldproapp.com/"
                    id="login"
                  >
                    <div> Login</div>
                  </Link>
                </YellowLink>
                <MenuButton>
                  <Link
                    target="_parent"
                    activeClassName="active"
                    to="/book-a-demo-es"
                  >
                    <button>Reserva una demo</button>
                  </Link>
                </MenuButton>

                <MenuLangBtns>
                  <DropDownLangContainer
                    onMouseEnter={() => setlangdropdownIsOpen(true)}
                    onMouseLeave={() => setlangdropdownIsOpen(false)}
                  >
                    <DropDownLangButton>
                      <img
                        style={{ padding: "6px 0px 0 0px" }}
                        src="/images/language_world_icon_grey.svg"
                        alt=""
                      />
                      <DropDownLangLabel>Espanol</DropDownLangLabel>
                    </DropDownLangButton>
                    {langdropdownIsOpen && (
                      <LangDropDown>
                        <DropDownLangGroup>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.en}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src={require("../../../static/images/flag-en.png")}
                                alt=""
                              />
                              English
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.fr}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src={require("../../../static/images/flag-fr.png")}
                                alt=""
                              />
                              Français
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.es}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              <img
                                style={{ padding: "8px 4px 0 0px" }}
                                src={require("../../../static/images/flag-es.png")}
                                alt=""
                              />
                              Espanol
                            </LanguageOption>
                          </Link>
                        </DropDownLangGroup>
                      </LangDropDown>
                    )}
                  </DropDownLangContainer>
                </MenuLangBtns>
                <MenuLangBtnsMobile>
                  <DropDownLangContainer
                    onClick={() => {
                      setlangdropdownIsOpen(!langdropdownIsOpen)
                    }}
                  >
                    <DropDownLangButton>
                      <img
                        style={{ padding: "10px 0px 0 0px" }}
                        src="/images/language_world_icon_grey.svg"
                        alt=""
                      />
                      <DropDownLangLabel> Espanol</DropDownLangLabel>
                    </DropDownLangButton>
                    {langdropdownIsOpen && (
                      <LangDropDown>
                        <DropDownLangGroup>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.en}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "12px 4px 0 0px" }}
                                src={require("../../../static/images/flag-en.png")}
                                alt=""
                              />
                              English
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.fr}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              {" "}
                              <img
                                style={{ padding: "12px 4px 0 0px" }}
                                src={require("../../../static/images/flag-fr.png")}
                                alt=""
                              />
                              Français
                            </LanguageOption>
                          </Link>
                          <Link
                            target="_parent"
                            activeClassName="active"
                            to={props.es}
                            class="darkGrey"
                          >
                            <LanguageOption>
                              <img
                                style={{ padding: "12px 4px 0 0px" }}
                                src={require("../../../static/images/flag-es.png")}
                                alt=""
                              />
                              Espanol
                            </LanguageOption>
                          </Link>
                        </DropDownLangGroup>
                      </LangDropDown>
                    )}
                  </DropDownLangContainer>
                </MenuLangBtnsMobile>
              </MenuLinks>
            </MenuGroup>
            <MenuIconContainer>
              <MenuIcon
                menuOpen={menuOpen}
                onClick={() => toggleMenuOpen(!menuOpen)}
              >
                <div />
                <div />
                <div />
              </MenuIcon>
            </MenuIconContainer>
          </MenuContainer>
        </HeaderGroup>
      )}
    />
  )
}

export default HeaderES
