import React, { Component } from "react"
import styled from "styled-components"
import { DownloadButton } from "../Buttons/CtaButton"
import { H3 } from "../styles/TextStyles"

class NewsletterFormFR extends Component {
  constructor(props) {
    super(props)

    this.state = { value: "" }
    this.state = { submitting: false }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  //handle changes in input fileds
  handleChange(event) {
    this.setState({ value: event.target.value })
  }
  //submit to sheets
  handleSubmit(event) {
    event.preventDefault()
    const data = new FormData(event.target)

    fetch(
      "https://script.google.com/macros/s/AKfycbxi18ZApNJqnBNBTeioyXMD1VbNV7IOH-dvTLO0D_0lyjnB609NH8uTJc5pNteHaGOl9A/exec",
      {
        method: "POST",
        body: data,
      }
    )
    //reset the form
    document.getElementById("newsletter").reset()
    //Confirmation message
    this.setState({
      submitting: true,
    })
    //make the message dissapear
    // setTimeout(() => {
    //   this.setState({
    //     submitting: false,
    //   })
    // }, 3000)
  }

  render() {
    const sucessMessage = (
      <SucessMsg>
        Merci de vous êtes inscrit à la newsletter de FieldPro.
      </SucessMsg>
    )
    return (
      <Newsletter>
        <form id="newsletter" method="post" onSubmit={this.handleSubmit}>
          <FormBody>
            <InputBox
              type="email"
              name="Email"
              id="email"
              placeholder="Adresse Email"
              required
            />

            <DownloadButton
              id="subscribenewsletter"
              type="submit"
              value={this.state.value}
              onChange={this.handleChange}
            >
              S'inscrire
            </DownloadButton>
          </FormBody>
          {this.state.submitting ? sucessMessage : ""}
        </form>
      </Newsletter>
    )
  }
}

export default NewsletterFormFR

const Newsletter = styled.div``
const FormBody = styled.div`
  display: grid;
  grid-template-columns: 400px 200px;
  gap: 20px;
  @media (max-width: 450px) {
    grid-template-columns: repeat(1, auto);
  }
`

const InputBox = styled.input`
  width: 400px;
  margin: 10px 0 0 0;
  padding: 0em 1em;
  height: 2.8em;
  border-style: solid;
  border-color: #2c2c2c;
  border-width: 0.1em;
  border-radius: 0.2em;
  font-size: 1.2em;
  outline: none;
  :focus {
    border: solid 1.5px #124e5d;
  }
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  @media (max-width: 450px) {
    width: auto;
  }
`

//Sucess message container
const SucessMsg = styled(H3)`
  display: inline-block;
  background-color: #f5f5f5;
  padding: 1em 1.5em 1em 1.3em;
  margin: 1.5em 0 2em 0;
  border-radius: 0.3em;
  @media (max-width: 450px) {
    max-width: 320px;
  }
`
