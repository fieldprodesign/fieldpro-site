import React from "react"
import styled from "styled-components"
import { ScheduleButton } from "../Buttons/CtaButton"
import { H2, P } from "../styles/TextStyles"
import { StaticQuery, graphql, Link } from "gatsby"
function ClientsFR(props) {
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulHomePage(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                clientsDescription
                clientsMainTitle
                clientsSmallTitle
                clientsLogos1 {
                  fluid {
                    src
                  }
                }
                clientsLogos3 {
                  fluid {
                    src
                  }
                }
                clientLogos4 {
                  fluid {
                    src
                  }
                }
                clientLogos5ThreeImages {
                  fluid {
                    src
                  }
                }
              }
            }
          }
        }
      `}
      render={data => (
        <ClientsWrapper>
          <ContentWrapper>
            <ClientText>
              <ClientsSmallTitle>
                {data.allContentfulHomePage.edges[0].node.clientsSmallTitle}
              </ClientsSmallTitle>
              <ClientTitle>
                {" "}
                {data.allContentfulHomePage.edges[0].node.clientsMainTitle}
              </ClientTitle>
              <ClientDescription>
                {data.allContentfulHomePage.edges[0].node.clientsDescription}
              </ClientDescription>
              <ClientCta>
                <Link to="/témoignages-clients">
                  <ScheduleButton>Lire des études de cas</ScheduleButton>
                </Link>
              </ClientCta>
            </ClientText>
            <BrandsLogos>
              <ColumnA>
                {data.allContentfulHomePage.edges[0].node.clientsLogos1.map(
                  clientlogo => {
                    return (
                      <BrandLogo>
                        <img src={clientlogo.fluid.src} alt="" />
                      </BrandLogo>
                    )
                  }
                )}
              </ColumnA>
              <ColumnB>
                {data.allContentfulHomePage.edges[0].node.clientsLogos3.map(
                  clientlogo2 => {
                    return (
                      <BrandLogo>
                        <img src={clientlogo2.fluid.src} alt="" />
                      </BrandLogo>
                    )
                  }
                )}
              </ColumnB>
              <ColumnC>
                {data.allContentfulHomePage.edges[0].node.clientLogos4.map(
                  clientlogo3 => {
                    return (
                      <BrandLogo>
                        <img src={clientlogo3.fluid.src} alt="" />
                      </BrandLogo>
                    )
                  }
                )}
              </ColumnC>
              <ColumnD>
                {data.allContentfulHomePage.edges[0].node.clientLogos5ThreeImages.map(
                  clientlogo4 => {
                    return (
                      <BrandLogo>
                        <img src={clientlogo4.fluid.src} alt="" />
                      </BrandLogo>
                    )
                  }
                )}
              </ColumnD>
            </BrandsLogos>
          </ContentWrapper>
        </ClientsWrapper>
      )}
    />
  )
}
export default ClientsFR
const ClientsWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 160px 0 180px 0;
  @media (max-width: 830px) {
    padding: 60px 48px 60px 48px;
  }
  @media (max-width: 512px) {
    padding: 60px 24px 60px 24px;
  }
`
const ContentWrapper = styled.div`
  display: grid;
  grid-template-columns: 440px 800px;
  grid-gap: 60px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
`
const ClientText = styled.div`
  margin: 240px 000;
  @media (max-width: 830px) {
    margin: 0;
  }
  @media (max-width: 512px) {
    margin: 0;
  }
`
const ClientsSmallTitle = styled(P)`
  margin: 0 0 12px 0;
  color: #124e5d;
`
const ClientTitle = styled(H2)`
  margin: 0;
`
const ClientDescription = styled(P)`
  max-width: 390px;
  margin-top: 20px;
  @media (max-width: 830px) {
    max-width: 480px;
  }
  @media (max-width: 512px) {
    max-width: 320px;
  }
`
const ClientCta = styled.div``

const BrandsLogos = styled.div`
  display: grid;
  grid-template-columns: repeat(4, auto);
  grid-gap: 20px;

  @media (max-width: 512px) {
    gap: 10px;
  }
`
const ColumnA = styled.div`
  margin: 180px 0 0 0;
  @media (max-width: 512px) {
    margin: 80px 0 0 0;
  }
`
const ColumnB = styled.div`
  margin: 90px 0 0 0;
  @media (max-width: 512px) {
    margin: 40px 0 0 0;
  }
`
const ColumnC = styled.div``
const ColumnD = styled.div`
  margin: 90px 0 0 0;
  @media (max-width: 512px) {
    margin: 40px 0 0 0;
  }
`
const BrandLogo = styled.div`
  width: 160px;
  height: 160px;
  margin-bottom: 20px;
  @media (max-width: 512px) {
    width: 72px;
    height: 72px;
    margin-bottom: 10px;
  }
`
