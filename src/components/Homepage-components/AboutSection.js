import React from "react"
import styled from "styled-components"
import { H2, H4, P } from "../styles/TextStyles"
import { StaticQuery, graphql } from "gatsby"
function AboutSection(props) {
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulHomePage(filter: { language: { eq: "EN" } }) {
            edges {
              node {
                aboutTitle3
                aboutTitle2
                aboutTitle1
                aboutText3 {
                  json
                }
                aboutText2 {
                  json
                }
                aboutText {
                  json
                }
                aboutSectionSmallTitle
                aboutSectionMainTitle
                aboutIcon3 {
                  file {
                    url
                  }
                }
                aboutIcon2 {
                  file {
                    url
                  }
                }
                aboutIcon1 {
                  file {
                    url
                  }
                }
              }
            }
          }
        }
      `}
      render={data => (
        <AboutSectionWrapper>
          <AboutContentWrapper>
            <AboutSmallTitle>
              {" "}
              {data.allContentfulHomePage.edges[0].node.aboutSectionSmallTitle}
            </AboutSmallTitle>
            <AboutTitle>
              {" "}
              {data.allContentfulHomePage.edges[0].node.aboutSectionMainTitle}
            </AboutTitle>
            <AboutTextGroup>
              <AboutTextBlocks>
                <TextBlockIcon>
                  <img
                    src={
                      data.allContentfulHomePage.edges[0].node.aboutIcon1.file
                        .url
                    }
                    alt=""
                  />
                </TextBlockIcon>
                <TextBlockTitle>
                  {" "}
                  {data.allContentfulHomePage.edges[0].node.aboutTitle1}
                </TextBlockTitle>
                <TextBlock>
                  {data.allContentfulHomePage.edges[0].node.aboutText.json.content[1].content.map(
                    AbtItem => {
                      return (
                        <AbtContent>
                          {AbtItem.content[0].content[0].value}
                        </AbtContent>
                      )
                    }
                  )}
                </TextBlock>
              </AboutTextBlocks>
              <AboutTextBlocks>
                <TextBlockIcon>
                  <img
                    src={
                      data.allContentfulHomePage.edges[0].node.aboutIcon2.file
                        .url
                    }
                    alt=""
                  />
                </TextBlockIcon>
                <TextBlockTitle>
                  {" "}
                  {data.allContentfulHomePage.edges[0].node.aboutTitle2}
                </TextBlockTitle>
                <TextBlock>
                  {data.allContentfulHomePage.edges[0].node.aboutText2.json.content[1].content.map(
                    AbtItem2 => {
                      return (
                        <AbtContent>
                          {AbtItem2.content[0].content[0].value}
                        </AbtContent>
                      )
                    }
                  )}
                </TextBlock>
              </AboutTextBlocks>
              <AboutTextBlocks>
                <TextBlockIcon>
                  <img
                    src={
                      data.allContentfulHomePage.edges[0].node.aboutIcon3.file
                        .url
                    }
                    alt=""
                  />
                </TextBlockIcon>
                <TextBlockTitle>
                  {" "}
                  {data.allContentfulHomePage.edges[0].node.aboutTitle3}
                </TextBlockTitle>
                <TextBlock>
                  {data.allContentfulHomePage.edges[0].node.aboutText3.json.content[1].content.map(
                    AbtItem3 => {
                      return (
                        <AbtContent>
                          {AbtItem3.content[0].content[0].value}
                        </AbtContent>
                      )
                    }
                  )}
                </TextBlock>
              </AboutTextBlocks>
            </AboutTextGroup>
          </AboutContentWrapper>
        </AboutSectionWrapper>
      )}
    />
  )
}
export default AboutSection
const AboutSectionWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 120px 0 160px 0;
  @media (max-width: 830px) {
    padding: 60px 48px 60px 48px;
  }
  @media (max-width: 512px) {
    padding: 60px 24px 60px 24px;
  }
`
const AboutContentWrapper = styled.div``
const AboutTextBlocks = styled.div`
  max-width: 360px;
`
const AboutTextGroup = styled.div`
  padding: 80px 0 0 40px;
  display: grid;
  grid-template-columns: 400px 400px 400px;
  grid-gap: 40px;
  @media (max-width: 830px) {
    padding: 40px 000;
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
  @media (max-width: 512px) {
    padding: 40px 000;
    grid-template-columns: repeat(1, auto);
    grid-gap: 20px;
  }
`
const TextBlockTitle = styled(H4)`
  margin: 0 0 20px 0;
`
const TextBlock = styled.ul``
const AboutTitle = styled(H2)`
  margin: 0;
`
const AbtContent = styled.li`
  font-weight: 400;
  font-size: 1.125em;
  line-height: 1.5;
  margin: 0 0 1em 0;
  padding: 0;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  list-style-image: url("/images/check_circle-24px.svg");
`
const AboutSmallTitle = styled(P)`
  margin: 0 0 12px 0;
  color: #124e5d;
`
const TextBlockIcon = styled.div`
  margin: 0;
`
