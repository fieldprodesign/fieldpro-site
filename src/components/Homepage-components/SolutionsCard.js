import React from "react"
import styled from "styled-components"
import { H4, P } from "../styles/TextStyles.js"
function SolutionsCard(props) {
  const { iconPic, title, description } = props
  return (
    <SolutionsCardWrapper>
      <SolutionsIcon>{iconPic}</SolutionsIcon>
      <SolutionsTitle>{title}</SolutionsTitle>
      <SolutionsDescription>{description}</SolutionsDescription>
    </SolutionsCardWrapper>
  )
}
export default SolutionsCard
const SolutionsCardWrapper = styled.div`
  width: 400px;
  height: 340px;
  border: 1px solid #979797;
  border-radius: 5px;
  padding: 40px;
  :hover {
    border: none;
    background-color: #e7edef;
    box-shadow: 0 2px 4px 0 #d0dcdf;
  }
  @media (max-width: 830px) {
    width: 320px;
    height: auto;
  }
  @media (max-width: 512px) {
    width: 320px;
    height: auto;
  }
`
const SolutionsIcon = styled.div`
  height: 54px;
  width: 54px;
`
const SolutionsTitle = styled(H4)`
  margin: 20px 0 10px 0;
`
const SolutionsDescription = styled(P)``
