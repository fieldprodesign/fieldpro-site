import React from "react"
import styled from "styled-components"
import { H2, P } from "../styles/TextStyles"
import { StaticQuery, graphql } from "gatsby"
function MetricsSection(props) {
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulHomePage(filter: { language: { eq: "EN" } }) {
            edges {
              node {
                benefitsMainTitle
                benefitsSmallTitle
                metricDescription1
                metricDescription2
                metricDescription3
                metricIcon1 {
                  file {
                    url
                  }
                }
                metricIcon2 {
                  file {
                    url
                  }
                }
                metricIcon3 {
                  file {
                    url
                  }
                }
                metricTitle1
                metricTitle2
                metricTitle3
              }
            }
          }
        }
      `}
      render={data => (
        <MetricsBackground>
          <MetricsSectionWrapper>
            <ContentWrapper>
              <MetricsSmallTitle>
                {" "}
                {data.allContentfulHomePage.edges[0].node.benefitsSmallTitle}
              </MetricsSmallTitle>
              <MetricsTitle>
                {data.allContentfulHomePage.edges[0].node.benefitsMainTitle}
              </MetricsTitle>
              <MetricsScorecardGroup>
                <MetricsScoreCard>
                  <MetricsIcon>
                    <img
                      src={
                        data.allContentfulHomePage.edges[0].node.metricIcon1
                          .file.url
                      }
                      alt="customer visits"
                    />
                  </MetricsIcon>
                  <MetricsContent>
                    <MetricsCardTitle>
                      {" "}
                      {data.allContentfulHomePage.edges[0].node.metricTitle1}
                    </MetricsCardTitle>
                    <MetricsDescription>
                      {
                        data.allContentfulHomePage.edges[0].node
                          .metricDescription1
                      }
                    </MetricsDescription>
                  </MetricsContent>
                </MetricsScoreCard>
                <MetricsScoreCard>
                  <MetricsIcon>
                    <img
                      src={
                        data.allContentfulHomePage.edges[0].node.metricIcon2
                          .file.url
                      }
                      alt="customer visits"
                    />
                  </MetricsIcon>
                  <MetricsContent>
                    <MetricsCardTitle>
                      {" "}
                      {data.allContentfulHomePage.edges[0].node.metricTitle2}
                    </MetricsCardTitle>
                    <MetricsDescription>
                      {
                        data.allContentfulHomePage.edges[0].node
                          .metricDescription2
                      }
                    </MetricsDescription>
                  </MetricsContent>
                </MetricsScoreCard>
                <MetricsScoreCard>
                  <MetricsIcon>
                    <img
                      src={
                        data.allContentfulHomePage.edges[0].node.metricIcon3
                          .file.url
                      }
                      alt="customer visits"
                    />
                  </MetricsIcon>
                  <MetricsContent>
                    <MetricsCardTitle>
                      {" "}
                      {data.allContentfulHomePage.edges[0].node.metricTitle3}
                    </MetricsCardTitle>
                    <MetricsDescription>
                      {
                        data.allContentfulHomePage.edges[0].node
                          .metricDescription3
                      }
                    </MetricsDescription>
                  </MetricsContent>
                </MetricsScoreCard>
              </MetricsScorecardGroup>
            </ContentWrapper>
          </MetricsSectionWrapper>
        </MetricsBackground>
      )}
    />
  )
}
export default MetricsSection
const MetricsBackground = styled.div`
  background-color: #f5f5f5;
  width: 100vw;
  position: relative;
  left: -8px;
`
const MetricsSectionWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 200px 0 200px 0;
  @media (max-width: 830px) {
    padding: 80px 48px 80px 48px;
  }
  @media (max-width: 512px) {
    padding: 60px 24px 60px 24px;
  }
`
const ContentWrapper = styled.div``

const MetricsScorecardGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(3, auto);
  @media (max-width: 830px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 60px;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 10px;
  }
`
const MetricsTitle = styled(H2)`
  margin: 0 0 60px 0;
`
const MetricsScoreCard = styled.div`
  width: 385px;
  height: 180px;

  @media (max-width: 450px) {
    max-width: 320px;
    margin: 40px 16px;
  }
`
const MetricsCardTitle = styled.p`
  font-weight: 800;
  font-size: 90px;
  line-height: 60px;
  padding: 60px 0 0 0;
  @media (max-width: 450px) {
    font-size: 80px;
  }
`
const MetricsDescription = styled(P)`
  font-size: 20px;
`
const MetricsIcon = styled.div``
const MetricsContent = styled.div`
  margin: -120px 0px 0px 50px;
`
const MetricsSmallTitle = styled(P)`
  margin: 0 0 12px 0;
  color: #124e5d;
`
