import React, { useCallback, useEffect, useRef, useState } from "react"
import styled from "styled-components"
import { RoundButtonNext, RoundButtonPrev } from "../Buttons/CtaButton.js"

const TestimonialCardSlider = ({ children }) => {
  const [targetSlide, setTargetSlide] = useState(0)
  const wrapperRef = useRef(null)
  const targetSlideRef = useRef(null)

  const scrollToTargetSlide = useCallback(() => {
    const targetSlide = targetSlideRef.current
    const wrapper = wrapperRef.current
    if (wrapper && targetSlide) {
      wrapper.scrollTo({
        top: 0,
        left: targetSlide.offsetLeft,
        behavior: "smooth",
      })
    }
  }, [])

  const moveLeft = useCallback(targetSlide => Math.max(0, targetSlide - 1), [])
  const moveRight = useCallback(
    targetSlide => Math.min(targetSlide + 1, children.length - 1),
    [children]
  )

  useEffect(scrollToTargetSlide, [targetSlide])

  return (
    <div id="trap">
      <Wrapper ref={wrapperRef}>
        {children.map((child, i) => (
          <Slide
            key={`slide-${i}`}
            ref={i === targetSlide ? targetSlideRef : null}
          >
            {child}
          </Slide>
        ))}
      </Wrapper>
      <Navbuttons>
        <RoundButtonPrev onClick={() => setTargetSlide(moveLeft)} />
        <RoundButtonNext onClick={() => setTargetSlide(moveRight)} />
      </Navbuttons>
    </div>
  )
}

export default TestimonialCardSlider
const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(30, auto);
  grid-gap: 40px;
  position: relative;
  overflow-x: auto;
  overflow-y: hidden;
  white-space: nowrap;
  max-width: 1200px;
  padding: 40px 0px 40px 0px;
`

const Slide = styled.div`
  white-space: normal;
`
const Navbuttons = styled.div`
  display: flex;

  gap: 30px;
  margin: -280px 0 20px -440px;
  @media (max-width: 830px) {
    margin: -720px 0 600px 0px;
    padding: 0 0 120px 0;
    display: flex;
  }
  @media (max-width: 512px) {
    margin: -720px 0 600px 0px;
    padding: 0 0 120px 0;

    display: flex;
  }
`
