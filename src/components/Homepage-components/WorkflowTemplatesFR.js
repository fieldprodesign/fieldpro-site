import React from "react"
import styled from "styled-components"
import { ScheduleButton } from "../Buttons/CtaButton"
import { H2, P } from "../styles/TextStyles"
import WorkflowCard from "./workflowcard"
import { StaticQuery, graphql, Link } from "gatsby"
function WorkflowTemplatesFR(props) {
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulHomePage(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                workflowTemplateTitle
                workflowTemplatesSmallTitle
              }
            }
          }
          allContentfulHomePageThree(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                workflowCard4 {
                  file {
                    url
                  }
                }
                workflowCard5 {
                  file {
                    url
                  }
                }
                workflowCard6 {
                  file {
                    url
                  }
                }
                workflowCardIcon1 {
                  file {
                    url
                  }
                }
                workflowCardIcon2 {
                  file {
                    url
                  }
                }
                workflowCardIcon3 {
                  file {
                    url
                  }
                }
                workflowCardLink1 {
                  json
                }
                workflowCardLink2 {
                  json
                }
                workflowCardLink3 {
                  json
                }
                workflowCardLink4 {
                  json
                }
                workflowCardLink5 {
                  json
                }
                workflowCardLink6 {
                  json
                }
                workflowCardTitle6
                workflowCardTitle5
                workflowCardTitle4
                workflowCardTitle3
                workflowCardTitle2
                workflowCardTitle1
                workflowDescription1 {
                  workflowDescription1
                }
                workflowDescription2 {
                  workflowDescription2
                }
                workflowDescription3 {
                  workflowDescription3
                }
                workflowDescription4 {
                  workflowDescription4
                }
                workflowDescription5 {
                  workflowDescription5
                }
                workflowDescription6 {
                  workflowDescription6
                }
              }
            }
          }
        }
      `}
      render={data => (
        <WorkflowTemplateWrapper>
          <WorkflowSmallTitle>
            {" "}
            {
              data.allContentfulHomePage.edges[0].node
                .workflowTemplatesSmallTitle
            }
          </WorkflowSmallTitle>
          <WorkflowTitle>
            {data.allContentfulHomePage.edges[0].node.workflowTemplateTitle}
          </WorkflowTitle>
          <WorkflowCta>
            <Link to="/workflowtemplates">
              <ScheduleButton>Explorer les modèles</ScheduleButton>
            </Link>
          </WorkflowCta>
          <WorkflowCardsGroup>
            <Link
              to={
                data.allContentfulHomePageThree.edges[0].node.workflowCardLink1
                  .json.content[0].content[1].data.uri
              }
            >
              <WorkflowCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageThree.edges[0].node
                        .workflowCardIcon1.file.url
                    }
                    alt="In-store Execution"
                  />
                }
                title={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowCardTitle1
                }
                description={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowDescription1.workflowDescription1
                }
              />
            </Link>
            <Link
              to={
                data.allContentfulHomePageThree.edges[0].node.workflowCardLink2
                  .json.content[0].content[1].data.uri
              }
            >
              <WorkflowCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageThree.edges[0].node
                        .workflowCardIcon2.file.url
                    }
                    alt="Mobile Inspections"
                  />
                }
                title={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowCardTitle2
                }
                description={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowDescription2.workflowDescription2
                }
              />
            </Link>
            <Link
              to={
                data.allContentfulHomePageThree.edges[0].node.workflowCardLink3
                  .json.content[0].content[1].data.uri
              }
            >
              <WorkflowCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageThree.edges[0].node
                        .workflowCardIcon3.file.url
                    }
                    alt="Mobile CRM"
                  />
                }
                title={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowCardTitle3
                }
                description={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowDescription3.workflowDescription3
                }
              />
            </Link>
            <Link
              to={
                data.allContentfulHomePageThree.edges[0].node.workflowCardLink4
                  .json.content[0].content[1].data.uri
              }
            >
              <WorkflowCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageThree.edges[0].node
                        .workflowCard4.file.url
                    }
                    alt="Sales Automation"
                  />
                }
                title={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowCardTitle4
                }
                description={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowDescription4.workflowDescription4
                }
              />
            </Link>
            <Link
              to={
                data.allContentfulHomePageThree.edges[0].node.workflowCardLink5
                  .json.content[0].content[1].data.uri
              }
            >
              <WorkflowCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageThree.edges[0].node
                        .workflowCard5.file.url
                    }
                    alt="Field Force Monotoring"
                  />
                }
                title={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowCardTitle5
                }
                description={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowDescription5.workflowDescription5
                }
              />
            </Link>
            <Link
              to={
                data.allContentfulHomePageThree.edges[0].node.workflowCardLink6
                  .json.content[0].content[1].data.uri
              }
            >
              <WorkflowCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageThree.edges[0].node
                        .workflowCard6.file.url
                    }
                    alt="No-Code App Builder"
                  />
                }
                title={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowCardTitle6
                }
                description={
                  data.allContentfulHomePageThree.edges[0].node
                    .workflowDescription6.workflowDescription6
                }
              />
            </Link>
          </WorkflowCardsGroup>
        </WorkflowTemplateWrapper>
      )}
    />
  )
}
export default WorkflowTemplatesFR
const WorkflowTemplateWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 160px 0 120px 0;
  @media (max-width: 830px) {
    padding: 60px 48px 60px 48px;
  }
  @media (max-width: 512px) {
    padding: 60px 24px 60px 24px;
  }
`
const WorkflowCardsGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(3, auto);
  grid-gap: 60px 40px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    grid-gap: 20px;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 10px;
  }
`
const WorkflowTitle = styled(H2)`
  max-width: 800px;
  margin: 0;
  padding: 0;
`
const WorkflowCta = styled.div`
  margin: 20px 0 60px 0;
  padding: 0;
`
const WorkflowSmallTitle = styled(P)`
  margin: 0 0 12px 0;
  color: #124e5d;
`
