import React from "react"
import styled from "styled-components"
import { H4, P } from "../styles/TextStyles.js"
function WorkflowCard(props) {
  const { iconPic, title, description } = props
  return (
    <WorkflowCardWrapper>
      <WorkflowCardIcon>{iconPic}</WorkflowCardIcon>
      <WorkflowCardTitle>{title}</WorkflowCardTitle>
      <WorkflowCardDescription>{description}</WorkflowCardDescription>
    </WorkflowCardWrapper>
  )
}
export default WorkflowCard
const WorkflowCardWrapper = styled.div`
  width: 400px;
  height: 340px;
  border: 1px solid #979797;
  border-radius: 5px;
  padding: 40px;
  :hover {
    border: 2px solid #2c2c2c;
  }
  @media (max-width: 830px) {
    width: 320px;
    height: auto;
  }
  @media (max-width: 512px) {
    width: 320px;
    height: auto;
  }
`
const WorkflowCardIcon = styled.div`
  height: 54px;
  width: 54px;
`
const WorkflowCardTitle = styled(H4)`
  margin: 20px 0 10px 0;
`
const WorkflowCardDescription = styled(P)``
