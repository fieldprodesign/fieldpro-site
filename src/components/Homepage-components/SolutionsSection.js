import React from "react"
import styled from "styled-components"
import { H2 } from "../styles/TextStyles.js"
import SolutionsCard from "./SolutionsCard.js"
import { StaticQuery, graphql, Link } from "gatsby"
function SolutionsSection(props) {
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulHomePage(filter: { language: { eq: "EN" } }) {
            edges {
              node {
                solutionSectionTitle
              }
            }
          }
          allContentfulHomePageTwo(filter: { language: { eq: "EN" } }) {
            edges {
              node {
                solutionCard4 {
                  file {
                    url
                  }
                }
                solutionCard5 {
                  file {
                    url
                  }
                }
                solutionCard6 {
                  file {
                    url
                  }
                }
                solutionCardIcon1 {
                  file {
                    url
                  }
                }
                solutionCardIcon2 {
                  file {
                    url
                  }
                }
                solutionCardIcon3 {
                  file {
                    url
                  }
                }
                solutionCardLink1 {
                  json
                }
                solutionCardLink2 {
                  json
                }
                solutionCardLink3 {
                  json
                }
                solutionCardLink4 {
                  json
                }
                solutionCardLink5 {
                  json
                }
                solutionCardLink6 {
                  json
                }
                solutionCardTitle1
                solutionCardTitle2
                solutionCardTitle3
                solutionCardTitle5
                solutionCardTitle6
                solutionDescription1 {
                  solutionDescription1
                }
                solutionCardTitle4
                solutionDescription2 {
                  solutionDescription2
                }
                solutionDescription3 {
                  solutionDescription3
                }
                solutionDescription4 {
                  solutionDescription4
                }
                solutionDescription5 {
                  solutionDescription5
                }
                solutionDescription6 {
                  solutionDescription6
                }
              }
            }
          }
        }
      `}
      render={data => (
        <SolutionsWrapper>
          <SectionTitle>
            {data.allContentfulHomePage.edges[0].node.solutionSectionTitle}
          </SectionTitle>
          <SolutionCardsGroup>
            <Link
              to={
                data.allContentfulHomePageTwo.edges[0].node.solutionCardLink1
                  .json.content[0].content[1].data.uri
              }
            >
              <SolutionsCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageTwo.edges[0].node
                        .solutionCardIcon1.file.url
                    }
                    alt="In-store Execution"
                  />
                }
                title={
                  data.allContentfulHomePageTwo.edges[0].node.solutionCardTitle1
                }
                description={
                  data.allContentfulHomePageTwo.edges[0].node
                    .solutionDescription1.solutionDescription1
                }
              />
            </Link>
            <Link
              to={
                data.allContentfulHomePageTwo.edges[0].node.solutionCardLink2
                  .json.content[0].content[1].data.uri
              }
            >
              <SolutionsCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageTwo.edges[0].node
                        .solutionCardIcon2.file.url
                    }
                    alt="Mobile Inspections"
                  />
                }
                title={
                  data.allContentfulHomePageTwo.edges[0].node.solutionCardTitle2
                }
                description={
                  data.allContentfulHomePageTwo.edges[0].node
                    .solutionDescription2.solutionDescription2
                }
              />
            </Link>
            <Link
              to={
                data.allContentfulHomePageTwo.edges[0].node.solutionCardLink3
                  .json.content[0].content[1].data.uri
              }
            >
              <SolutionsCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageTwo.edges[0].node
                        .solutionCardIcon3.file.url
                    }
                    alt="Mobile CRM"
                  />
                }
                title={
                  data.allContentfulHomePageTwo.edges[0].node.solutionCardTitle3
                }
                description={
                  data.allContentfulHomePageTwo.edges[0].node
                    .solutionDescription3.solutionDescription3
                }
              />
            </Link>
            <Link
              to={
                data.allContentfulHomePageTwo.edges[0].node.solutionCardLink4
                  .json.content[0].content[1].data.uri
              }
            >
              <SolutionsCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageTwo.edges[0].node.solutionCard4
                        .file.url
                    }
                    alt="Sales Automation"
                  />
                }
                title={
                  data.allContentfulHomePageTwo.edges[0].node.solutionCardTitle4
                }
                description={
                  data.allContentfulHomePageTwo.edges[0].node
                    .solutionDescription4.solutionDescription4
                }
              />
            </Link>
            <Link
              to={
                data.allContentfulHomePageTwo.edges[0].node.solutionCardLink5
                  .json.content[0].content[1].data.uri
              }
            >
              <SolutionsCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageTwo.edges[0].node.solutionCard5
                        .file.url
                    }
                    alt="Field Force Monotoring"
                  />
                }
                title={
                  data.allContentfulHomePageTwo.edges[0].node.solutionCardTitle5
                }
                description={
                  data.allContentfulHomePageTwo.edges[0].node
                    .solutionDescription5.solutionDescription5
                }
              />
            </Link>
            <Link
              to={
                data.allContentfulHomePageTwo.edges[0].node.solutionCardLink6
                  .json.content[0].content[1].data.uri
              }
            >
              <SolutionsCard
                iconPic={
                  <img
                    src={
                      data.allContentfulHomePageTwo.edges[0].node.solutionCard6
                        .file.url
                    }
                    alt="No-Code App Builder"
                  />
                }
                title={
                  data.allContentfulHomePageTwo.edges[0].node.solutionCardTitle6
                }
                description={
                  data.allContentfulHomePageTwo.edges[0].node
                    .solutionDescription6.solutionDescription6
                }
              />
            </Link>
          </SolutionCardsGroup>
        </SolutionsWrapper>
      )}
    />
  )
}
export default SolutionsSection
const SolutionsWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 120px 0 120px 0;
  @media (max-width: 830px) {
    padding: 60px 48px 60px 48px;
  }
  @media (max-width: 512px) {
    padding: 60px 24px 60px 24px;
  }
`
const SectionTitle = styled(H2)`
  margin: 0 0 60px 0;
`
const SolutionCardsGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(3, auto);
  grid-gap: 60px 40px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(2, auto);
    grid-gap: 20px;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 10px;
  }
`
