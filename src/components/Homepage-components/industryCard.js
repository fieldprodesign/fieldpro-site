import React from "react"
import styled from "styled-components"
import { H5 } from "../styles/TextStyles.js"
function IndustryCard(props) {
  const { iconPic, title } = props
  return (
    <IndustryCardWrapper>
      <IndustryCardIcon>{iconPic}</IndustryCardIcon>
      <IndustryCardTitle>{title}</IndustryCardTitle>
    </IndustryCardWrapper>
  )
}
export default IndustryCard
const IndustryCardWrapper = styled.div`
  width: 198px;

  border: none;
  border-radius: 2px;
  padding: 20px 20px 30px 20px;
  background-color: #2a606d;
  :hover {
    border: 2px solid #fefefe;
    background-color: #104654;
  }

  @media (max-width: 512px) {
    width: 152px;
    height: 138px;
  }
`
const IndustryCardIcon = styled.div`
  height: 48px;
  width: 48px;
`
const IndustryCardTitle = styled(H5)`
  margin: 10px 0 0px 0;
  color: #fefefe;
`
