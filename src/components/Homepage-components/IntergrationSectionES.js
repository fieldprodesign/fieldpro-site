import React from "react"
import styled from "styled-components"
import { H3, P } from "../styles/TextStyles"
import { StaticQuery, graphql } from "gatsby"
function IntergrationSectionES(props) {
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulHomePage(filter: { language: { eq: "ES" } }) {
            edges {
              node {
                integrationSmallTitle
                integrationDescription {
                  integrationDescription
                }
                integrationBrandLogos {
                  file {
                    url
                  }
                }
              }
            }
          }
        }
      `}
      render={data => (
        <IntergrationSectionWrapper>
          <IntergrationContent>
            <IntergrationText>
              <IntergrationsSmallTitle>
                {" "}
                {data.allContentfulHomePage.edges[0].node.integrationSmallTitle}
              </IntergrationsSmallTitle>
              <IntegrationTitle>
                {
                  data.allContentfulHomePage.edges[0].node
                    .integrationDescription.integrationDescription
                }
              </IntegrationTitle>
            </IntergrationText>
            <IntergrationBrandLogos>
              <RowA>
                {data.allContentfulHomePage.edges[0].node.integrationBrandLogos.map(
                  brandicon => {
                    return (
                      <BrandLogo>
                        <img src={brandicon.file.url} alt="" />
                      </BrandLogo>
                    )
                  }
                )}
                {/* <BrandLogo>
                  <img src="/images/africas talking logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/beyonic logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/Dataloop logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/freshdesk logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/gravity logo.png" alt="" />
                </BrandLogo> */}
              </RowA>
              {/* <RowB>
                <BrandLogo>
                  <img src="/images/mailjet logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/mapotempo logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/microsft nav logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/odoo logo.png" alt="" />
                </BrandLogo>
              </RowB>
              <RowC>
                <BrandLogo>
                  <img src="/images/paystack logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/pezesha logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/syspro logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/Twilio logo.png" alt="" />
                </BrandLogo>
                <BrandLogo>
                  <img src="/images/zendesk logo.png" alt="" />
                </BrandLogo>
              </RowC> */}
            </IntergrationBrandLogos>
          </IntergrationContent>
        </IntergrationSectionWrapper>
      )}
    />
  )
}
export default IntergrationSectionES
const IntergrationSectionWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 200px 0 120px 0;
  @media (max-width: 830px) {
    padding: 60px 48px 60px 48px;
  }
  @media (max-width: 512px) {
    padding: 60px 24px 60px 24px;
  }
`
const IntergrationContent = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 60px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 20px;
  }
`
const IntergrationsSmallTitle = styled(P)`
  margin: 0 0 12px 0;
  color: #124e5d;
`
const IntergrationText = styled.div`
  margin: 0px 0 0 0;
`
const IntegrationTitle = styled(H3)`
  max-width: 500px;
  margin: 0;

  font-weight: 600;
  @media (max-width: 830px) {
    max-width: 600px;
  }
  @media (max-width: 512px) {
    max-width: 320px;
  }
`
const IntergrationBrandLogos = styled.div``
const RowA = styled.div`
  margin: 0 0 20px 0;
  padding: 0;
  display: grid;
  grid-template-columns: repeat(5, auto);
  grid-gap: 20px 40px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(4, auto);
    grid-gap: 20px;
    margin: 0;
    padding: 0;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(2, auto);
    grid-gap: 20px;
    margin: 0;
    padding: 0;
  }
`

const BrandLogo = styled.div`
  height: 120px;
  width: 120px;
`
