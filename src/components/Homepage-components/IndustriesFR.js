import React from "react"
import styled from "styled-components"
import { BodyMain, H2 } from "../styles/TextStyles"

import { StaticQuery, graphql, Link } from "gatsby"
import IndustryCardFR from "./industryCardFR"
function IndustriesFR(props) {
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulHomePage(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                industryTitle
                industryDescription {
                  industryDescription
                }
              }
            }
          }
          allContentfulHomePageTwo(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                industryCardIcon1 {
                  file {
                    url
                  }
                }
                industryCardIcon2 {
                  file {
                    url
                  }
                }
                industryCardIcon3 {
                  file {
                    url
                  }
                }
                industryCardIcon4 {
                  file {
                    url
                  }
                }
                industryCardIcon5 {
                  file {
                    url
                  }
                }
                industryCardIcon6 {
                  file {
                    url
                  }
                }
                industryCardIcon7 {
                  file {
                    url
                  }
                }
                industryCardIcon8 {
                  file {
                    url
                  }
                }
                industryCardTitle8
                industryCardTitle7
                industryCardTitle6
                industryCardTitle5
                industryCardTitle4
                industryCardTitle3
                industryCardTitle2
                industryCardTitle1
                industryCardLink1 {
                  json
                }
                industryCardLink2 {
                  json
                }
                industryCardLink3 {
                  json
                }
                industryCardLink4 {
                  json
                }
                industryCardLink5 {
                  json
                }
                industryCardLink6 {
                  json
                }
                industryCardLink7 {
                  json
                }
                industryCardLink8 {
                  json
                }
              }
            }
          }
        }
      `}
      render={data => (
        <IndustriesBackground>
          <HeroIllustration>
            <img src="/images/Group 16.svg" alt="" />
          </HeroIllustration>
          <IndustriesWrapper>
            <ContentWrapper>
              <IndustryTextWrapper>
                <IndustriesTitle>
                  {" "}
                  {data.allContentfulHomePage.edges[0].node.industryTitle}
                </IndustriesTitle>
                <IndustriesDescription>
                  {
                    data.allContentfulHomePage.edges[0].node.industryDescription
                      .industryDescription
                  }
                </IndustriesDescription>
              </IndustryTextWrapper>
              <IndustriesCardsGroup>
                <Link
                  to={
                    data.allContentfulHomePageTwo.edges[0].node
                      .industryCardLink1.json.content[0].content[1].data.uri
                  }
                >
                  <IndustryCardFR
                    iconPic={
                      <img
                        src={
                          data.allContentfulHomePageTwo.edges[0].node
                            .industryCardIcon1.file.url
                        }
                        alt=""
                      />
                    }
                    title={
                      data.allContentfulHomePageTwo.edges[0].node
                        .industryCardTitle1
                    }
                  />
                </Link>
                <Link
                  to={
                    data.allContentfulHomePageTwo.edges[0].node
                      .industryCardLink2.json.content[0].content[1].data.uri
                  }
                >
                  <IndustryCardFR
                    iconPic={
                      <img
                        src={
                          data.allContentfulHomePageTwo.edges[0].node
                            .industryCardIcon2.file.url
                        }
                        alt="Industrial Goods"
                      />
                    }
                    title={
                      data.allContentfulHomePageTwo.edges[0].node
                        .industryCardTitle2
                    }
                  />
                </Link>
                <Link
                  to={
                    data.allContentfulHomePageTwo.edges[0].node
                      .industryCardLink3.json.content[0].content[1].data.uri
                  }
                >
                  <IndustryCardFR
                    iconPic={
                      <img
                        src={
                          data.allContentfulHomePageTwo.edges[0].node
                            .industryCardIcon3.file.url
                        }
                        alt="Dairy"
                      />
                    }
                    title={
                      data.allContentfulHomePageTwo.edges[0].node
                        .industryCardTitle3
                    }
                  />
                </Link>
                <Link
                  to={
                    data.allContentfulHomePageTwo.edges[0].node
                      .industryCardLink4.json.content[0].content[1].data.uri
                  }
                >
                  <IndustryCardFR
                    iconPic={
                      <img
                        src={
                          data.allContentfulHomePageTwo.edges[0].node
                            .industryCardIcon4.file.url
                        }
                        alt="Beverages"
                      />
                    }
                    title={
                      data.allContentfulHomePageTwo.edges[0].node
                        .industryCardTitle4
                    }
                  />
                </Link>
                <Link
                  to={
                    data.allContentfulHomePageTwo.edges[0].node
                      .industryCardLink5.json.content[0].content[1].data.uri
                  }
                >
                  <IndustryCardFR
                    iconPic={
                      <img
                        src={
                          data.allContentfulHomePageTwo.edges[0].node
                            .industryCardIcon5.file.url
                        }
                        alt="Water Access"
                      />
                    }
                    title={
                      data.allContentfulHomePageTwo.edges[0].node
                        .industryCardTitle5
                    }
                  />
                </Link>
                <Link
                  to={
                    data.allContentfulHomePageTwo.edges[0].node
                      .industryCardLink6.json.content[0].content[1].data.uri
                  }
                >
                  <IndustryCardFR
                    iconPic={
                      <img
                        src={
                          data.allContentfulHomePageTwo.edges[0].node
                            .industryCardIcon6.file.url
                        }
                        alt="Consumer Goods"
                      />
                    }
                    title={
                      data.allContentfulHomePageTwo.edges[0].node
                        .industryCardTitle6
                    }
                  />
                </Link>
                <Link
                  to={
                    data.allContentfulHomePageTwo.edges[0].node
                      .industryCardLink7.json.content[0].content[1].data.uri
                  }
                >
                  <IndustryCardFR
                    iconPic={
                      <img
                        src={
                          data.allContentfulHomePageTwo.edges[0].node
                            .industryCardIcon7.file.url
                        }
                        alt="Agriculture"
                      />
                    }
                    title={
                      data.allContentfulHomePageTwo.edges[0].node
                        .industryCardTitle7
                    }
                  />
                </Link>
                <Link
                  to={
                    data.allContentfulHomePageTwo.edges[0].node
                      .industryCardLink8.json.content[0].content[1].data.uri
                  }
                >
                  <IndustryCardFR
                    iconPic={
                      <img
                        src={
                          data.allContentfulHomePageTwo.edges[0].node
                            .industryCardIcon8.file.url
                        }
                        alt="Energy Access"
                      />
                    }
                    title={
                      data.allContentfulHomePageTwo.edges[0].node
                        .industryCardTitle8
                    }
                  />
                </Link>
              </IndustriesCardsGroup>
            </ContentWrapper>
          </IndustriesWrapper>
        </IndustriesBackground>
      )}
    />
  )
}
export default IndustriesFR
const IndustriesBackground = styled.div`
  background-color: #124e5d;
  width: 100vw;
  position: relative;
  left: -8px;
`
const IndustriesWrapper = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  padding: 160px 0 160px 0;
  @media (max-width: 830px) {
    padding: 80px 48px 80px 48px;
  }
  @media (max-width: 512px) {
    padding: 60px 24px 60px 24px;
  }
`

const ContentWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 80px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 40px;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(1, auto);
    grid-gap: 20px;
  }
`
const IndustriesCardsGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(3, auto);
  grid-gap: 40px 20px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(3, auto);
    grid-gap: 40px 20px;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(2, auto);
    grid-gap: 30px 20px;
  }
`
const IndustryTextWrapper = styled.div`
  margin: 100px 0 0 0;
  @media (max-width: 830px) {
    margin: 0 0 40px 0;
  }
  @media (max-width: 512px) {
    margin: 0 0 40px 0;
  }
`

const IndustriesTitle = styled(H2)`
  color: #fefefe;
  max-width: 600px;
  margin: 0 0 20px 0;
  padding: 0;
`
const IndustriesDescription = styled(BodyMain)`
  color: #fefefe;
  max-width: 600px;
  margin: 0;
  padding: 0;
`
const HeroIllustration = styled.div`
  position: absolute;

  top: 550px;
  left: 0px;
  @media (max-width: 830px) {
    display: none;
  }
`
