import React from "react"
import styled from "styled-components"
import { H5, P } from "../styles/TextStyles.js"
function TestimonialCard(props) {
  const { quote, profilePic, personname, position, companyName } = props
  return (
    <TestimonialCardWrapper>
      <TextBlock>
        <Quotation>"</Quotation>
        <Quote>{quote}"</Quote>
      </TextBlock>
      <Citation>
        <Profile>{profilePic}</Profile>
        <Details>
          <PersonName>{personname}</PersonName>
          <Position>{position}</Position>
          <CompanyName>{companyName}</CompanyName>
        </Details>
      </Citation>
    </TestimonialCardWrapper>
  )
}
export default TestimonialCard
const TestimonialCardWrapper = styled.div`
  width: 400px;
  height: 580px;
  border: none;
  overflow: hidden;

  border-radius: 2px;
  padding: 40px 40px 20px 40px;
  background-color: #fefefe;
  box-shadow: 2px 4px 8px 0 #d0dcdf;

  @media (max-width: 830px) {
    width: 320px;
  }
  @media (max-width: 512px) {
    width: 320px;
  }
`
const TextBlock = styled.div`
  display: grid;
  grid-template-columns: auto auto;
`
const Quotation = styled(P)`
  font-size: 1.125em;
  position: absolute;
  left: 500;
  margin-left: -12px;
  margin-top: -4px;
  @media (max-width: 830px) {
    position: relative;
  }
  @media (max-width: 512px) {
    position: relative;
  }
`
const Quote = styled(P)`
  font-size: 1.125em;
`
const Citation = styled.div`
  display: grid;
  grid-template-columns: 100px auto;
  grid-gap: 0;

  @media (max-width: 450px) {
    grid-template-columns: 60px auto;
    grid-gap: 20px;
  }
`
const Details = styled.div`
  margin: 0;
  padding: 0;
`
const Profile = styled.div`
  margin-top: 16px;
  padding: 0;
`

const PersonName = styled(H5)`
  margin-bottom: 5px;
  padding: 0;
`

const Position = styled.p`
  font-size: 18px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.5;
  letter-spacing: normal;
  margin-bottom: 5px;
  color: #2c2c2c;
  padding: 0;
  @media (max-width: 450px) {
    line-height: 1;
  }
`
const CompanyName = styled.p`
  font-size: 18px;
  font-weight: normal;
  font-stretch: normal;
  font-style: italic;
  line-height: 1.5;
  letter-spacing: normal;
  color: #2c2c2c;
  padding: 0;
`
