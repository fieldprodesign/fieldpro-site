import React, { useState, useEffect } from "react"
import styled from "styled-components"

import { StaticQuery, graphql } from "gatsby"

import { H1, BodyMain, Caption } from "../styles/TextStyles.js"
import DarkCtaES from "../sections/DarkCtaES.js"
function HeroES(props) {
  const [offsetY, setOffsetY] = useState(0)
  const handleScroll = () => setOffsetY(window.pageYOffset)

  useEffect(() => {
    window.addEventListener("scroll", handleScroll)

    return () => window.removeEventListener("scroll", handleScroll)
  }, [])
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulHomePage(filter: { language: { eq: "ES" } }) {
            edges {
              node {
                heroTitle
                heroDescription
                heroImage {
                  file {
                    url
                  }
                }
              }
            }
          }
        }
      `}
      render={data => (
        <HeroSectionBackground>
          <HeroIllustration
            style={{ transform: `translateY(-${offsetY * 0.3}px)` }}
          >
            <img src="/images/Group 16.svg" alt="" />
          </HeroIllustration>
          <HeroSectionWrapper>
            <HeroContentWrapper>
              <HeroText>
                <MainHeading>
                  {data.allContentfulHomePage.edges[0].node.heroTitle}
                </MainHeading>
                <Description>
                  {data.allContentfulHomePage.edges[0].node.heroDescription}
                </Description>
                <TopCta>
                  <DarkCtaES />
                </TopCta>
              </HeroText>
              <HeroImg>
                <img
                  src={
                    data.allContentfulHomePage.edges[0].node.heroImage.file.url
                  }
                  style={{ width: "1200px" }}
                  alt="FieldPro | Field Force Automation"
                />
              </HeroImg>
            </HeroContentWrapper>
          </HeroSectionWrapper>
        </HeroSectionBackground>
      )}
    />
  )
}
export default HeroES
export const HeroSectionBackground = styled.div`
  padding: 0px !important;
  margin: 0px !important;
  height: 880px;
  width: 100vw;
  position: relative;

  left: -8px;
  background-color: #124e5d;

  @media (max-width: 830px) {
    width: 100vw;
    margin: 0 0 0 0px;
    height: 1180px;
    background-color: #124e5d;
  }
  @media (max-width: 512px) {
    width: 100vw;
    margin: 0 0 0 0px;

    height: 1080px;
    background-color: #124e5d;
  }
`
export const HeroSectionWrapper = styled.div`
  position: relative;
  max-width: 1280px;
  margin: 0 auto;
`
export const HeroContentWrapper = styled.div`
  padding: 400px auto;
  display: grid;
  grid-template-columns: repeat(2, auto);
`
export const HeroText = styled.div`
  padding: 300px 0px 0px 0px;
  max-width: 620px;

  @media (max-width: 830px) {
    padding: 200px 40px 80px 40px;
    margin: 0 0 0 0px;
    max-width: 672px;
  }
  @media (max-width: 512px) {
    padding: 200px 40px 80px 40px;
    margin: 0 0 0 0px;
    max-width: 350px;
  }
`
export const MainHeading = styled(H1)`
  color: #fefefe;
  padding: 0 0 16px 0;
`
export const Description = styled(BodyMain)`
  color: #fefefe;
  padding: 0 0 10px 0;
`
export const TopCta = styled.div`
  display: flex;
  flex-direction: row;
  grid-gap: 20px;
  @media (max-width: 512px) {
    flex-direction: column;
  }
`
const HeroImg = styled.div`
  margin: 200px 0 0 0;
  position: absolute;
  right: -580px;
  @media (max-width: 830px) {
    top: 520px;
    position: absolute;
    right: 0px;
  }

  @media (max-width: 512px) {
    top: 660px;
    position: absolute;
    right: 0px;
  }
`
export const TrialGroup = styled.div``
export const DisclaimerText = styled(Caption)`
  color: #fefefe;
  padding: 4px 0 0 16px;
  font-weight: 600;
  font-size: 14px;
`
export const HeroIllustration = styled.div`
  position: absolute;

  top: 640px;
  left: 0px;
  @media (max-width: 830px) {
    display: none;
  }
`
