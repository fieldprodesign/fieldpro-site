import React, { useCallback, useEffect, useRef, useState } from "react"
import styled from "styled-components"

const RatingsSlider = ({ children }) => {
  const [targetSlide, setTargetSlide] = useState(0)
  const wrapperRef = useRef(null)
  const targetSlideRef = useRef(null)

  const scrollToTargetSlide = useCallback(() => {
    const targetSlide = targetSlideRef.current
    const wrapper = wrapperRef.current
    if (wrapper && targetSlide) {
      wrapper.scrollTo({
        top: 0,
        left: targetSlide.offsetLeft,
        behavior: "smooth",
      })
    }
  }, [])

  //   const finishScrolling = useCallback(() => {
  //     setTargetSlide(visibleSlide)
  //     scrollToTargetSlide()
  //   }, [visibleSlide, scrollToTargetSlide])

  const moveLeft = useCallback(targetSlide => Math.max(0, targetSlide - 1), [])
  const moveRight = useCallback(
    targetSlide => Math.min(targetSlide + 1, children.length - 1),
    [children]
  )

  useEffect(scrollToTargetSlide, [targetSlide])

  return (
    <div id="trap">
      <Wrapper ref={wrapperRef}>
        {children.map((child, i) => (
          <Slide
            key={`slide-${i}`}
            ref={i === targetSlide ? targetSlideRef : null}
          >
            {child}
          </Slide>
        ))}
      </Wrapper>
      <Navbuttons>
        <ButtonPrev onClick={() => setTargetSlide(moveLeft)} />
        <ButtonNext onClick={() => setTargetSlide(moveRight)} />
      </Navbuttons>
    </div>
  )
}

export default RatingsSlider
export const ButtonNext = styled.button`
  width: 48px;
  height: 48px;
  border: 0px solid #b8cace;
  background-color: #41717d;
  border-radius: 50%;
  display: inline-block;

  :after {
    content: "";
    display: inline-block;
    margin-top: 6px;
    margin-left: -6px;
    width: 16px;
    height: 16px;
    border-top: 4px solid #2c2c2c;
    border-right: 4px solid #2c2c2c;
    -moz-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
  }
  :hover {
    background-color: #fefefe;
    border: 4px solid #124e5d;
    border-top: 4px solid #124e5d;
    border-right: 4px solid #124e5d;
  }
`
export const ButtonPrev = styled.button`
  width: 48px;
  height: 48px;
  border: 0px solid #b8cace;
  background-color: #41717d;
  border-radius: 50%;
  display: inline-block;
  :after {
    content: "";
    display: inline-block;
    margin-top: 6px;
    margin-left: 6px;
    width: 16px;
    height: 16px;
    border-top: 4px solid #2c2c2c;
    border-right: 4px solid #2c2c2c;
    -moz-transform: rotate(-135deg);
    -webkit-transform: rotate(-135deg);
    transform: rotate(-135deg);
  }
  :hover {
    background-color: #fefefe;
    border: 4px solid #124e5d;
    border-top: 4px solid #124e5d;
    border-right: 4px solid #124e5d;
  }
`
const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(4, auto);
  grid-gap: 20px;
  position: relative;
  overflow-x: auto;
  overflow-y: hidden;
  white-space: nowrap;
  max-width: 540px;
  padding: 40px 0px 40px 0px;
  @media (max-width: 830px) {
    margin: 0px;
    max-width: 660px;
  }
  @media (max-width: 512px) {
    margin: 0px;
    grid-gap: 10px;
  }
`

const Slide = styled.div`
  white-space: normal;
`
const Navbuttons = styled.div`
  display: flex;

  gap: 30px;
  margin: 20px 0 20px 0px;
  @media (max-width: 830px) {
    margin: 20px 0 0px 0px;
    padding: 0 0 0 0;
    display: flex;
  }
  @media (max-width: 512px) {
    margin: 20px 0 0px 0px;
    padding: 0 0 0 0;

    display: flex;
  }
`
