import React from "react"
import styled from "styled-components"

import { H2, P } from "../styles/TextStyles"
import TestimonialCard from "./testimonialCard"
import { StaticQuery, graphql } from "gatsby"

import TestimonialCardSliderES from "./testimonialSliderES.js"

function TestimonialsFR(props) {
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulHomePage(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                testimonialMainTitle
                testimonialSmallTitle
                testimonialDetails {
                  testimonialDetails
                }
              }
            }
          }
          allContentfulTestimonial(filter: { language: { eq: "FR" } }) {
            edges {
              node {
                quote {
                  quote
                }
                citationImage {
                  fluid {
                    src
                  }
                }
                citationName
                citationPosition
                citationCompany
              }
            }
          }
        }
      `}
      render={data => (
        <TestimonialSectionWrapper>
          <TestimonialsWrapper>
            <TestimonialContent>
              <Textgroup>
                <TestimonialSmallTitle>
                  {
                    data.allContentfulHomePage.edges[0].node
                      .testimonialSmallTitle
                  }
                </TestimonialSmallTitle>
                <TestimonialText>
                  {" "}
                  {
                    data.allContentfulHomePage.edges[0].node
                      .testimonialMainTitle
                  }
                </TestimonialText>
                <TestimonialDescription>
                  {
                    data.allContentfulHomePage.edges[0].node.testimonialDetails
                      .testimonialDetails
                  }
                </TestimonialDescription>
              </Textgroup>
              <TestimonialCardGroup>
                <TestimonialCardSliderES>
                  {data.allContentfulTestimonial.edges.map(testimonialcard => {
                    return (
                      <TestimonialCard
                        quote={testimonialcard.node.quote.quote}
                        profilePic={
                          <img
                            src={testimonialcard.node.citationImage.fluid.src}
                            alt="Citation"
                          />
                        }
                        personname={testimonialcard.node.citationName}
                        position={testimonialcard.node.citationPosition}
                        companyName={testimonialcard.node.citationCompany}
                      />
                    )
                  })}
                </TestimonialCardSliderES>
              </TestimonialCardGroup>
            </TestimonialContent>
          </TestimonialsWrapper>
        </TestimonialSectionWrapper>
      )}
    />
  )
}
export default TestimonialsFR

const TestimonialSectionWrapper = styled.div`
  width: 100vw;
  height: auto;
  overflow: hidden;
  @media (max-width: 830px) {
    overflow: hidden;
  }
  @media (max-width: 512px) {
    overflow: hidden;
  }
`
const TestimonialsWrapper = styled.div`
  max-width: 1280px;
  margin: 160px auto;
  padding: 0px 0 200px 0;

  @media (max-width: 830px) {
    margin: 0;
    padding: 60px 48px 60px 48px;
    overflow: hidden;
  }
  @media (max-width: 512px) {
    margin: 0;
    padding: 60px 24px 60px 24px;
    overflow: hidden;
  }
`
const TestimonialContent = styled.div`
  display: grid;
  grid-template-columns: 400px auto;
  gap: 40px;
  @media (max-width: 830px) {
    grid-template-columns: repeat(1, auto);
    gap: 40px;
  }
  @media (max-width: 512px) {
    grid-template-columns: repeat(1, auto);
    gap: 60px;
  }
`
const TestimonialText = styled(H2)`
  margin: 20px 0 20px 0;
  padding: 0;
  @media (max-width: 512px) {
    max-width: 320px;
  }
`
const TestimonialDescription = styled(P)`
  margin: 0 0 60px 0;
  padding: 0;
  max-width: 320px;
  @media (max-width: 512px) {
    max-width: 320px;
  }
`
const TestimonialCardGroup = styled.div`
  white-space: normal !important;
`

const Textgroup = styled.div`
  margin: 120px 0 0 0;
`
const TestimonialSmallTitle = styled(P)`
  margin: 0 0 12px 0;
  color: #124e5d;
`
