import React from "react"
import styled from "styled-components"
import NewsletterForm from "../sections/newsletter.js"
import { H2, P } from "../styles/TextStyles.js"
import { StaticQuery, graphql } from "gatsby"
function NewsletterSection(props) {
  return (
    <StaticQuery
      query={graphql`
        {
          allContentfulHomePage(filter: { language: { eq: "EN" } }) {
            edges {
              node {
                newsletterTitle
                newsletterDescription {
                  newsletterDescription
                }
              }
            }
          }
        }
      `}
      render={data => (
        <NewsletterSectionWrapper>
          <NewsletterIllustration>
            <img src="/images/Oval icon.svg" alt="" />
          </NewsletterIllustration>
          <NewsletterContentWrapper>
            <NewsletterTitle>
              {" "}
              {data.allContentfulHomePage.edges[0].node.newsletterTitle}
            </NewsletterTitle>
            <NewsletterDescription>
              {
                data.allContentfulHomePage.edges[0].node.newsletterDescription
                  .newsletterDescription
              }
            </NewsletterDescription>
            <NewsletterForm />
            <SocialMediaIcons>
              <a
                href="https://www.linkedin.com/company/optimetriks"
                target="_blank"
                rel="noopener noreferrer"
              >
                <SocialIcon src="/images/linkedIn.png" alt="LinkedIn Logo" />
              </a>
              <a
                href="https://www.facebook.com/fieldproapp"
                target="_blank"
                rel="noopener noreferrer"
              >
                <SocialIcon src="/images/facebook.png" alt="Facebook Logo" />
              </a>
            </SocialMediaIcons>
          </NewsletterContentWrapper>
        </NewsletterSectionWrapper>
      )}
    />
  )
}
export default NewsletterSection
const NewsletterSectionWrapper = styled.div`
  position: relative;
  max-width: 1280px;
  margin: 0 auto;
  padding: 160px 0 160px 0;
  @media (max-width: 830px) {
    padding: 80px 48px 80px 48px;
  }
  @media (max-width: 512px) {
    padding: 60px 24px 60px 24px;
  }
`
const NewsletterContentWrapper = styled.div`
  border: 2px solid #eaeef2;
  padding: 80px 0px 80px 160px;
  @media (max-width: 830px) {
    padding: 0;
    border: none;
  }
  @media (max-width: 512px) {
    padding: 0;
  }
`
const NewsletterTitle = styled(H2)`
  font-size: 2.5em;
  margin: 0 0 12px 0;
`
const NewsletterDescription = styled(P)`
  margin: 0 0 40px 0;
`
const SocialMediaIcons = styled.div`
  margin: 40px 0 0 0;
  display: grid;
  grid-template-columns: 80px 80px;
  grid-gap: 20px;
`
const SocialIcon = styled.img``
const NewsletterIllustration = styled.div`
  position: absolute;

  top: 80px;
  left: -80px;
  @media (max-width: 830px) {
    display: none;
  }
`
