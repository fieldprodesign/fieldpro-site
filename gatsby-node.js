const path = require(`path`)

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  const blogPost = path.resolve(`./src/templates/blogPost.js`)
  const blogCategory = path.resolve(`./src/templates/category.js`)

  const industryPage = path.resolve(`./src/templates/industry-page.js`)
  const productPage = path.resolve(`./src/templates/product-page.js`)
  const workflowPage = path.resolve(`./src/templates/workflow-page.js`)
  const addOnPage = path.resolve(`./src/templates/addOn-page.js`)
  // const findPartnerPage = path.resolve(`./src/templates/findpartner-page.js`)
  const featurePage = path.resolve(`./src/templates/feature-page.js`)

  return graphql(
    `
      {
        allContentfulFeaturesPage {
          edges {
            node {
              id
              slug
              pageTitleAsSeenOnMenu
            }
          }
        }
        allContentfulAddOnPage {
          edges {
            node {
              title
              slug
              id
            }
          }
        }
        allContentfulWorkflowTemplate {
          edges {
            node {
              title
              slug
              id
            }
          }
        }
        allContentfulBlogPost {
          edges {
            node {
              slug
              title
              language
            }
          }
        }
        allContentfulBlogCategory {
          edges {
            node {
              language
              slug
              title
            }
          }
        }
        allContentfulIndustryPage {
          edges {
            node {
              id
              slug
              pageName
            }
          }
        }
        allContentfulProductPage {
          edges {
            node {
              id
              slug
              pageName
            }
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }
    const feature = result.data.allContentfulFeaturesPage.edges
    const industry = result.data.allContentfulIndustryPage.edges
    const product = result.data.allContentfulProductPage.edges
    const posts = result.data.allContentfulBlogPost.edges
    const categories = result.data.allContentfulBlogCategory.edges
    const workflow = result.data.allContentfulWorkflowTemplate.edges
    const addOn = result.data.allContentfulAddOnPage.edges
    // const partner = result.data.allContentfulFindPartnerPage.edges

    // Create product pages.
    product.forEach((productTemp, index) => {
      const previous =
        index === product.length - 1 ? null : product[index + 1].node
      const next = index === 0 ? null : product[index - 1].node

      createPage({
        path: productTemp.node.slug,
        component: productPage,
        context: {
          slug: productTemp.node.slug,
          previous,
          next,
        },
      })
    }),
      //create blog posts page
      posts.forEach((post, index) => {
        const previous =
          index === posts.length - 1 ? null : posts[index + 1].node
        const next = index === 0 ? null : posts[index - 1].node

        createPage({
          path: post.node.slug,
          component: blogPost,
          context: {
            slug: post.node.slug,
            language: post.node.language,
            previous,
            next,
          },
        })
      })
    //create categories page
    categories.forEach((category, index) => {
      const previous =
        index === categories.length - 1 ? null : categories[index + 1].node
      const next = index === 0 ? null : categories[index - 1].node

      createPage({
        path: category.node.slug,
        component: blogCategory,
        context: {
          slug: category.node.slug,
          previous,
          next,
        },
      })
    })
    // Create industry pages.

    industry.forEach((industryTemp, index) => {
      const previous =
        index === industry.length - 1 ? null : industry[index + 1].node
      const next = index === 0 ? null : industry[index - 1].node

      createPage({
        path: industryTemp.node.slug,
        component: industryPage,
        context: {
          slug: industryTemp.node.slug,
          previous,
          next,
        },
      })
    })
    // Create workflow pages.

    workflow.forEach((workflowTemp, index) => {
      const previous =
        index === workflow.length - 1 ? null : workflow[index + 1].node
      const next = index === 0 ? null : workflow[index - 1].node

      createPage({
        path: workflowTemp.node.slug,
        component: workflowPage,
        context: {
          slug: workflowTemp.node.slug,
          previous,
          next,
        },
      })
    })
    // Create  addOn pages.

    addOn.forEach((addOnTemp, index) => {
      const previous = index === addOn.length - 1 ? null : addOn[index + 1].node
      const next = index === 0 ? null : addOn[index - 1].node

      createPage({
        path: addOnTemp.node.slug,
        component: addOnPage,
        context: {
          slug: addOnTemp.node.slug,
          previous,
          next,
        },
      })
    })

    // Create find partner pages.

    // partner.forEach((partnerTemp, index) => {
    //   const previous =
    //     index === partner.length - 1 ? null : partner[index + 1].node
    //   const next = index === 0 ? null : partner[index - 1].node

    //   createPage({
    //     path: partnerTemp.node.slug,
    //     component: findPartnerPage,
    //     context: {
    //       slug: partnerTemp.node.slug,
    //       previous,
    //       next,
    //     },
    //   })
    // })

    // Create feature pages.

    feature.forEach((featureTemp, index) => {
      const previous =
        index === feature.length - 1 ? null : feature[index + 1].node
      const next = index === 0 ? null : feature[index - 1].node

      createPage({
        path: featureTemp.node.slug,
        component: featurePage,
        context: {
          slug: featureTemp.node.slug,
          previous,
          next,
        },
      })
    })
  })
}
